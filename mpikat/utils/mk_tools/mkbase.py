from typing import List
from dataclasses import asdict
import spead2

from mpikat.core.data_stream import DataStreamRegistry, DataStream
from mpikat.core.logger import getLogger
from mpikat.utils.process_tools import ManagedProcess
from mpikat.utils.dada_tools.reader import DadaClient
from mpikat.utils.ip_utils import concatenate_ips

from .mkheader import MkHeader, MkItem
from .mkutils import get_item


_LOG = getLogger("mpikat.utils.mk_tools.mkbase")

class MkBase(DadaClient):

    def __init__(self, streams: List[dict], key: str, physcpu: str="",
                 ibv: bool=True, mapping: dict=None):
        """Base class of Mksend and Mkrecv - Abstrcats general/shared components

        Args:
            streams (List[dict]): A list of stream definitions.
            key (str): The dada key to connect to
            physcpu (str, optional): The physical CPU to run the CLI on. Defaults to "".
            ibv (bool, optional): Use ibv or udp -> ibv for maximum performance. Defaults to True.
            mapping (dict, optional): A mapping dictionary allowing to translate streams
                to mkheader. Defaults to None.
        """
        self.header_file = ""
        self.items: List[MkItem] = []
        self.streams: List[dict] = streams
        if not isinstance(streams, list):
            self.streams = [streams]

        self.header: MkHeader = MkHeader(self.streams[0], mapping=mapping)
        self.header.dada_key = key

        if ibv:
            self.header.udp_if = "unset"
        else:
            self.header.udp_if = self.header.ibv_if
            self.header.ibv_if = "unset"

        self.formats: List[DataStream] = list(set(
            [DataStreamRegistry.get_class(stream["type"]) for stream in self.streams]))
        self.spead_items: List[spead2.Item] = [
            item for sublist in [f.data_items for f in self.formats] for item in sublist]
        self.ip_list: List[str] = concatenate_ips([stream["ip"] for stream in self.streams])

        super().__init__(key, physcpu=physcpu)

    def command(self) -> str:
        """Function to construct the Mk-command

        Returns:
            str: The constructed command
        """
        return self.cmd

    def start(self, env: dict=None) -> ManagedProcess:
        """Start and return the ManagedProcess

        Args:
            env (dict, optional): Environmental variables to run the command with. Defaults to None.

        Returns:
            ManagedProcess: The running process
        """
        self.command()
        _LOG.info("Starting MK process with cmd: %s", self.cmd)
        return super().start(env)

    def get_item(self, name: str) -> MkItem:
        """Returns the item by its name

        Args:
            name (str): The item's name

        Returns:
            MkItem: The item
        """
        return get_item(self.items, name)

    def set_item(self, name: str, key: str, val: any):
        """Sets the a property of an item by its name and key
            The property must exists in the Mkitem-dataclass

        Args:
            name (str): The item's name
            key (str): The key/property to set
            val (any): The value of the property
        """
        setattr(self.get_item(name), key, val)


    def set_item_by_conf(self, name: str, item_conf: dict):
        """Sets the properties of an item by a given conf
            Allows to set multiple properties with one function call
            The properties must exist in the Mkitem-dataclass
        Args:
            name (str): The item's name
            item_conf (dict): The dicionary key-value-pairs
        """
        for key, val in item_conf.items():
            self.set_item(name, key, val)


    def set_header(self, key:str, val: any) -> None:
        """Sets a header property
            The property must exists in the Mkheader-dataclass
        Args:
            key (str): The property
            val (any): The value
        """
        if not key in asdict(self.header):
            _LOG.warning("Key %s is not not declared in MkHeader")
        setattr(self.header, key, val)

