# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring

import unittest

import asyncio

import aiokatcp
import datetime
from aiokatcp.client import Client

from mpikat.utils import testing, get_port
from mpikat.core.datastore import EDDDataStore
from mpikat.core.telescope_meta_information_server import TMIServer, OverridableSensor
from mpikat.telescopes.skampi import ska_protodish_status_server

HOST = "127.0.0.1"

class DummyTMI(TMIServer):
    async def ensure_update(self, cfg=None):
        values = {str: 'foo', float: 23., int: 42}
        for s in self.sensors.values():
            v = values.get(s.stype)
            if v:
                s.set_value(v)

class Test_OverridableSensor(unittest.TestCase):
    def setUp(self):
        self.redis = testing.setup_redis()
        self.datastore = EDDDataStore(host=HOST, port=self.redis.server_config['port'])
        self.sensor = OverridableSensor(str, name='foo', default='foo', datastore=self.datastore)

    def tearDown(self):
        self.redis.shutdown()

    def test_setting_of_value(self):
        self.sensor.set_value('bar')
        self.assertEqual(self.sensor.value, 'bar')
        self.assertEqual(self.datastore.getTelescopeDataItem('foo'), 'bar')

    def test_overriding_of_value(self):
        self.assertEqual(self.datastore.getTelescopeDataItem('foo', 'overriden'), 'False')
        self.sensor.override = True
        self.sensor.set_value('bar')
        self.assertEqual(self.sensor.value, 'foo')
        self.assertEqual(self.datastore.getTelescopeDataItem('foo'), 'foo')

        self.sensor.override_value('bar')
        self.assertEqual(self.sensor.value, 'bar')
        self.assertEqual(self.datastore.getTelescopeDataItem('foo'), 'bar')
        self.assertEqual(self.datastore.getTelescopeDataItem('foo', 'overriden'), 'True')


class Test_TMI(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.port = get_port()
        self.server_thread = testing.launch_server_in_thread(DummyTMI, HOST, self.port)
        self.server = self.server_thread.server
        self.client = None

    async def asyncSetUp(self) -> None:
        self.client = await asyncio.wait_for(
            Client.connect(HOST, self.port), timeout=5
        )

    async def test_server_running(self):
        """Version list should not fail"""
        await asyncio.wait_for(self.client.request('version-list'), timeout=5)

    async def test_update_of_value(self):
        """Ensure_update should update the sensors to dummy default values"""
        self.assertEqual(self.server.sensors['source-name'].value, '')

        future = asyncio.run_coroutine_threadsafe(self.server.ensure_update(), self.server_thread.loop)
        future.result()
        self.assertEqual(self.server.sensors['source-name'].value, 'foo')
        self.assertEqual(self.server.sensors['ra'].value, 23.)

    async def test_sensor_control(self):
        """A controlled sensor should not be updated to a dummy value"""
        await asyncio.wait_for(self.client.request('sensor-control', 'source-name'), timeout=5)
        future = asyncio.run_coroutine_threadsafe(self.server.ensure_update(), self.server_thread.loop)
        future.result()
        self.assertEqual(self.server.sensors['source-name'].value, '')

    async def test_sensor_control_request_invalid_sensor(self):
        with self.assertRaises(aiokatcp.FailReply):
            await asyncio.wait_for(self.client.request('sensor-control', 'non-existing-sensor'), timeout=5)

    async def test_control_all(self):
        for s in self.server.sensors.values():
            if not isinstance(s, OverridableSensor):
                continue
            self.assertFalse(s.override)
        await asyncio.wait_for(self.client.request('sensor-control-all'), timeout=5)
        for s in self.server.sensors.values():
            if not isinstance(s, OverridableSensor):
                continue
            self.assertTrue(s.override)

    async def test_release_sensor(self):
        self.server.sensors['source-name'].override = True
        await asyncio.wait_for(self.client.request('sensor-release', 'source-name'), timeout=5)
        self.assertFalse(self.server.sensors['source-name'].override)

    async def test_release_all_sensor(self):
        for s in self.server.sensors.values():
            if not isinstance(s, OverridableSensor):
                continue
            s.override = True
        await asyncio.wait_for(self.client.request('sensor-release-all'), timeout=5)
        for s in self.server.sensors.values():
            if not isinstance(s, OverridableSensor):
                continue
            self.assertFalse(s.override)

    async def test_sensor_set(self):

        with self.assertRaises(aiokatcp.FailReply):
            await asyncio.wait_for(self.client.request('sensor-set', 'non-existing-sensor', 'foo'), timeout=5)

        with self.assertRaises(aiokatcp.FailReply):
            await asyncio.wait_for(self.client.request('sensor-set', 'source-name', 'foo'), timeout=5)

        await asyncio.wait_for(self.client.request('sensor-control-all'), timeout=5)
        await asyncio.wait_for(self.client.request('sensor-set', 'source-name', 'foo'), timeout=5)
        self.assertEqual(self.server.sensors['source-name'].value, 'foo')
        await asyncio.wait_for(self.client.request('sensor-set', 'ra', 12.3), timeout=5)
        self.assertEqual(self.server.sensors['ra'].value, 12.3)

    async def test_list_controlled_sensors(self):
        await asyncio.wait_for(self.client.request('sensor-control', 'source-name'), timeout=5)
        rep, _ = await asyncio.wait_for(self.client.request('sensor-list-controlled'), timeout=5)
        self.assertEqual(rep[0].decode(), 'source-name')


    async def test_removal_of_sensors(self):
        self.assertTrue('source-name' in self.server.sensors)
        self.server.sensors.pop('source-name')
        self.assertFalse('source-name' in self.server.sensors)


    async def asyncTearDown(self) -> None:
        self.client.close()
        self.server_thread.stop().result()
        self.server_thread.join(timeout=5)


    async def test_date_update_on_ensure_update(self):
        """Date sensor should be updated to current date"""
        await asyncio.wait_for(self.client.request('ensure-update', '{}'), timeout=5)
        d = datetime.datetime.now()
        self.assertEqual(self.server.sensors['date'].value, f'{d.year}-{d.month:02}-{d.day:02}')


class Test_SKAMPI_TMI(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.port = get_port()
        self.server_thread = testing.launch_server_in_thread(ska_protodish_status_server.SKAMPIStatusServer, HOST, self.port)
        self.server = self.server_thread.server
        self.client = None

    async def asyncSetUp(self) -> None:
        self.client = await asyncio.wait_for(
            Client.connect(HOST, self.port), timeout=5
        )
    async def asyncTearDown(self) -> None:
        self.client.close()
        self.server_thread.stop().result()
        self.server_thread.join(timeout=5)

    async def test_removal_wind_sensors(self):
        self.assertFalse('wind-speed' in self.server.sensors)

        with self.assertRaises(aiokatcp.FailReply):
            await asyncio.wait_for(self.client.request('sensor-control', 'wind-speed'), timeout=5)


if __name__ == '__main__':
    unittest.main()
