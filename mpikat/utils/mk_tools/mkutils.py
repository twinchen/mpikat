from typing import Union, List
from mpikat.core.logger import getLogger

from .mkheader import MkItem

_LOG = getLogger("mpikat.utils.mk_tools.mkutils")



def get_item(items: List[MkItem], name: str) -> MkItem:
    """Returns an item from a list that matches the name

    Args:
        name (str): The name to search for

    Returns:
        MkItem: The item to return
    """
    for item in items:
        if item.name == name:
            return item
    _LOG.error("No item exists with name %s", name)
    return None


def is_hex(s: str) -> bool:
    """Checks if provided string is a hex string

    Args:
        s (str): the string to check

    Returns:
        bool: True if it is a hex string, otherwise False
    """
    try:
        int(s, 16)
        return True
    except ValueError:
        return False

def get_index_mask(mask: Union[int, str]) -> str:
    """Proofs or converts the given input to a hex string
    Args:
        mask (Union[int, str]): The inital mask

    Returns:
        str: Returns the mask. If input is a invalid mask, returns None
    """
    if isinstance(mask, int):
        return hex(mask)
    if isinstance(mask, str):
        if is_hex(mask):
            return mask
    return None

def get_index_list(index_list: Union[range, str]) -> str:
    """Proofs or converts the given input to a Mk index list

    Args:
        index_list (Union[range, str]): The inital index list

    Returns:
        str: returns modified index list, returns None if input is an invalid list
    """
    if isinstance(index_list, range):
        return f"{index_list.start}:{index_list.stop}:{index_list.step}"
    if isinstance(index_list, str):
        if "," in index_list or ":" in index_list:
            return index_list
    return None

def read_header(fname: str) -> dict:
    """Reads file of the DADA format, parses its content
        and returns it as a dictionary

    Args:
        fname (str): The filename

    Returns:
        dict: The resulting dictionary
    """
    res = {}
    with open(fname, "r", encoding="ascii") as f:
        for line in f:
            line = line.strip()
            if line and not line.startswith("#"):  # Skip empty lines and comments
                key, value = line.split(maxsplit=1)
                key = key.strip()
                value = value.split("#", 1)[0].strip()  # Remove comments
                res[key.lower()] = value
    return res
