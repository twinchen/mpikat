import unittest
import asyncio
from aiokatcp.sensor import Sensor
from mpikat.pipelines.gated_spectrometer import Plotter

class Test_ProcessIO(unittest.IsolatedAsyncioTestCase):

    def setUp(self) -> None:
        self.tobj = Plotter()
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    async def test_cancel_task_repititive(self):
        # Plotting not running
        dummy = Sensor(str, "dummy")
        self.assertFalse(self.tobj.plotting)
        for __ in range(3):
            # Plotting should run
            task = asyncio.create_task(self.tobj.consume_plots(dummy))
            await asyncio.sleep(.5)
            self.assertTrue(self.tobj.plotting)
            # After task.cancel() plotting not running and task cancelled
            task.cancel()
            await asyncio.sleep(.5)
            self.assertTrue(task.cancelled)
            self.assertFalse(self.tobj.plotting)




if __name__ == "__main__":
    unittest.main()