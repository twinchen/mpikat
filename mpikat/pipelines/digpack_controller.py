"""
The DigitizerController pipeline controls MPIfR digitizer/packetizer via KATCP.

Consecutive configure commands are send to the packetizer only if the
parameters have changed. This behavior can be controlled using the
**force_reconfigure** and **max_sync_age** options.

Note:
    The digitizer state is not queried, but only previous configs received by
    the pipeline are remembered. Thus, the first  configuration is always send,
    and interacting with the packetizer by other tools in parallel will result
    in possibly wrong configuration options.


Configuration Settings
----------------------

 bit_depth (int)
    Number of bits of information in each sample. Possible values are 8 and 12.

 sampling_rate
    Sampling rate of the packetizer in Hz. Note that the effective sampling
    rate also depends on the **predecimation_factor**.

 predecimation_factor (int)
    Only 1 out of **predecimation_factor** samples is used by the ADC. A
    sampling_rate setting of 2600000000 and a **predecimation_factor** of 2
    corresponds to an effective sampling_rate of 1.3 GHz. Possible values are:
    1,2,4,8.

 invert_1pps (boolean)
    invert the 1PPS sampling sense, for all packetizers before UBB (P217/S110/S45/Multifiba),
    this should be set to True, otherwise False.

 noise_diode_pattern (float, float) (fraction [0-1], period [s])
    Noise diode will be turned on for a given fraction of the given period.
    Fraction 0.5 and period 1s thus corresponds to a noise diode firing
    frequency of 1 Hz. This setting can be changed during measurement_prepare.

 flip_spectrum (boolean)
    Flips the spectrum, e.g. to correct for Nyquist zone.


Output Data Streams
-------------------
    polarization_0, polarization_1
        One output data stream per polarization in MPIFR_EDD_Packetizer format.
        polarization_0 maps to capture_destination "v" of the packetizer with packer 0,
        polarization_1 maps to capture_destination "h" of the packetizer with packer 1.


Measurement prepare
-------------------
noise_diode_pattern (float, float) (fraction [0-1], period [s])
    Changes the noise diode pattern as if it would have been specified in the
    configuration.


Expert/Debugging Settings
-------------------------
 sync_time (int)
    Provide time stamp (Unix time) at which the packetizer will be synced. The
    timestamp has to be in the future.  If the value is 0 (default), the
    current time is used.

 force_reconfigure (boolean)
    Force a reconfiguration even if parameters are the same.

 max_sync_age (int)
    Maximum time since the last synchronization [s] before a packetizer is
    reconfigured even if no settings have changed.  Default: 82800 s

 skip_packetizer_config (boolean)
    If true, ALWAYS skips the packetizer configuration - Overrides force and
    max sync age. Useful for pipeline development.

 dummy_configure (boolean)
    Update output streams with dummy configuration. Useful for provision
    development.

"""
from concurrent.futures import ProcessPoolExecutor
import asyncio
import time
import aiokatcp
import redis
import json
from aiokatcp.sensor import Sensor

from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change, getArgumentParser
from mpikat.core.digpack_client import DigitiserPacketiserClient
from mpikat.core.datastore import EDDDataStore
from mpikat.core import logger as logging
from mpikat.utils.data_streams import PacketizerStreamFormat

_log = logging.getLogger("mpikat.pipelines.digpack_controller")

_DEFAULT_CONFIG = {
    "id": "DigitizerController",
    "type": "DigitizerController",
    "bit_depth" : 12,
    "sampling_rate" : 2600000000,
    "predecimation_factor" : 1,
    "flip_spectrum": False,
    "invert_1pps": False,                                       #For all packetizers before UBB (P217/S110/S45/Multifiba), this should be set to True, otherwise False.
    'sync_time': 0,
    'noise_diode_sync_window': 5,                                      # Time window [s] in which to sync noise diode to other digitizer/packetizer instances.
    'noise_diode_pattern': {"percentage": 0, "period": 0},                  # Note: noise diode pattern can also be set in measurement prepare
    'force_reconfigure': False,
    'skip_packetizer_config': False,
    'packer_sensor_update_interval': 10,     # interval on which packer sensors are queried [ms]
    'dummy_configure': False,
    'max_sync_age': 82800,
    'interface_addresses': [],                               # if set, the digitizer NICs are assigned an IP manually during configure
    'interface_mac': [],                               # if set, the digitizer NICs are assigned a mac manually during configure
    'digital_filter': 0,
    'digpack_generation': 1,
    'snapshot_frequency': -1.,                                  # Frequency for queries of digitizer snapshot, disabled for 0 or negative values
    "output_data_streams":
    {
        "polarization_0" :                          # polarization_0 maps to v in packetizer nomenclature
        {                                           # Dual info on polarization, as stream naming is arbitrary string, but pol field carries the information
                            "format": "MPIFR_EDD_Packetizer:1",
            "polarization": 0,
        },
            "polarization_1" :
        {
                            "format": "MPIFR_EDD_Packetizer:1",
            "polarization": 1,
        }
    }
}

def get_shared_sync_time(redis_db, key, delay):
    """
    Returns a time stamp for a sync between now and at most now+delay.
    """
    _log.debug("Getting shared sync time")
    # Use pipeline to guarantee non-deletion of key between set and get
    pipeline = redis_db.pipeline()
    # Create a new key only when not existing. Set a expiry of the key with the
    # delay time
    pipeline.set(key, int(time.time()) + delay, nx=True, ex=delay)
    pipeline.get(key)
    r, v = pipeline.execute()
    # return the key
    _log.debug("Shared synctime set %s - value [%s]", r, v)

    return float(v)


def plot_script(data):
    """plot data and return b64 encoded image"""
    import matplotlib as mpl
    mpl.use('Agg')
    import numpy as np
    import pylab as plt

    import sys
    import io

    import base64
    mpl.rcParams.update(mpl.rcParamsDefault)
    mpl.use('Agg')

    spectrum_figure = plt.figure()
    spectrum = spectrum_figure.add_subplot(111)
    level_figure = plt.figure()
    level = level_figure.add_subplot(111)

    for i in range(2):
        k = 'adc{}'.format(i)
        if k in data:
            spectrum.plot(data[k], label='Pol {}'.format(i))
        k = 'level{}'.format(i)
        if k in data:
            level.step(np.arange(0, len(data[k])), data[k], label='Pol {}'.format(i))

    spectrum.legend()
    spectrum.set_xlabel('Channel')
    spectrum.set_ylabel('Power [dB]')
    level.legend()
    level.set_xticklabels([])
    level.set_yticklabels([])

    level.set_xlabel('V [a.u.]')
    level.set_ylabel('Samples / V')

    def get_b64(fig):
        """
        Encodes figure as b64 encoded PNG bytes object.
        """
        fig_buffer = io.BytesIO()
        fig.savefig(fig_buffer, format='png')
        fig_buffer.seek(0)
        b64 = base64.b64encode(fig_buffer.read())
        return b64

    result = []
    for f in [spectrum_figure, level_figure]:
        f.tight_layout()
        f.suptitle(data['header']['timestamp'])
        result.append(get_b64(f))
        plt.close(f)

    return result

class DigitizerControllerPipeline(EDDPipeline):
    VERSION_INFO = ("mpikat-edd-api", 0, 1)
    BUILD_INFO = ("mpikat-edd-implementation", 0, 1, "rc1")

    def __init__(self, ip, port, packetizer_ip=None, packetizer_port=None, loop=None):
        """Initialize the pipeline.

           Device_ip / device_port is the control IP/port of the packetizer or its wrapper.
           Packer command prefix can be set in case of a packer wrapper.
        """
        EDDPipeline.__init__(self, ip, port, _DEFAULT_CONFIG, loop=loop)
        self.__eddDataStore = None
        self._client_connection = aiokatcp.Client(packetizer_ip, packetizer_port)

        # We do not know the initial state of the packetizer before we take
        # control, thus we will config on first try
        self.__previous_config = None
        self.__pass_through_sensors = {}
        # List of sensors to query regularly from packetizer

        self.__periodic_callback = None

        self._client = DigitiserPacketiserClient(self._client_connection)

    async def packer_sensor_update(self):
        """Update all sensors from packer"""
        _, informs = await self._client._safe_request(False, "sensor-value")

        for inform in informs:
            try:
                self.update_sensor_values(inform)
            except IndexError:
                pass

    def update_sensor_values(self, inform):
        sensor_key = inform.arguments[2].decode().replace(".", "_").replace("-", "_")[4:]

        if sensor_key in self.__pass_through_sensors:
            sensor = self.__pass_through_sensors[sensor_key]
            value = inform.arguments[4].decode()
            timestamp = inform.arguments[-2].decode()
            sensor.set_value(value, timestamp)

    def add_sensor(self, sensor):
        self.sensors.add(sensor)
        _log.debug(f"adding sensor {sensor}")

    async def install_sensors(self):
        """
        Create sensors from pass through sensors
        """
        _log.debug("Adding pass-through sensors")
        if not self._client:
            _log.warning("No client available! Not adding sensor.")
            return
        _, informs = await self._client._safe_request(False, "sensor-list")

        for i in range(len(informs)):
            sensor_name = informs[i].arguments[0].decode().replace(".","_").replace("-","_")[4:]
            description = informs[i].arguments[1].decode()
            units = informs[i].arguments[2].decode()
            data_type = informs[i].arguments[3].decode()
            try:
                if data_type == "discrete":
                    s = Sensor(str, sensor_name, description, units)
                elif data_type == "integer":
                    s = Sensor(int, sensor_name, description, units)
                elif data_type == "float":
                    s = Sensor(float, sensor_name, description, units)
                self.__pass_through_sensors[sensor_name] = s
                self.add_sensor(s)
            except Exception as E:
                _log.error("Error adding sensor %s", sensor_name)
                _log.exception(E)
        self.mass_inform('interface-changed')


    def setup_sensors(self):
        """
        @brief Setup monitoring sensors
        """
        EDDPipeline.setup_sensors(self)

        self._bandpass = Sensor(str,
            "bandpass_PNG",
            description="band-pass data (base64 encoded)",
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(self._bandpass)

        self.add_sensor(
                Sensor(float,"noise_diode_sync_time", description="sync time for noise diode pattern", initial_status=Sensor.Status.UNKNOWN)
                )

        self._level = Sensor(str,
            "level_PNG",
            description="ADC Level (base64 encoded)",
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(self._level)

        self._plotting = Sensor(bool,
            "plotting",
            description="If True, the digpackclient will periodically query sensors and spectra from the digpack client. Toggled by start/stop plotting request.",
            initial_status=False)
        self.add_sensor(self._plotting)


    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Configure the Packetizer
        """
        self.__eddDataStore = EDDDataStore(self._config["data_store"]["ip"], self._config["data_store"]["port"])
        if not self.__pass_through_sensors:
            await self.install_sensors()
            _log.info("packer_sensor_update")
            await self.packer_sensor_update()

        async def _sensor_value_callback():
            while True:
                await self.packer_sensor_update()
                await asyncio.sleep(self._config['packer_sensor_update_interval'])
        self._sensor_value_callback_task = asyncio.create_task(_sensor_value_callback())

        await self._client.query_packer_command_prefix()
        if self._config["dummy_configure"]:
            _log.warning("DUMMY CONFIGURE ENABELD!")
            for pol in ["polarization_0", "polarization_1"]:
                self._config["output_data_streams"][pol]["sync_time"] = 23
                self._config["output_data_streams"][pol]["bit_depth"] = self._config["bit_depth"]
                self._config["output_data_streams"][pol]["sample_rate"] = self._config["sampling_rate"] / self._config['predecimation_factor']
            self._configUpdated()
            return

        # Do not use configure from packetizer client, as we know the previous
        # config and may thus send this only once.
        _log.info("Configuring packetizer")

        sync_age = time.time() - (await self._client.get_sync_time())

        if self._config["snapshot_frequency"] > 0:
            self.start_plotting()
        else:
            self.stop_plotting()

        await self._client.check_packetizer_state()

        if self._config['force_reconfigure'] or self.__previous_config != self._config or sync_age > self._config["max_sync_age"]:
            _log.debug("Reconfiguring packetizer - Config changed: {}; Sync_age : {}; Forced: {}".format(self.__previous_config != self._config, sync_age, self._config['force_reconfigure']))

            if not self._config["interface_addresses"]:
                _log.debug("Getting IP addresses")
                R = await self._client.get_source_ip(0)
                self._config["interface_addresses"].append(R)
                R = await self._client.get_source_ip(1)
                self._config["interface_addresses"].append(R)

            if not self._config["interface_mac"]:
                _log.debug("Getting MAC addresses")
                R = await self._client.get_source_mac(0)
                self._config["interface_mac"].append(R)
                R = await self._client.get_source_mac(1)
                self._config["interface_mac"].append(R)


            if not self._config["output_data_streams"]["polarization_0"]["ip"]:
                vips = await self._client.get_destination(0)
                hips = await self._client.get_destination(1)
                self._config["output_data_streams"]["polarization_0"]["ip"] = vips.split(':')[0]
                self._config["output_data_streams"]["polarization_0"]["port"] = vips.split(':')[1]

                self._config["output_data_streams"]["polarization_1"]["ip"] = hips.split(':')[0]
                self._config["output_data_streams"]["polarization_1"]["port"] = hips.split(':')[1]
            else:
                vips = "{}:{}".format(self._config["output_data_streams"]["polarization_0"]["ip"], self._config["output_data_streams"]["polarization_0"]["port"])
                hips = "{}:{}".format(self._config["output_data_streams"]["polarization_1"]["ip"], self._config["output_data_streams"]["polarization_1"]["port"])

            if self._config["skip_packetizer_config"]:
                _log.warning('Packetizer configuration manually skipped')
            else:
                await self._client.capture_stop()
                await self._client.set_sampling_rate(self._config["sampling_rate"])
                await self._client.set_predecimation(self._config["predecimation_factor"])

                await self._client.flip_spectrum(self._config["flip_spectrum"])
                await self._client.set_bit_width(self._config["bit_depth"])

                if self._config['digpack_generation'] == 1:
                    await self._client.set_digital_filter(self._config['digital_filter'])
                else:
                    pass
                for i, ip_address in enumerate(self._config["interface_addresses"]):
                    _log.debug("Set IP address iface %i -> %s", i, ip_address)
                    await self._client.set_interface_address(i, ip_address)

                for i, mac in enumerate(self._config["interface_mac"]):
                    _log.debug("Set mac address iface %i -> %s", i, mac)
                    await self._client.set_mac_address(i, mac)

                await self._client.set_destinations(vips, hips)
                await self._client.invert_1pps(self._config["invert_1pps"])
                if self._config["sync_time"] > 0:
                    await self._client.synchronize(self._config["sync_time"])
                else:
                    await self._client.synchronize()

            _log.debug("Update output data streams")
            sync_time = await self._client.get_sync_time()

            for pol in ["polarization_0", "polarization_1"]:
                self._config["output_data_streams"][pol]["sync_time"] = sync_time
                self._config["output_data_streams"][pol]["bit_depth"] = self._config["bit_depth"]
                self._config["output_data_streams"][pol]["sample_rate"] = self._config["sampling_rate"] / self._config['predecimation_factor']
            self._configUpdated()
            self.__previous_config = self._config
        else:
            _log.debug("Configuration of packetizer skipped as not changed.")

    @state_change(target="streaming", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        """
        @brief start streaming spectrometer output
        """
        _log.info("Starting streaming")
        await self._client.capture_start()
        await self.set_noise_diode_pattern(self._config)

    async def start_plotting(self):
        """
        start plotting of snapshots
        """
        if not self._plotting.value():
            self._plotting.set_value(True)
            self._config['snapshot_frequency'] = 10.
            await self._client.enable_snapshot()
            try:
                await self.periodic_plot_snapshot()
            except Exception as E:
                _log.error("Error starting plotting")
                _log.exception(E)

    def stop_plotting(self):
        """
        start plotting of snapshots
        """
        self._plotting.set_value(False)

    async def periodic_plot_snapshot(self):
        """Periodically plot the packetizer snapshots"""
        _log.debug("Start periodic plot")

        _log.debug("Starting periodic plot")
        with ProcessPoolExecutor(max_workers=1) as executor:
            try:
                while self._plotting.value():

                    starttime = time.time()
                    data = await self._client.get_snapshot()
                    plt = await executor.submit(plot_script, data)
                    self._bandpass.set_value(plt[0])
                    self._level.set_value(plt[1])

                    duration = time.time() - starttime
                    _log.debug("Plot duration: {} s".format(duration))
                    if duration > self._config['snapshot_frequency']:
                        _log.warning("Plot duration {} larger than plot interval!".format(duration))
                    else:
                        await asyncio.sleep(self._config['snapshot_frequency'] - duration)
            except Exception as E:
                _log.error("Error in periodic plot. Abandon plotting.")
                _log.exception(E)


    async def set_noise_diode_pattern(self, cfg):
        _log.debug('Setting noise diode pattern')

        status = Sensor.Status.NOMINAL
        try:
            sync_time = get_shared_sync_time(self.__eddDataStore.edd_data, "DigitizerController:noise_diode_sync_time", self._config['noise_diode_sync_window'])
            wait_time = sync_time - time.time()
        except (redis.exceptions.ConnectionError, ConnectionError) as E:
            _log.warning("Cannot sync shared time")
            _log.debug(E)
            sync_time = 'now'
            status = Sensor.Status.WARN
            wait_time = 0

        self.sensors['noise_diode_sync_time'].set_value(sync_time if sync_time != 'now' else 0, status=status)

        if "noise_diode_frequency" in cfg:
            task = self._client.set_noise_diode_frequency(cfg["noise_diode_frequency"], starttime=sync_time)
        elif "noise_diode_pattern" in cfg:
            c = cfg["noise_diode_pattern"]
            task = self._client.set_noise_diode_firing_pattern(c["percentage"], c["period"], starttime=sync_time)
        else:
            return

        await asyncio.gather(task, asyncio.sleep(wait_time))


    @state_change(target="streaming", allowed=["streaming"], intermediate="streaming")
    async def measurement_prepare(self, config):
        """
        Set noise diode properties using

        config['noise_diode_frequency'],

        or

        config['noise_diode_pattern']['percentage']
        config['noise_diode_pattern']['period']
        """

        await self.set_noise_diode_pattern(config)
        nd = await self._client.get_noise_diode_pattern()
        self.__eddDataStore.setTelescopeDataItem('noise_diode_pattern', value=json.dumps(nd))

    @state_change(target="idle", allowed=["streaming"], intermediate="capture_stopping")
    async def capture_stop(self):
        """
        Stop streaming of data
        """
        await self._client.capture_stop()


    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """
        Deconfigure the gated spectrometer pipeline.
        """
        _log.info("Deconfiguring EDD backend")
        await self._client.capture_stop()

def main():
    parser = getArgumentParser()
    parser.add_argument('--packetizer-ip', dest='packetizer_ip', type=str, help='The control IP of the packetizer')
    parser.add_argument('--packetizer-port', dest='packetizer_port', type=int, default=7147, help='The port number to control the packetizer')
    args = parser.parse_args()

    _log.info('Connecting to packetizer @ {}:{}'.format(args.packetizer_ip, args.packetizer_port))

    async def wrapper():
        pipeline = DigitizerControllerPipeline(args.host, args.port, args.packetizer_ip, args.packetizer_port)
        await launchPipelineServer(pipeline, args)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(wrapper())


if __name__ == "__main__":
    main()

