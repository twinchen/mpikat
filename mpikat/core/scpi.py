import errno
import shlex
import socket
import asyncio
import functools
from threading import Event
from datetime import datetime
from asyncio import sleep
import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.scpi")


class UnhandledScpiRequest(Exception):
    pass


class MalformedScpiRequest(Exception):
    pass


class ScpiRequest:
    def __init__(self, data, addr, socket):
        """
        @brief      Class for wrapping SCPI requests.

        @param data   The data in the SCPI message
        @param addr   The address of the message origin
        @param socket The socket instance on which this message was received
        """
        if isinstance(data, bytes):
            data = data.decode('utf-8')
        self._data = data
        self._addr = addr
        self._socket = socket

    @property
    def data(self):
        return self._data.rstrip()

    @property
    def command(self):
        return self._data.split()[0].lower()

    @property
    def args(self):
        return shlex.split(self._data)[1:]

    def _send_response(self, msg):
        _log.trace("Sending response")
        isotime = "{}UTC".format(
            datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3])
        response = "{} {}".format(msg, isotime)
        _log.info(
            "Acknowledging request '{}' from {}".format(
                self.data, self._addr))
        _log.debug("Acknowledgment: {}".format(response))
        self._socket.sendto(response.encode('utf-8'), self._addr)

    def error(self, error_msg):
        """
        @brief Return an SCPI error to the original sender

        @detail The response consists of the original message followed by and ISO timestamp.
        """
        self._send_response("{} ERROR {}".format(self.data, error_msg))

    def ok(self):
        """
        @brief Return an SCPI acknowledgment to the original sender

        @detail The response consists of the original message followed by and ISO timestamp.
        """
        self._send_response(self.data)


class ScpiAsyncDeviceServer:
    def __init__(self, interface, port):
        """
        @brief      Server for handling SCPI requests

        @param interface    The interface on which to listen
        @param port         The port on which to listen
        """
        _log.debug("Creating SCPI interface on port {}".format(port))
        self._address = (interface, port)
        self._buffer_size = 4096
        self._stop_event = Event()
        self._socket = None
        self._receiver_task = None
        self._dispatcher_task = None
        self._request_queue = asyncio.Queue()

    def _create_socket(self):
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._socket.bind(self._address)
        _log.debug('binding to socket: {}:{}'.format(*self._address))
        self._socket.setblocking(False)

    def _flush(self):
        _log.debug("Flushing socket")
        while True:
            try:
                message, addr = self._socket.recvfrom(self._buffer_size)
                _log.debug(
                    "Flushing message '{}' from {}:{}".format(
                        message, addr[0], addr[1]))
            except Exception as E:
                break
        _log.debug("Socket flushed")

    def _make_coroutine_wrapper(self, req, cr, *args, **kwargs):
        async def wrapper():
            try:
                await cr(*args, **kwargs)
            except Exception as error:
                _log.error(str(error))
                req.error(str(error))
            else:
                req.ok()
        return wrapper

    async def receiver(self):
        _log.debug("In receiver block")
        while not self._stop_event.is_set():
            _log.debug("Receiving message")
            try:
                message, addr = self._socket.recvfrom(self._buffer_size)
                _log.info("Message received from {}: {}".format(
                    addr, message.strip()))
            except socket.error as error:
                error_id = error.args[0]
                if error_id == errno.EAGAIN or error_id == errno.EWOULDBLOCK:
                    _log.debug("Would block: sleeping")
                    await sleep(0.2)
                else:
                    _log.exception("Error while fetching SCPI message")
                    raise error
            except Exception as error:
                _log.exception("Error while fetching SCPI message")
                raise error
            else:
                _log.debug("Creating request")
                request = ScpiRequest(message, addr, self._socket)
                _log.debug("putting request on queue")
                try:
                    await self._request_queue.put(request)
                except Exception as error:
                    print(error)
                _log.debug("request enqueued")

    async def dispatcher(self):
        _log.debug("In dispatcher block")
        while not self._stop_event.is_set():
            _log.debug("fetching from queue")
            req = await self._request_queue.get()
            _log.debug("got request: {}".format(req))
            try:
                handler_name = "request_{}".format(
                    req.command.replace(":", "_"))
            except Exception as error:
                print(error)
            _log.debug("Searching for handler '{}'".format(handler_name))
            try:
                handler = self.__getattribute__(handler_name)
            except AttributeError:
                _log.error("No handler named '{}'".format(handler_name))
                req.error("UNKNOWN COMMAND")
                raise UnhandledScpiRequest(req.command)
            except Exception as error:
                _log.error(
                    "Exception during handler lookup: {}".format(
                        str(error)))
                req.error(str(error))
                raise error
            else:
                _log.debug('Executing handler: {}'.format(handler))
                try:
                    await handler(req)
                except Exception as error:
                    print(error)
                    raise
                _log.debug('Handler called')

    async def start(self):
        """
        @brief      Start the SCPI interface listening for new commands
        """
        _log.info("Starting SCPI interface")
        try:
            self._create_socket()
        except Exception as error:
            print(error)
        _log.info("Socket created")
        self._stop_event.clear()
        self._flush()
        self._receiver_task = asyncio.create_task(self.receiver())
        _log.info("Receiving")
        self._dispatcher_task = asyncio.create_task(self.dispatcher())

    async def stop(self):
        """
        @brief      Stop the SCPI interface listening for new commands
        """
        _log.debug("Stopping SCPI interface")
        self._stop_event.set()
        if self._receiver_task:
            await self._receiver_task
            self._receiver_task = None
        if self._dispatcher_task:
            await self._dispatcher_task
            self._dispatcher_task = None
        if self._socket:
            self._socket.close()
            self._socket = None


def scpi_request(*types):
    def wrapper(func):
        @functools.wraps(func)
        async def request_handler(obj, req):
            if len(types) != len(req.args):
                message = "Expected {} arguments but received {}".format(
                    len(types), len(req.args))
                _log.error(message)
                req.error("INCORRECT NUMBER OF ARGUMENTS")
                raise MalformedScpiRequest(message)
            new_args = []
            for _, (arg, type_) in enumerate(zip(req.args, types)):
                try:
                    new_args.append(type_(arg))
                except Exception as error:
                    _log.error(str(error))
                    req.error("UNABLE TO PARSE ARGUMENTS")
                    raise MalformedScpiRequest(str(error))
            await func(obj, req, *new_args)
        return request_handler
    return wrapper
