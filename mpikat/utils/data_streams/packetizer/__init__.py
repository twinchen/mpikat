from mpikat.core.data_stream import DataStream, DataStreamRegistry, edd_spead


class PacketizerStreamFormat(DataStream):
    """
    Format description oof Redis Spectrum stream
    """
    samples_per_heap = 4096

    stream_meta = {
        "type": "MPIFR_EDD_Packetizer:1",
        "ip": "",
        "port": "",
        "bit_depth" : 8,
        "sample_rate" : 0,
        "sync_time" : 0,
        "band_flip": False,
        "central_freq": 0,
        "receiver_id": "",
        "polarization": None,
        "samples_per_heap": samples_per_heap,
        "description": "Spead stream of time-domain packetizer data as in EDD ICD."
    }

    data_items = [edd_spead(5632, "timestep"),
                  edd_spead(5633, "polarization"),
                  edd_spead(5634, "payload")]

DataStreamRegistry.register(PacketizerStreamFormat)