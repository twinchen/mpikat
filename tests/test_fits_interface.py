
import dataclasses
import unittest
from aiokatcp.client import Client

import numpy as np

from mpikat.pipelines.fits_interface import FitsInterfaceServer, PrepPack, FitsPacketMerger
from mpikat.pipelines.mock_fits_writer import MockFitsWriter
from mpikat.utils import get_port
from mpikat.utils import testing


HOST = "127.0.0.1"
class Test_FitsInterface(unittest.IsolatedAsyncioTestCase):

    def setUp(self) -> None:
        self.sender_port = get_port()
        self.sender_thread = testing.launch_server_in_thread(FitsInterfaceServer, HOST, self.sender_port)
        self.receiver_port = get_port()
        self.receiver_thread = testing.launch_server_in_thread(MockFitsWriter, HOST, self.receiver_port)
        self.sender = self.sender_thread.server
        self.receiver = self.receiver_thread.server

    async def asyncSetUp(self):
        self.sender_client = await Client.connect(HOST, self.sender_port)
        self.receiver_client = await Client.connect(HOST, self.receiver_port)

    def tearDown(self) -> None:
        self.sender_client.close()
        self.receiver_client.close()

        self.sender_thread.stop().result()
        self.sender_thread.join(timeout=5)

        self.receiver_thread.stop().result()
        self.receiver_thread.join(timeout=5)

    async def test_state_machine_sequenceof_fits_writer_interface_and_mock(self):
        """Test if expected states are assumed by according commands"""
        self.assertEqual(self.sender.state, "idle")
        self.assertEqual(self.receiver.state, "idle")

        await self.sender_client.request('configure', '{"id":"sender","enable_plotting":false}')
        await self.receiver_client.request('configure', '{"id":"receiver"}')
        self.assertEqual(self.sender.state, "configured")
        self.assertEqual(self.receiver.state, "configured")

        await self.sender_client.request('capture-start')
        await self.receiver_client.request('capture-start')
        self.assertEqual(self.sender.state, "ready")
        self.assertEqual(self.receiver.state, "ready")

        await self.sender_client.request('measurement-prepare')
        await self.receiver_client.request('measurement-prepare')
        self.assertEqual(self.sender.state, "set")
        self.assertEqual(self.receiver.state, "set")

        await self.sender_client.request('measurement-start')
        await self.receiver_client.request('measurement-start')
        self.assertEqual(self.sender.state, "measuring")
        self.assertEqual(self.receiver.state, "measuring")

        await self.sender_client.request('measurement-stop')
        await self.receiver_client.request('measurement-stop')
        self.assertEqual(self.sender.state, "ready")
        self.assertEqual(self.receiver.state, "ready")

        await self.sender_client.request('deconfigure')
        await self.receiver_client.request('deconfigure')
        self.assertEqual(self.sender.state, "idle")
        self.assertEqual(self.receiver.state, "idle")

@dataclasses.dataclass
class SpeadPacketMock:
    nchannels: int
    polarization: int=0
    reference_time: float=0
    number_of_input_samples: int=0
    integration_period: float=0
    noise_diode_status: int=0
    sampling_rate: float=2E9

    def __post_init__(self):
        self.data = np.zeros(self.nchannels, dtype='float32')



class TestPrepPack(unittest.TestCase):

    def test_call_counter_check(self):
        """ Should raise error if too many spectra are added"""
        nchannels = 1025
        npolarizations = 2
        prep_pack = PrepPack(npolarizations, [(1, None)], 0)

        for p in range(npolarizations):
            s = SpeadPacketMock(nchannels=nchannels, polarization=p)
            prep_pack.addSpectrum(s)

        with self.assertRaises(RuntimeError):
            prep_pack.addSpectrum(s)


    def test_single_band_dualpol(self):

        nchannels = 1025
        npolarizations = 2
        prep_pack = PrepPack(npolarizations, [(1, None)], 0)

        for p in range(npolarizations):
            s = SpeadPacketMock(nchannels=nchannels, polarization=p)
            prep_pack.addSpectrum(s)


        # should have two sections of equal length
        self.assertEqual(len(prep_pack.payload.sections), 2)
        self.assertEqual(len(prep_pack.payload.sections[0].data), 1024)
        self.assertEqual(len(prep_pack.payload.sections[1].data), 1024)

        self.assertTrue(prep_pack.valid)


    def test_invalidation_for_nan(self):
        nchannels = 1025
        npolarizations = 2
        prep_pack = PrepPack(npolarizations, [(1, None)], 0)

        s = SpeadPacketMock(nchannels=nchannels, polarization=0)
        s.data[0] = np.NaN
        prep_pack.addSpectrum(s)
        self.assertFalse(prep_pack.valid)


    def test_multi_band_dualpol(self):

        nchannels = 1025
        npolarizations = 2
        prep_pack = PrepPack(npolarizations, [(1, 100), (200, 400)], 0)

        for p in range(npolarizations):
            s = SpeadPacketMock(nchannels=nchannels, polarization=p)
            s.number_of_input_samples = 1
            s.data = np.linspace(0, nchannels-1, nchannels, dtype='float32')
            prep_pack.addSpectrum(s)


        # should have two sections of equal length
        self.assertEqual(len(prep_pack.payload.sections), 4)
        self.assertEqual(len(prep_pack.payload.sections[0].data), 99)
        self.assertEqual(len(prep_pack.payload.sections[1].data), 200)

        self.assertEqual(len(prep_pack.payload.sections[2].data), 99)
        self.assertEqual(len(prep_pack.payload.sections[3].data), 200)
        self.assertEqual(prep_pack.payload.sections[0].data[0], 1)
        self.assertEqual(prep_pack.payload.sections[1].data[0], 200)
        self.assertEqual(prep_pack.payload.sections[2].data[-1], 99)
        self.assertEqual(prep_pack.payload.sections[3].data[-1], 399)

        self.assertTrue(prep_pack.valid)



class TestFitsPacketMerger(unittest.TestCase):
    def setUp(self):
        self.fits_sender = unittest.mock.Mock()
        self.data_plotter = unittest.mock.Mock()

    def test_adding_package_to_prep_package(self):
        """Finalized packets are removed from preparation list"""
        fpm = FitsPacketMerger(self.fits_sender, self.data_plotter, 2, [[1, None]], 0.1)

        nchannels = 1000

        s = SpeadPacketMock(nchannels=nchannels, polarization=0)
        s.number_of_input_samples = 1
        s.data = np.linspace(0, nchannels-1, nchannels, dtype='float32')

        self.assertEqual(len(fpm.packages_in_preparation), 0)
        fpm(s)
        self.assertEqual(len(fpm.packages_in_preparation), 1)

        s = SpeadPacketMock(nchannels=nchannels, polarization=1)
        s.number_of_input_samples = 1
        s.data = np.linspace(0, nchannels-1, nchannels, dtype='float32')

        fpm(s)
        # package should be final after two pols 
        self.assertEqual(len(fpm.packages_in_preparation), 0)


    def test_progress_of_time_in_merger(self):
        """Time of merger is defined by latest input packet"""
        fpm = FitsPacketMerger(self.fits_sender,  self.data_plotter, 2, [[1, None]], 0.0)
        nchannels = 1000

        fpm(SpeadPacketMock(nchannels=nchannels, polarization=0, reference_time=1))
        self.assertEqual(fpm.now, 1)

        fpm(SpeadPacketMock(nchannels=nchannels, polarization=0, reference_time=0))
        self.assertEqual(fpm.now, 1)

        fpm(SpeadPacketMock(nchannels=nchannels, polarization=0, reference_time=2))
        self.assertEqual(fpm.now, 2)

    def test_adding_of_noise_time_delta(self):
        """Noise time delat should be added to nose diode 0 only"""

        fpm = FitsPacketMerger(self.fits_sender,  self.data_plotter, 2, [[1, None]], 0.0123)
        nchannels = 1000

        fpm(SpeadPacketMock(nchannels=nchannels, polarization=0, reference_time=1))
        self.assertEqual(fpm.now, 1.0123)

        # Delta only added for noise diode status 0
        fpm(SpeadPacketMock(nchannels=nchannels, polarization=0, reference_time=5, noise_diode_status=1))
        self.assertEqual(fpm.now, 5.)


    def test_clean_queue(self):
        """Too old packages should be cleaned from preparation list"""
        fpm = FitsPacketMerger(self.fits_sender,  self.data_plotter, 2, [[1, None]], 0.0123)
        nchannels = 1000

        fpm(SpeadPacketMock(nchannels=nchannels, polarization=0, reference_time=1))
        self.assertEqual(fpm.now, 1.0123)
        self.assertEqual(len(fpm.packages_in_preparation), 1)


        fpm(SpeadPacketMock(nchannels=nchannels, polarization=0, reference_time=5))
        self.assertEqual(fpm.now, 5.0123)
        self.assertEqual(len(fpm.packages_in_preparation), 2)

        fpm(SpeadPacketMock(nchannels=nchannels, polarization=0, reference_time=12))
        self.assertEqual(fpm.now, 12.0123)
        self.assertEqual(len(fpm.packages_in_preparation), 2)


if __name__ == '__main__':
    unittest.main()
