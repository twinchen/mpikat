import time
import asyncio
import tempfile
from mpikat.utils.dada_tools.reader import DadaClient

DADA_HEADER ="""
HEADER DADA
HDR_VERSION 1.0
HDR_SIZE 4096
DADA_VERSION 1.0
FILE_SIZE unset
UTC_START unset
NBIT 8
"""

def make_default_header(lines: list=[]):
    """Creates a simple default header. ToDo: Not necessarly a class member

    Returns:
        string: The filename as string of the DADA default header

    """

    with tempfile.NamedTemporaryFile(delete=False, mode="w") as fid:
        fid.write(DADA_HEADER)
        for line in lines:
            fid.write(line)
    return fid.name
    
class DadaWriteClient(DadaClient):
    """
    is derived from DadaClient and the base class for all writing clients.
    Writing clients are clients which are writing TO the DADA buffer. It
    implement the method wait(), allowing to wait until the CLI program
    is exited.
    """
    def __init__(self, key, stdout_handler=None, physcpu=None):
        """Base class for Writing clients

        Parameters
            key (str): The hex key used for the DADA buffer
            stdout_handler (callable): A function that parses the standard output
                of the client

        Returns:
            DadaWriteClient: An instance of the the DadaWriteClient class.

        """
        DadaClient.__init__(self, key, stdout_handler)

    def wait(self):
        """Waits for the wirting client to finish its task."""
        while self.proc.is_alive():
            time.sleep(1)

    # async def wait(self):
    #     """Waits for the wirting client to finish its task."""
    #     while self.proc.is_alive():
    #         await asyncio.sleep(1)



class DiskDb(DadaWriteClient):
    """
    is a write client,  whic reads the contents of a file and writes it to
    the attached DADA buffer
    """
    def __init__(self, key, fname="/tmp/test.dat", header="", n_reads=1,
                 data_rate=0, stdout_handler=None, physcpu=None):
        """is a write client, which reads the contents of a file and writes it
            to the attached DADA buffer

        Parameters:
            key (str): see DadaClient
            fname (str): The file that should be read and written to the buffer
            header (str): Name of the header file if any
            n_reads (int): Number of read repitions of the file
            data_rate (int): The data rate to read the file
            physcpu (str): see DadaClient
            stdout_handler (callable): see DadaClient

        Returns:
            DbNull: An instance of the the DbNull class.

        """
        DadaWriteClient.__init__(self, key, stdout_handler, physcpu)
        self.fname = fname
        self.n_reads = n_reads
        self.header = header
        if self.header == "":
            self.header = make_default_header()
        self.data_rate = data_rate
        self.cmd += "diskdb -k {}".format(self.key)
        self.cmd += " -d {}".format(self.fname)
        self.cmd += " -f {}".format(self.header)
        self.cmd += " -r {}".format(self.n_reads)
        self.cmd += " -s {}".format(self.data_rate)


class SyncDb(DadaWriteClient):
    """
    is a write client, which fills multiple DADA buffer synchronously.
    """
    def __init__(self, key, n_bytes=0, sync_epoch=0, period=1,
                 ts_per_block=1048576, stdout_handler=None, physcpu=None):
        """is a write client, which fills multiple DADA buffer synchronously.
        Parameters:
            key (str): see DadaClient
            n_bytes (int):
            sync_epoch (int): The start time to sync to
            period (int):
            ts_per_block (int):
            physcpu (str): see DadaClient
            stdout_handler (callable): see DadaClient

        Returns:
            DbNull: An instance of the the DbNull class.
        """
        DadaWriteClient.__init__(self, key, stdout_handler, physcpu)
        self.n_bytes = int(n_bytes)
        self.sync_epoch = int(sync_epoch)
        self.period = float(period)
        self.ts_per_block = int(ts_per_block)
        self.cmd += "syncdb -k {}".format(self.key)
        self.cmd += " -n {}".format(self.n_bytes)
        self.cmd += " -s {}".format(self.sync_epoch)
        self.cmd += " -p {}".format(self.period)
        self.cmd += " -t {}".format(self.ts_per_block)


class JunkDb(DadaWriteClient):
    """
    is a write client, which fills a single DADA buffer with "junk".
    """
    def __init__(self, key, header="", data_rate=64e6, duration=60,
                 charachter="", zero_copy=True, gaussian=False,
                 stdout_handler=None, physcpu=None):
        """is a write client, which fills a single DADA buffer with "junk"

        Parameters:
            key (str): see DadaClient
            header (str): The DADA header if any
            data_rate (int):
            duration (int):
            charachter (char):
            zero_copy (bool):
            gaussian (bool):
            physcpu (str): see DadaClient
            stdout_handler (callable): see DadaClient

        Returns:
            DbNull: An instance of the the DbNull class.
        """
        DadaWriteClient.__init__(self, key, stdout_handler, physcpu)
        self.header = header
        if self.header == "":
            self.header = make_default_header()
        self.data_rate = data_rate
        self.duration = duration
        self.cmd += "dada_junkdb -k {}".format(self.key)
        self.cmd += " -r {}".format(int(self.data_rate//(1e6)))
        self.cmd += " -t {}".format(self.duration)
        if zero_copy:
            self.cmd += " -z "
        if gaussian:
            self.cmd += " -g "
        if charachter:
            self.cmd += " -c {}".format(charachter)
        self.cmd += " "  + str(self.header)
