from abc import ABC, abstractmethod
from mpikat.core import logger

_log = logger.getLogger('mpikat.utils.testing.provision_tests.OutputProcessor')

class OutputProcessor(ABC):
    """
    Processor for test rest results
    """

    @abstractmethod
    def print_head(self, summary):
        """
        Write header information
        """


    @abstractmethod
    def print_test_result(self, test):
        """
        Write result of a single test to the output
        """

    @abstractmethod
    def print_stage(self, summary):
        """
        Print results of a stage.
        """

    def run(self, summary):
        """
        Run all test results.
        """
        self.print_head(summary)
        for s in summary['stage_results']:
            self.print_stage(s)



class MultiOutput(OutputProcessor):
    """
    Output test to multiple outputs
    """
    def __init__(self, *args):
        self.outputs = args

    def print_head(self, summary):
        _log.debug('MultiHead: print_head called')
        for o in self.outputs:
            o.print_head(summary)

    def print_test_result(self, test):
        _log.debug('MultiHead: print_test_result called')
        for o in self.outputs:
            o.print_test_result(test)

    def print_stage(self, summary):
        _log.debug('MultiHead: print_stage called')
        for o in self.outputs:
            o.print_stage(summary)
