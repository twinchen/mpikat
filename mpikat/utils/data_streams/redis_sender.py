
import multiprocessing as mp
import redis

from mpikat.core.logger import getLogger

_LOG = getLogger("mpikat.utils.data_streams.redis_sender")

class RedisJSONSender(mp.Process):
    """
    A multprocessing.Process implementation to calculate spectra and send them to a redis server via pub/sub
    """

    def __init__(self, ip: str, port: int, db: int, product_name: str,
                 queue:mp.Queue, callback: callable=None):
        """
        Constructor
        """
        super().__init__()
        self.pname = product_name
        self.ip = ip
        self.port = port
        self.db = db
        self.streams = set()
        self.callback = callback
        self._quit = mp.Event()
        self._queue = queue
        self._connected = False

    def connect(self):
        """Connects the stream to the redis database
        """
        try:
            _LOG.info("Connecting to redis %s:%d/%d", self.ip, self.port, self.db)
            self._redis = redis.Redis(self.ip, self.port, db=self.db)
            self._redis.ping()
        except Exception as e:
            raise ConnectionError("Connection to redis %s:%d/%d failed with %s",
                                  self.ip, self.port, self.db, e)
        self._client = self._redis.json()
        self._connected = True

    def send_json(self, d: dict) -> None:
        """Sends a dictonary to the connect redis instance via redisJSON

        Args:
            d (dict): The dictionary to send
        """
        if self._connected:
            _LOG.info("Sending stream %s to DB", d["name"])
            ds_name = f"{self.pname}:{d['name']}_stream"
            self._client.set(ds_name, '$', d)
            self.streams.add(ds_name)
            return
        raise ConnectionError("Not connected to redis")

    def stop(self):
        """Stops the transmission by setting mp.Event()
        """
        self._quit.set()
        # Remove entry after stop
        for name in self.streams:
            self._client.delete(name)
        self.streams = set()

    def run(self):
        """
        Function to run the processing and transmission of spectra
        """
        _LOG.info(f"Starting stream transmission in {mp.current_process().name}")
        while not self._quit.is_set():

            if self._queue.full():
                _LOG.warning("Queue is full, flushing..")
                while not self._queue.empty():
                    self._queue.get()

            if not self._queue.empty():
                if self.callback is not None:
                    qitem = self.callback(self._queue.get())
                else:
                    qitem = self._queue.get()
                if isinstance(qitem, dict):
                    self.send_json(qitem)
