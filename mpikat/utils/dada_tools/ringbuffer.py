import asyncio
from subprocess import Popen, PIPE

from mpikat.utils.process_tools import command_watcher
from mpikat.utils.dada_tools.monitor import DbMonitor
import mpikat.utils.async_util # pylint: disable=unused-import
import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.utils.dada_tools.ringbuffer")

# ---------------- #
# Helper functions #
# ---------------- #

def int2key(key: int) -> str:
    """Convert a integer number to a DADA key

    Args:
        key (int): key in integer representation

    Returns:
        str: DADA key
    """
    return str(hex(key)).replace("0x", "")

def hex2key(key: str) -> str:
    """convert a hey string into a dada key
    Example:
        '0x0000dada' is returned as 'dada'
    Args:
        key (str): hexadecimal string

    Returns:
        str: DADA key
    """
    return hex(int(key,16))[2:]

def key2int(key: str) -> int:

    """convert a dada key to an int representation
    Example:
        'dada' is returned as 56026
    Args:
        key (str): hexadecimal string

    Returns:
        int: DADA key in int
    """
    return int(key, 16)

def occupied(exclude: set=None):
    """
    Queries the system for occupied shared memory addresses by the use of
    'ipcs -m', stores the result and returns a list of occupied addresses.

    Args:
        exclude (set): A set of dada keys to exclude

    Returns:
        Returns a set of occupied dada keys
    """
    occ = set([])
    proc = Popen(["ipcs", "-m"], stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate()
    stdout = stdout.decode().split("\n")

    if not exclude:
        exclude = set()
    if stderr.decode():
        raise RuntimeError(f"ipcs -m failed with error code: {stderr.decode()}")
    for line in stdout:
        if line[0:2] != "0x":
            continue
        items = line.split(" ")
        occ.add(hex2key(items[0]))
    return occ | exclude

def get_key(n_keys=1, start='dada', width=1024, step=2, exclude=None):
    """Searches for a non-occupied DADA key.

    Args:
        n_keys (int): number of keys
        start (str): The first possible DADA key
        width (int): Defines the last possible DADA key (start+width)
        step  (int): Defines the distance between two DADA keys. For instance,
            if step = 2 and the key 'dada' is occupied it will return 'dadc'
        exclude (set): A set of dada keys to exclude

    Returns:
        str: Returns a free DADA key, if n=1,
        list: Return a list of DADA keys, if n > 1 or empty list if no key was found
    """
    occ = occupied(exclude)
    key = key2int(start)
    width = int(width)
    step = int(step)
    keys = []
    for k in range(key, key+width, step):
        if int2key(k) not in occ:
            if n_keys == 1:
                return int2key(k)
            elif len(keys) == n_keys:
                return keys
            keys.append(int2key(k))
    _log.warning("No dada keys available")
    return keys

def is_free(key):
    """Checks if the given key is free

    Parameters
        key (int or str): The key to check for

    Returns:
        Return True if the keys is free and False if the key is occupied
    """
    occ = occupied()
    if key in occ or key2int(key) in occ:
        return False
    return True

def rm_buffers():
    async def rm_buffers_wrapper():
        occ = occupied()

        tasks = []
        for key in occ:
            buf = DadaBuffer(key=hex2key(key), n_bytes=0)
            tasks.append(asyncio.create_task(buf.destroy()))
            _log.info("Removing key %s", key)
        await asyncio.gather(*tasks)
    asyncio.run(rm_buffers_wrapper())

class DadaBuffer():
    __USED_KEYS = set()
    """
    Wraps the 'dada_db' CLI into a class. An instance of this class is able to
    create, destroy and monitor a DADA buffer

    Methods:
        __init__(n_bytes, key=None, n_slots=16, persistance=False, n_reader=1,
                 gpu_id=None, node_id=None, lock=False, pin=False)
        create()
        destroy()
        get_monitor(callback)
        is_alive()

    Attributes:
        key (str): The dada key
        cmd (str): The command line string to create the dada buffer

    """
    def __init__(self, n_bytes, key=None, n_slots=16, persistance=False,
                 n_reader=1, gpu_id=None, node_id=None, lock=False,
                 pin=False):
        """ Constructs an DadaBuffer object

        Parameters:
            n_bytes (int): Number of bytes per DADA buffer slot.
            key (str) or None: If None get_key() is used to get a free key.
            n_slots (int): Number of slots in the ringbuffer.
            persistance (bool): If True, waits for a signal before destruction.
            gpu_id (int) or None: If int the ringbuffer is allocate on GPU
                with the given ID.
            node_id (int) or None: If int the DADA buffer is bound to the NUMA
                node with the selected ID.
            lock (bool): If True locks the rungbuffer in RAM.
            pin (bool): If True pinns/pages the ringbuffer into RAM
                (Used for async copy from/to Host to/from GPU).

        Returns:
            DadaBuffer: An instance of the the DadaBuffer class.

        """
        self.node_id = node_id
        self.n_reader = n_reader
        self.gpu_id = gpu_id
        self.persistance = persistance
        self.lock = lock
        self.pin = pin
        self.n_bytes = n_bytes
        self.n_slots = n_slots
        self.n_reader = n_reader
        self._key = key
        self._cmd = ""
        self._created = False
        self._monitor = None

    @classmethod
    def used(cls) -> list:
        """Return all occupied keys by the DadaBuffer class.
        Note: It will not return the all keys occupied by the system. Use occupied() insted

        Returns:
            list: list of occupied keys
        """
        keys = []
        for key in cls.__USED_KEYS:
            keys.append(key)
        return keys

    @property
    def key(self) -> str:
        """Returns the actual used key by the DadaBuffer object.

        Returns:
            str: the key
        """
        if self._key is None:
            self._key = get_key(exclude=self.__USED_KEYS)
            # Only a single key is accpeted
            if isinstance(self._key, list):
                raise RuntimeError("Could not find dada key")
            self.__USED_KEYS.add(self._key)
        return self._key

    @property
    def cmd(self):
        cmd = []
        # Construct the command
        if self.node_id is not None:
            cmd.append(f'numactl --cpubind={self.node_id} --membind={self.node_id}')
        cmd.append(f'dada_db -k {self.key}')
        cmd.append(f'-b {self.n_bytes}')
        cmd.append(f'-n {self.n_slots}')
        if self.n_reader < 1:
            _log.error("No readers attached!")
            raise RuntimeError("No readers attached!")
        cmd.append(f'-r {self.n_reader}')
        if self.gpu_id:
            cmd.append(f'-g {self.gpu_id}')
        if self.persistance:
            cmd.append(f'-w')
        if self.lock:
            cmd.append(f'-l')
        if self.pin:
            cmd.append(f'-p')
        return ' '.join(cmd)

    async def create(self):
        """ Creates the DADA buffer asynchronously and attaches a DbMonitor
        object to the buffer.

        Note:
            If a DADA buffer with the same key already existis on the system,
            this method will destroy the previously created buffer and create a
            new buffer with the given command.

        """
        # Delete dada buffer if exists
        if self.key in occupied():
            _log.warning("A ipcs buffer already exists with %s",self._key)
            await self.destroy()
        _log.info("Creating dada buffer with command: %s", self.cmd)
        await command_watcher(self.cmd)
        self._created = True

    async def destroy(self):
        """Destroys the DADA buffer asychronously.

        Note:
            If a DbMonitor instance was attached to the buffer the method will
            stop the DbMonitor object before destruction.

        """
        _log.info("Destroying dada buffer with key: %s", self.key)
        if isinstance(self._monitor, DbMonitor):
            await asyncio.to_thread(self._monitor.stop)

        await command_watcher(f"dada_db -k {self.key} -d", allow_fail=~self._created)
        if self._key in self.__USED_KEYS:
            self.__USED_KEYS.remove(self._key)
        self._created = False

    async def reset(self):
        """Resets a whole dada buffer. Can be used to avoid re-allocation.
            All meta information are reset, but data in the buffer is not overwritten.

        Note: Only works with psrdada_cpp version 07417574987e27008979a6e0a3783739d86947a1 or later
        """
        _log.info("Resseting dada buffer with key: %s", self.key)
        await command_watcher(f"dbreset -k {self.key}", allow_fail=~self._created)

    def get_monitor(self, callback=None):
        """ This method instantiates an object of DbMonitor if not exists
        and a dada buffer was already created. There can only be one monitor
        at a time per DadaBuffer object.

        Parameters
            callback (callable or list of callables): Callback functions which
                handles the standard output of 'dada_dbmonitor'-CLI

        Returns
            DbMonitor: Returns an object of DbMonitor which is also stored as
            protected class attribute.

        """
        if not isinstance(self._monitor, DbMonitor) and self._created:
            self._monitor = DbMonitor(self._key, callback)
        return self._monitor

    def is_alive(self):
        """Checks if this object already has created a DADA buffer

        Returns:
            bool: True if the buffer was created, False if not.

        """
        return self._created
