import unittest
import sys
import asyncio
import threading

def dummy_sleep():
    return 1337, threading.get_ident()

class Test_AsyncUtil(unittest.IsolatedAsyncioTestCase):

    def test_to_thread_patch(self):
        """Should test that 'to_thread' exists"""
        if not hasattr(asyncio, 'to_thread'):
            import mpikat.utils.async_util
        self.assertTrue(hasattr(asyncio, 'to_thread'))

    async def test_to_thread_is_thread(self):
        """Should test that to_thread runs in a thread
        """
        import mpikat.utils.async_util
        res, tid = await asyncio.to_thread(dummy_sleep)
        self.assertEqual(res, 1337)
        self.assertNotEqual(tid, threading.get_ident())


if __name__ == "__main__":
    unittest.main()
