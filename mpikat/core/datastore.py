import json
import argparse
import sys
import time

import redis

import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.core.datastore")


class redisfail2warn:
    """
    Context manager that turns Redis connection errors into warnings.
    """
    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, traceback):
        if isinstance(exc_val, redis.exceptions.ConnectionError):
            _log.warning("Redis connection error\n\n{}\n\n - request ignored.".format(exc_val))
            return True
        return False



class EDDDataStore:
    """
    Interface to the data store for the EDD.

    The data store contains the current state of the EDD, augmented
    with additional data of the current state of telescope needed by products.

    The data is stored in different Redis data bases.
    """
    def __init__(self, host: str, port: int=6379):
        _log.debug("Init data store connection: {}:{}".format(host, port))

        common_redis_settings = {"host": host, "port": port,
                                 "socket_timeout": 10.,
                                 "decode_responses": True}

        # The data collected by the Ansible configuration
        self._ansible = redis.StrictRedis(db=0, **common_redis_settings)
        # The currently configured data producers
        self._products = redis.StrictRedis(db=1, **common_redis_settings)
        # The currently configured data streams (JSON objects)
        self._dataStreams = redis.StrictRedis(db=2, **common_redis_settings)
        # EDD Static data
        self.edd_data = redis.StrictRedis(db=3, **common_redis_settings)
        # Telescope meta data
        self._telescopeMetaData = redis.StrictRedis(db=4, **common_redis_settings)

        self.__dataBases = [self._ansible, self._products, self._dataStreams,
                            self.edd_data, self._telescopeMetaData]
        for d in self.__dataBases:
            with redisfail2warn():
                d.ping()

    def ping(self):
        """Raise connection error if Redis is not available
        """
        try:
            for d in self.__dataBases:
                d.ping()
        except redis.exceptions.ConnectionError as E:
            raise ConnectionError from E

    def flush(self):
        """
        Flush content of all databases.
        """
        _log.debug("Flushing all databses")
        for d in self.__dataBases:
            with redisfail2warn():
                d.flushdb()

    def addDataStream(self, streamid: str, streamdescription: dict):
        """
        Add a new data stream to the store. Description as dict.
        """
        _log.debug("Adding datastream: {}".format(streamid))
        with redisfail2warn():
            if streamid in self._dataStreams:
                nd = json.dumps(streamdescription)
                if nd == self._dataStreams[streamid]:
                    _log.debug("Duplicate output streams: %s defined but with same description", streamid)
                    return
                _log.warning("Duplicate output stream %s defined with conflicting description!\n Existing description: %s\n New description: %s", streamid, self._dataStreams[streamid], nd)
                #raise RuntimeError("Invalid configuration")
            self._dataStreams[streamid] = json.dumps(streamdescription)


    def getDataStream(self, streamid: str) -> str:
        """
        Return data stream with stream-id as dict.
        """
        return json.loads(self._dataStreams[streamid])


    def removeProduct(self, product_config: dict):
        """Remove a product and its datastreams"""
        netid = "{ip}:{port}".format(**product_config)
        self._products.delete(netid)
        if "output_data_streams" in product_config:
            _log.debug("Remove data streams for product")
            for k in product_config["output_data_streams"]:
                key = f"{product_config['id']}:{k}"
                self._dataStreams.delete(key)


    def updateProduct(self, cfg: dict):
        """
        Updates the global product database for a product with a given config.
        """
        c = dict.fromkeys(['id', 'type', 'ip', 'port'])
        for k in c.copy():
            if k in cfg:
                c[k] = cfg[k]

        self._products.hset(c['id'], mapping=c)


    @property
    def products(self):
        """
        List of all product ids.
        """
        d = []
        # Create dict with id as key
        for k in self._products.keys():
            d.append(self._products.hgetall(k))
        return d

    def get_product_netid(self, product: str):
        """
        Get IP:port of product
        """
        return self._products.hmget(product, 'ip', 'port')


    def hasDataStream(self, streamid: str) -> bool:
        """
        True if data stream with given id exists.
        """
        return streamid in self._dataStreams

    def addDataFormatDefinition(self, format_definition: dict):
        """
        Adds a new data format description dict to store.
               All formats have its name as key.
        """
        with redisfail2warn():
            key = f"DataFormats:{format_definition['format']}"
            if key in self.edd_data:
                _log.warning("Data format already defined.")

            params = json.dumps(format_definition)
            _log.debug("Add data format definition %s - %s", key, params)
            self.edd_data[key] = params

    def hasDataFormatDefinition(self, format_name: str):
        """
        Check if data format description already exists.
        """
        key = f"DataFormats:{format_name}"
        return key in self.edd_data

    def getDataFormatDefinition(self, format_name: str):
        """
        Returns data format description as dict.
        """
        key = f"DataFormats:{format_name}"
        if key in self.edd_data:
            return json.loads(self.edd_data[key])
        _log.warning("Unknown data format: - %s", key)
        return {}

    def addTelescopeDataItem(self, key: str, pars: dict):
        """Add a new item describing telescope data"""
        _log.debug("Add telescope data item %s: %s", key, pars)
        with redisfail2warn():
            pars['value'] = pars['default']
            if not 'timestamp' in pars:
                pars['timestamp'] = time.time()
            try:
                self._telescopeMetaData.hset(key, mapping=pars)
            except Exception as E:
                _log.error("Error when setting key %s, %s", key, E)

    def setTelescopeDataItem(self, key: any, value: any, timestamp: any=None, field: str='value'):
        """Set an item describing telescope data"""
        with redisfail2warn():
            self._telescopeMetaData.hset(key, field, value)
            if not timestamp:
                timestamp = time.time()
            self._telescopeMetaData.hset(key, "timestamp", timestamp)

    def getTelescopeDataItem(self, key: any, field: str="value"):
        """Get an item describing telescope data"""
        return self._telescopeMetaData.hget(key, field)

def main():
    """program part to register products to the Redis data store"""
    parser = argparse.ArgumentParser(description="commandline access to data store")
    parser.add_argument('--redis-ip', dest='redis_ip', type=str, default="localhost",
                        help='The ip for the redis server')
    parser.add_argument('--redis-port', dest='redis_port', type=int, default=6379,
                        help='The port number for the redis server')
    parser.add_argument('--register_product', dest='product_config', type=str,
                        help='Product config for the registration id=ProductId;ip=HOSTNAME;port=port')
    parser.add_argument('--get_product_netid', type=str,
                        help='Product config for the registration id=ProductId;ip=HOSTNAME;port=port')

    args = parser.parse_args()

    dataStore = EDDDataStore(args.redis_ip, args.redis_port)

    if args.product_config:
        cfg = {}
        print("Updating product info;")
        for t in args.product_config.split(';'):
            k, v = t.split('=')
            print(" {}: {}".format(k, v))
            cfg[k] = v
        dataStore.updateProduct(cfg)

    elif args.get_product_netid:
        netid = dataStore.get_product_netid(args.get_product_netid)
        if not netid[0]:
            sys.exit(-1)
        print('{}:{}'.format(*netid))


if __name__ == "__main__":
    main()
