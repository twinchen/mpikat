import os
import threading
import socket
import time
from queue import Queue

from mpikat.core import logger
from mpikat.utils.fits.format import FitsPacket


_log = logger.getLogger("mpikat.utils.fits.receiver")

def recv_nbytes(sock: socket.socket, nbytes: int) -> bytes:
    """Receives N bytes from a socket

    Args:
        sock (socket.socket): The socket to receive from
        nbytes (int): The number of bytes

    Raises:
        socket.error: Error

    Returns:
        bytes: The bytes to receive
    """
    data = b''
    while len(data) < nbytes:
        _log.debug("Requesting %s bytes", nbytes - len(data))
        try:
            data += sock.recv(nbytes - len(data))
        except socket.error as error:
            raise error
    return data


def recv_packet(sock: socket.socket) -> FitsPacket:
    """Receives and returns a FITS Packet

    Args:
        sock (socket.socket): The socket to receive from

    Returns:
        FitsPacket: Object containing the packet
    """
    packet = FitsPacket()
    try:
        packet.setHeader(recv_nbytes(sock, packet.header_size))
        packet.setPayload(recv_nbytes(sock, packet.payload_size))
    except socket.error as error:
        if isinstance(error, socket.timeout):
            _log.debug("Socket timed out after %d seconds, no data acquired", sock.gettimeout())
            return None
        else:
            raise error
    return packet


class Receiver(threading.Thread):
    """
    Class for receiving and storing FITS packets
    """
    def __init__(self, address: tuple, directory: str="", queue: Queue=None):
        """Constructor

        Args:
            address (tuple): Tuple contianing the address (ip, port)
            directory (str, optional): The directory to store the fits packets. Defaults to "".
        """
        super().__init__()
        self._address = address
        self._directory = directory
        self._queue = queue
        if directory:
            if not os.path.isdir(directory):
                os.makedirs(directory)
        self._quit = threading.Event()
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._is_connected = False
        self._count = 0

    @property
    def queue(self) -> Queue:
        return self._queue

    @queue.setter
    def queue(self, q: Queue):
        if isinstance(q, Queue):
            self._queue = q

    @property
    def packets(self):
        return self._count

    def is_connected(self):
        return self._is_connected

    def connect(self, timeout=1) -> None:
        """Trys to connect to the given address

        Args:
            timeout (int, optional): Set the socket timeout. Defaults to 5.

        Raises:
            error: If the socket connection can not be established
        """
        if self._is_connected:
            return
        try:
            self._socket.connect(self._address)
            self._socket.settimeout(timeout)
        except socket.error as error:
            self._socket.close()
            raise error
        self._is_connected = True

    def disconnect(self):
        """Closes the socket
        """
        if self._is_connected:
            self._socket.close()
        self._is_connected = False
        self._socket = None
        _log.info("Disconnected socket")

    def run(self):
        """Thread main loop function
        - Connects to the socket, if not connected
        - Receives and parses FITS packets
        - Writes to disk if desired
        - Writes to queue if desired
        """
        self._quit.clear()
        if not self._is_connected:
            self.connect()
        try:
            while not self._quit.is_set():
                packet = recv_packet(self._socket)
                # If a packet was not received, continue until next socket.timeout.
                # This enables the receiver to stop even when the socket is blocking
                if not isinstance(packet, FitsPacket):
                    continue
                if self._directory:
                    fname = f"FWP_{(packet.header.timestamp).decode('utf-8').replace(' ','')}.dat"
                    fpath = os.path.join(self._directory, fname)
                    with open(fpath, "wb") as fid:
                        fid.write(bytes(packet))
                if isinstance(self._queue, Queue):
                    self._queue.put(packet)
                self._count += 1
        finally:
            self.disconnect()
            _log.info("Receiving thread stopped")

    def wait_start(self, timeout: int=5):
        """Waits until the socket is connected

        Args:
            timeout (int, optional): Timeout to throw error. Defaults to 5.

        Raises:
            TimeoutError: When timeout is exceeded
        """
        self.start()
        start = time.time()
        while not self._is_connected:
            time.sleep(0.1)
            if start + timeout < time.time():
                raise TimeoutError("wait_start() timed out after %s", timeout)

    def stop(self):
        """Stops the running thread
        """
        _log.info("Stopping receiving thread")
        self._quit.set()
