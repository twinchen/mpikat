import ipaddress

import mpikat.core.logger

from . ip_utils import network2ipstring, ipstring2network

_log = mpikat.core.logger.getLogger('mpikat.utils.ip_manager')



class IPManager:
    """
    Manages allocation of IP ranges in a given subnet.

    Args:
      subnet: network to manage, either katcp-style IP-string ( e.g. 239.0.0.4+3),
              netmask string (e.g. 239.0.0.4/30) or ipaddress.ip_network instance
    """
    def __init__(self, subnet):
        self.network = self._get_ip_network(subnet)

        self.free_ips = self.network.num_addresses
        self.subnet_managers = {}
        _log.debug('New IPmanager for subnet %s of %i ips', self.network, self.free_ips)

    def _get_ip_network(self, subnet):
        """ Get a ip_network object from any string or ip_network_object
        """
        if isinstance(subnet, str):
            if '/' in subnet:
                return ipaddress.ip_network(subnet, strict=False)
            return ipstring2network(subnet)
        return subnet

    def allocate(self, size):
        """
        Allocate a subnet of given size in the managed IP range
        """
        _log.debug('Allocating range of %i ips for subnet %s with %i free ips', size, self.network, self.free_ips)
        if size > self.network.num_addresses:
            raise RuntimeError(f'Requested allocation of {size} ips in network of size {self.network.num_addresses} ({self.network})')
        if size > self.free_ips:
            _log.debug('No free ips!')
            return None
        if size == self.network.num_addresses:
            _log.debug('Allocating whole subnet %s', self.network)
            self.free_ips = 0
            return network2ipstring(self.network)

        for s in self.network.subnets():
            _log.debug('Checking subnet %s', s)
            # Only generate subnet manager if necessary
            if s not in self.subnet_managers:
                self.subnet_managers[s] = IPManager(s)

            alloc = self.subnet_managers[s].allocate(size)
            if alloc:
                self.free_ips -= size
                _log.debug('Found matching ips in subnet %s - now %i ips free in %s', s, self.free_ips, self.network)
                return alloc
        return None

    def block(self, ipstring):
        """
        Mark sub-networks containing to a given string as used so that they are not available for auto-allocation
        """
        network = self._get_ip_network(ipstring)
        _log.debug('Blocking iprange %s in %s', network, self.network)

        if network == self.network:
            _log.debug('Blocking this network %s', self.network)
            # Save guard as e could have freed an already free network
            blocked = network.num_addresses
            self.free_ips = 0
            self.subnet_managers = {}
            return blocked

        if not self.network.supernet_of(network):
            _log.debug('Subnet %s not part of %s', network, self.network)
            return 0

        for s in self.network.subnets():
            _log.debug('Checking subnet %s', s)
            if not s.supernet_of(network) and s != network:
                _log.debug('Subnet %s not part of %s', network, s)
                continue

            if s not in self.subnet_managers:
                self.subnet_managers[s] = IPManager(s)

            f = self.subnet_managers[s].block(network)
            if f > 0:
                self.free_ips -= f
                _log.debug('Blocked %i ips in subnet %s - now %i ips free in %s', f, s, self.free_ips, self.network)
                break
        return f


    def free(self, network):
        """Free the specified network

        Returns: Number of freed IPs
        """

        _log.debug('Freeing iprange %s in %s', network, self.network)
        network = self._get_ip_network(network)

        if network == self.network:
            _log.debug('Freeing this network!')
            # Save guard as e could have freed an already free network
            freed = network.num_addresses - self.free_ips
            self.free_ips = network.num_addresses
            self.subnet_managers = {}
            return freed

        if not self.network.supernet_of(network):
            _log.debug('Subnet %s not part of %s', network, self.network)
            return 0

        for s in self.subnet_managers.values():
            f = s.free(network)
            if f > 0:
                self.free_ips += f
                _log.debug('Freed %i ips in subnet %s - now %i ips free in %s', f, s, self.free_ips, self.network)
                break
        return f
