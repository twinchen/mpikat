import json
import copy
import fnmatch 
from mpikat.core.logger import getLogger

_log = getLogger('mpikat.core')


def config_update(cfg, update):
    """
    Updates the config dictionary cfg with values in update. Keys in update containing *,? are applied to all cfgs that match the glob pattern
    """
    result = copy.deepcopy(cfg)
    for k, v in update.items():
        match = fnmatch.filter(cfg.keys(), k)
        for m in match:
            result[m].update(v)
    return result




def updateConfig(oldo, new):
    """
    Merge retrieved config [new] into [old] via recursive dict merge

    Example::

        >>> old = {'a:': 0, 'b': {'ba': 0, 'bb': 0}}
        >>> new = {'a:': 1, 'b': {'ba': 2}}

        >>> print(updateConfig(old, new))
        {'a:': 1, 'b': {'ba': 2, 'bb': 0}}

    """
    old = copy.deepcopy(oldo)
    for k, v in new.items():
        match = fnmatch.filter(old.keys(), k)
        _log.trace("Key %s matches %s", k, match)
        if not match:
            raise KeyError(f"No match for key {k} found in config")
        for m in match:
            if isinstance(old[m], dict):
                _log.trace("update sub dict for key: {}".format(m))
                old[m] = updateConfig(old[m], v)
            else:
                _log.trace('Updating key %s with type %s', m, type(old[m]))
                if type(old[m]) != type(v): # pylint: disable=unidiomatic-typecheck
                    _log.warning("Update option {} with different type! Old value(type) {}({}), new {}({}) ".format(m, old[m], type(old[m]), v, type(v)))
                old[m] = v
    return old


def cfgjson2dict(config_json: str) -> dict:
    """
    Returns the provided config as dict if a JSON object or returns the object
    if it already is a dict.
    """
    if isinstance(config_json, str):
        _log.debug("Received config as string:\n  {}".format(config_json))
        if (not config_json.strip()) or config_json.strip() == '""':
            _log.debug("String empty, returning empty dict.")
            return {}
        cfg = json.loads(config_json)
    elif isinstance(config_json, dict):
        _log.debug("Received config as dict")
        cfg = config_json
    else:
        raise TypeError(f"Cannot handle config type {type(config_json)}. Config has to bei either json formatted string or dict!")

    _log.debug("Got cfg: {}, {}".format(cfg, type(cfg)))
    return cfg




def value_list(d):
    """
    Return list of values for a dict, list directly otherwise.
    """
    if isinstance(d, dict):
        return d.values()
    return d
