from threading import Thread, Event
import time
import os

import mpikat.core.logger

_log = mpikat.core.logger.getLogger('mpikat.process_monitor')


class SubprocessMonitor(Thread):
    """
    Monitors managed subprocesses and passes the process to a specific callback function if crashed.
    """

    def __init__(self, poll_intervall=1):
        """
        Args:
            poll_intervall: Intervall [s] in which to poll the subprocess output
        """
        Thread.__init__(self)
        self.setDaemon(True)
        self._procs = []
        self._poll_intervall = poll_intervall

        self._stop_event = Event()

    def run(self):
        while not self._stop_event.is_set():
            for proc, cb in self._procs:
                if not os.path.exists("/proc/{}/stat".format(proc.pid)):
                    _log.debug('Process {} terminated with returncode {}'.format(proc.pid, proc.returncode))
                    cb(proc)
                else:
                    with open("/proc/{}/stat".format(proc.pid)) as f:
                        stat = f.readline().split()
                    if stat[2] in ['R', 'S', 'D']:
                        _log.debug('Process {} [{}] still alive in state {}'.format(proc.pid, stat[1], stat[2]))
                    else:
                        _log.debug('Process {} [{}] terminated with returncode {} - state {}'.format(proc.pid, stat[1], proc.returncode, stat[2]))
                        cb(proc)
            time.sleep(self._poll_intervall)

    def add(self, proc, cb):
        """
        Add a process to the monitor

        Args:
            proc: Popen object to monitor
            cb:   Callback to execute on proc
        """
        _log.debug('Adding process {} with callback {}'.format(proc.pid, cb))
        self._procs.append([proc, cb])

    def stop(self):
        """
        Stop the thread
        """
        _log.debug('Stopping monitoring of {} subprocesses'.format(len(self._procs)))
        self._stop_event.set()
