"""
The GatedSpectrometer receives data in MPIFR EDD packetizer Format from both
polarizations and calculates the resulting spectra per polarization. Computing
requires one NUMA node per input polarization.

Data is received using `mkrecv`_, stored in a `dada`_ buffer and processed using
the `gated spectrometer command line application`.


Configuration Settings
----------------------
    input_data_streams
        The spectrometer requires two input data streams ``polarization_0,
        polarization_1`` in MPIFR_EDD_Packetizer format.

    fft_length (int)
        Number of samples to be included in a FFT. As the dc channel is
        dropped, the output contains fft_length/2 channels.

    naccumulate (int)
        Number of output spectra to be accumulated into one output spectrum.

Note:
    The integration time is given by fft_length * naccumulate / sampling_rate
    of the packetizer.


Output Data Streams
-------------------
    Two data streams in GatedSpectrometer format per input polarisation
    corresponding to spectra for noise diode on and off.


Expert/Debugging Settings
-------------------------
    samples_per_block (int)
        Configure the size of the internal ring buffer. (Default 256*1024*1024)
        This limits the maximum size of the FFT to 128 M Channels by default.
        With this option the code can be tweaked to run on low-mem GPUs or if
        the GPU is  shared with other codes.

    output_rate_factor (float)
        The nominal output data rate is multiplied by this factor and the
        result used to send the data fast enough to keep up with the data
        stream while avoiding bursts. Default: 1.10

    output_type ('network', 'disk', 'null')
        Instead of sending the output to the network, it can be dropped ('null') or written to 'disk'.

    output_directory (str)
        Output directory used for output_type 'disk'. Defaults to '/mnt'

    dummy_input (boolean)
        Don't connect to packetizer data streams but use randomly generated
        input data.

    disable_gate (-1,0,1):
        Disable the calculation of gate 0 or 1 to reduce strain on the GPU. Set
        to -1 (default) to calculate both gates.  Note that also for a disabled
        gate output data is written to the dada_buffer and send to a stream.
        The data will be random numbers though.



    nonfatal_numacheck
        The code check the available NUMA nodes and selects only nodes for
        execution with GPU and network card. If no suitable NUMA node exists,
        configuration will fail unless this this option is set to True. If set
        to True, only a warning is emitted. Used e.g. in automatic testing.


.. _mkrecv:
    https://gitlab.mpifr-bonn.mpg.de/mhein/mkrecv

.. _dada:
    http://psrdada.sourceforge.net/

.. _GatedSpectrometer CLI application:


"""
import os
import time
import json
import asyncio

from aiokatcp.sensor import Sensor
from aiokatcp.connection import FailReply

from mpikat.utils.process_tools import ManagedProcess
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.sensor_watchdog import SensorWatchdog
from mpikat.utils.sensor_watchdog import conditional_update
from mpikat.utils.core_manager import CoreManager
from mpikat.utils.ip_utils import ipstring_to_list
from mpikat.utils import numa
import mpikat.utils.dada_tools as dada
import mpikat.utils.mk_tools as mk
from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change

import mpikat.pipelines.digpack_controller  # pylint: disable=unused-import
from mpikat.pipelines.gated_spectrometer import GatedSpectrometerDataStreamFormat

import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.pipelines.GatedSpectrometerPipeline")


_DEFAULT_CONFIG = {
        "id": "GatedSpectrometer",                          # default item for master-controller. Needs to get a unique ID -- TODO, from Ansible
        "type": "GatedSpectrometer",
        "supported_input_formats": {"MPIFR_EDD_Packetizer": [1]},      # supported input formats name:version
        "samples_per_block": 256 * 1024 * 1024,             # 256 Mega samples per buffer block to allow high res  spectra - the
                                                            # theoretical  maximum is thus  128 M Channels. This option allows to tweak the
                                                            # execution on low-mem GPUs or if the GPU is shared with other codes
        "input_data_streams":
        [
            {
                "format": "MPIFR_EDD_Packetizer:1",         # Format has version separated via colon
                "ip": "225.0.0.140+3",
                "port": "7148",
                "bit_depth" : 8,
                "sample_rate": 1300000000,
                "polarization":0
            },
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip": "225.0.0.144+3",
                "port": "7148",
                "bit_depth" : 8,
                "sample_rate": 1300000000,
                "polarization": 1
            }
        ],
        "output_data_streams":
        {
            "polarization_0_0" :
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.172",        #two destinations gate on/off
                "port": "7152",
            },
            "polarization_0_1" :
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.173",        #two destinations gate on/off
                "port": "7152",
            },
            "polarization_1_0" :
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.174",        #two destinations, one for on, one for off
                "port": "7152",
            },
            "polarization_1_1" :
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.175",        #two destinations, one for on, one for off
                "port": "7152",
            }
        },

        "fft_length": 1024 * 1024 * 2 * 8,
        "naccumulate": 32,
        "disable_gate": -1,                                 # Disable calculation of a gate.
        "output_directory": "/mnt",                         # ToDo: Should be a output data stream def.
        "output_type": 'network',                           # ['network', 'disk', 'null']  ToDo: Should be a output data stream def.
        "dummy_input": False,                               # Use dummy input instead of mkrecv process. Should be input data stream option.
        "nonfatal_numacheck": False,                             # Ignore NUMA node constraints, e.g. due to missing networks for debugging
        "log_level": "debug",

        "output_rate_factor": 1.10,                         # True output date rate is multiplied by this factor for sending.
        "idx1_modulo": "auto",
    }

class GatedSpectrometerPipeline(EDDPipeline):
    """Gated spectrometer pipeline
    """
    VERSION_INFO = ("mpikat-edd-api", 0, 1)
    BUILD_INFO = ("mpikat-edd-implementation", 0, 1, "rc1")

    DATA_FORMATS = [GatedSpectrometerDataStreamFormat.stream_meta]

    def __init__(self, ip, port, loop=None):
        super().__init__(ip, port, default_config=_DEFAULT_CONFIG, loop=loop)
        self.__numa_node_pool = []
        self._mkrec_proc = []
        self._dada_buffers = []
        self.__watchdogs = []
        self.__coreManager = []
        self.input_heapSize = None

    def setup_sensors(self):
        """
        Setup the monitoring sensors
        """
        super().setup_sensors()

        self._integration_time_status = Sensor(
            float, "integration-time",
            description="Integration time [s]",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._integration_time_status)

        self._output_rate_status = Sensor(
            float, "output-rate",
            description="Output data rate [Gbyte/s]",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._output_rate_status)

        self._polarization_sensors = {}


    def add_input_stream_sensor(self, streamid):
        """
        Add sensors for i/o buffers for an input stream with given stream-id.
        """
        self._polarization_sensors[streamid] = {}
        self._polarization_sensors[streamid]["mkrecv_sensors"] = mk.MkrecvSensors(streamid)
        for s in self._polarization_sensors[streamid]["mkrecv_sensors"].sensors.values():
            self.sensors.add(s)
        self._polarization_sensors[streamid]["input-buffer-fill-level"] = Sensor(
            float, "input-buffer-fill-level-{}".format(streamid),
            description="Fill level of the input buffer for polarization{}".format(streamid),
            initial_status=Sensor.Status.UNKNOWN,
            # params=[0, 1] # not supported by aiokatcp
        )
        self.sensors.add(self._polarization_sensors[streamid]["input-buffer-fill-level"])
        self._polarization_sensors[streamid]["input-buffer-total-write"] = Sensor(
            float, "input-buffer-total-write-{}".format(streamid),
            description="Total write into input buffer for polarization {}".format(streamid),
            initial_status=Sensor.Status.UNKNOWN,
            # params=[0, 1] # not supported by aiokatcp
        )
        self.sensors.add(self._polarization_sensors[streamid]["input-buffer-total-write"])
        self._polarization_sensors[streamid]["output-buffer-fill-level"] = Sensor(
            float, "output-buffer-fill-level-{}".format(streamid),
            description="Fill level of the output buffer for polarization {}".format(streamid),
            initial_status=Sensor.Status.UNKNOWN
        )
        self._polarization_sensors[streamid]["output-buffer-total-read"] = Sensor(
            float, "output-buffer-total-read-{}".format(streamid),
            description="Total read from output buffer for polarization {}".format(streamid),
            initial_status=Sensor.Status.UNKNOWN
        )
        self.sensors.add(self._polarization_sensors[streamid]["output-buffer-total-read"])
        self.sensors.add(self._polarization_sensors[streamid]["output-buffer-fill-level"])
        self.mass_inform('interface-changed')


    def _buffer_status_handle(self, status):
        """
        Process a change in the buffer status.
        """
        timestamp = time.time()
        for i, stream_description in enumerate(self._config["input_data_streams"]):
            streamid = "polarization_{}".format(stream_description['polarization'])
            if status['key'] == self._dada_buffers[i*2].key: # Multiply by 2 to access only input buffers
                conditional_update(self._polarization_sensors[streamid]["input-buffer-total-write"], status['written'], timestamp=timestamp)
                self._polarization_sensors[streamid]["input-buffer-fill-level"].set_value(status['fraction-full'], timestamp=timestamp)
        for i, stream_description in enumerate(self._config["input_data_streams"]):
            streamid = "polarization_{}".format(stream_description['polarization'])
            if status['key'] == self._dada_buffers[i*2+1].key: # Multiply by 2 and add 1 to access only output buffers
                self._polarization_sensors[streamid]["output-buffer-fill-level"].set_value(status['fraction-full'], timestamp=timestamp)
                conditional_update(self._polarization_sensors[streamid]["output-buffer-total-read"], status['read'], timestamp=timestamp)


    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Configure the EDD gated spectrometer

        The size of the dada buffers are calculated from the configuration
        settings and the memory is allocated. The gated spectrometer sub-process
        is started on the GPU.
        """
        _log.info("Configuring EDD backend for processing")

        cfs = json.dumps(self._config, indent=4)
        _log.info("Final configuration:\n%s", cfs)

        #for l in self._config["output_data_streams"].values():
        #    l["central_freq"] = self._config["input_data_streams"][0]["central_freq"]
        #    l["band_flip"] = self._config["input_data_streams"][0]["band_flip"]
        #    l["receiver_id"] = self._config["input_data_streams"][0]["receiver_id"]
        #self._configUpdated()

        self.__numa_node_pool = []
        # remove NUMA nodes with missing capabilities
        for node in numa.getInfo():
            if len(numa.getInfo()[node]['gpus']) < 1:
                _log.debug("Not enough gpus on numa node {} - removing from pool.".format(node))
                continue
            if len(numa.getInfo()[node]['net_devices']) < 1:
                _log.debug("Not enough nics on numa node {} - removing from pool.".format(node))
                continue
            self.__numa_node_pool.append(node)

        _log.debug("{} numa nodes remaining in pool after constraints.".format(len(self.__numa_node_pool)))

        if len(self._config['input_data_streams']) > len(self.__numa_node_pool):
            if self._config['nonfatal_numacheck']:
                _log.warning("Not enough numa nodes to process data!")
                self.__numa_node_pool = list(numa.getInfo().keys())
            else:
                raise FailReply("Not enough numa nodes to process {} polarizations!".format(len(self._config['input_data_streams'])))

        self._subprocessMonitor = SubprocessMonitor()
        #ToDo: Check that all input data streams have the same format, or allow different formats
        for i, stream_description in enumerate(self._config['input_data_streams']):
            streamid = "polarization_{}".format(stream_description['polarization'])
            # calculate input buffer parameters
            self.add_input_stream_sensor(streamid)
            self.input_heapSize = stream_description["samples_per_heap"] * stream_description['bit_depth'] // 8

            nHeaps = self._config["samples_per_block"] // stream_description["samples_per_heap"]
            input_bufferSize = nHeaps * (self.input_heapSize + 64 // 8)
            _log.info('Input dada parameters created from configuration:\n\
                    heap size:        {} byte\n\
                    heaps per block:  {}\n\
                    buffer size:      {} byte'.format(self.input_heapSize, nHeaps, input_bufferSize))

            # calculate output buffer parameters
            nSlices = max(self._config["samples_per_block"] // self._config['fft_length'] //  self._config['naccumulate'], 1)
            nChannels = self._config['fft_length'] // 2 + 1
            # on / off spectrum  + one side channel item per spectrum
            output_bufferSize = nSlices * 2 * (nChannels * 32 // 8 + 2 * 8) # 2 sci items and 2 pols

            output_heapSize = nChannels * 32 // 8
            integrationTime = self._config['fft_length'] * self._config['naccumulate']  / (float(stream_description["sample_rate"]))
            self._integration_time_status.set_value(integrationTime)
            rate = output_heapSize / integrationTime # in spead2 documentation BYTE per second and not bit!
            rate *= self._config["output_rate_factor"]        # set rate to (100+X)% of expected rate
            self._output_rate_status.set_value(rate / 1E9)


            _log.info('Output parameters calculated from configuration:\n\
                spectra per block:  {} \n\
                nChannels:          {} \n\
                buffer size:        {} byte \n\
                integrationTime :   {} s \n\
                heap size:          {} byte\n\
                rate ({:.0f}%):     {} Gbps'\
            .format(nSlices, nChannels, output_bufferSize, integrationTime,
                output_heapSize, self._config["output_rate_factor"]*100, rate / 1E9
            ))

            numa_node = self.__numa_node_pool[i]
            _log.debug("Associating {} with numa node {}".format(streamid, numa_node))

            self.__coreManager.append(CoreManager(numa_node))
            self.__coreManager[i].add_task("gated_spectrometer", 1)

            N_inputips = 0
            for p in stream_description["ip"].split(','):
                N_inputips += len(ipstring_to_list(p))

            if self._config["dummy_input"]:
                self.__coreManager[i].add_task("mkrecv", 1)
            else:
                self.__coreManager[i].add_task("mkrecv", N_inputips + 1, prefere_isolated=True)

            if self._config["output_type"] == "network":
                self.__coreManager[i].add_task("mksend", 2)

            # Create dada buffer for input data
            self._dada_buffers.append(
                dada.DadaBuffer(input_bufferSize, n_slots=64, node_id=numa_node, pin=True, lock=True)
            )

            await self._dada_buffers[-1].create()
            # Create dada buffer for output data
            self._dada_buffers.append(
                dada.DadaBuffer(output_bufferSize, n_slots=8 * nSlices, node_id=numa_node, pin=True, lock=True)
            )
            await self._dada_buffers[-1].create()

            if self._config['disable_gate'] in [0, 1]:
                disable_gate_opt = "--disable_gate={}".format(self._config['disable_gate'])
            else:
                disable_gate_opt = ""

            # Configure + launch
            cmd = "taskset -c {physcpu} gated_spectrometer {disable_gate_opt} --nsidechannelitems=1 --input_key={dada_key} --speadheap_size={heapSize} --selected_sidechannel=0 --nbits={bit_depth} --fft_length={fft_length} --naccumulate={naccumulate} -o {ofname} --log_level={log_level} --output_type=dada".format(dada_key=self._dada_buffers[-2].key, ofname=self._dada_buffers[-1].key, heapSize=self.input_heapSize, numa_node=numa_node, physcpu=self.__coreManager[i].get_coresstr('gated_spectrometer'), bit_depth=stream_description['bit_depth'], disable_gate_opt=disable_gate_opt, **self._config)
            _log.debug("Command to run: {}".format(cmd))

            cudaDevice = numa.getInfo()[numa_node]['gpus'][0]
            gated_cli = ManagedProcess(cmd, env={"CUDA_VISIBLE_DEVICES": cudaDevice})
            _log.debug("Visble Cuda Device: {}".format(cudaDevice))
            self._subprocessMonitor.add(gated_cli, self._subprocess_error)
            self._subprocesses.append(gated_cli)

            cfg = self._config.copy()
            cfg.update(stream_description)

            if self._config["output_type"] == 'network':
                fastest_nic, nic_params = numa.getFastestNic(numa_node)
                _log.info("Sending data for %s on NIC %s [ %s ] @ %s Mbit/s",
                          streamid, fastest_nic, nic_params['ip'], nic_params['speed'])
                streams = [v for k,v in self._config["output_data_streams"].items() if streamid in k]
                mksend = mk.Mksend(streams, self._dada_buffers[-1].key, self.__coreManager[i].get_coresstr('mksend'))
                mksend.set_header("packet_size", 8400)
                mksend.set_header("buffer_size", 128000000)
                mksend.set_header("heap_id_start", 2 * stream_description['polarization'])
                mksend.set_header("heap_id_step", 13)
                mksend.set_header("heap_id_offset", 1)
                mksend.set_header("rate", rate)
                mksend.set_header("nhops", 4)
                mksend.set_header("heap_size", output_heapSize)
                mksend.set_header("ibv_if", nic_params['ip'])
                mksend.set_item("timestamp_count", "step", cfg["fft_length"] * cfg["naccumulate"])
                mksend.set_item("polarization", "list", stream_description['polarization'])
                mksend.set_item("noise_diode_status", "list", "0,1")
                mksend.set_item("noise_diode_status", "index", 2)
                mksend.set_item("fft_length", "list", cfg["fft_length"])
                mksend.set_item("number_of_input_samples", "sci", 1)
                mksend.set_item("sync_time", "list", stream_description["sync_time"])
                mksend.set_item("sampling_rate", "list", stream_description["sample_rate"])
                mksend.set_item("naccumulate", "list", cfg["naccumulate"])
                mksend.set_item("number_of_saturated_samples", "sci", 2)
                mksend.set_item("type", "list", 0)

                _log.debug("Mksend command: %s", mksend.cmd)
                mks = mksend.start(env={"CUDA_VISIBLE_DEVICES": cudaDevice})

            elif self._config["output_type"] == 'disk':
                ofpath = os.path.join(cfg["output_directory"], self._dada_buffers[-1].key)
                _log.debug("Writing output to {}".format(ofpath))
                if not os.path.isdir(ofpath):
                    os.makedirs(ofpath)
                mks = dada.DbDisk(self._dada_buffers[-1].key, directory=ofpath).start()
            else:
                _log.warning("Selected null output. Not sending data!")
                mks = dada.DbNull(self._dada_buffers[-1].key, zerocopy=True).start()

            self._subprocessMonitor.add(mks, self._subprocess_error)
            self._subprocesses.append(mks)
        # For each buffer attach an instance of DbMonitor
        for buf in self._dada_buffers:
            buf.get_monitor(self._buffer_status_handle).start()
        self._subprocessMonitor.start()



    @state_change(target="streaming", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        """
        Start streaming spectrometer output.

        Starts mkrecv process.
        """
        _log.info("Starting EDD backend")
        for i, desc in enumerate(self._config["input_data_streams"]):
            numa_node = self.__numa_node_pool[i]
            streamid = f"polarization_{desc['polarization']}"
            fastest_nic, nic_params = numa.getFastestNic(numa_node)
            _log.info("Receiving data on NIC %s [ %s ] @ %s Mbit/s",
                      fastest_nic, nic_params['ip'], nic_params['speed'])
            mkrecv = mk.Mkrecv(desc, self._dada_buffers[i*2].key,
                               self.__coreManager[i].get_coresstr('mkrecv'))
            mkrecv.stdout_handler(self._polarization_sensors[streamid]["mkrecv_sensors"].stdout_handler)
            mkrecv.set_header("packet_size", 8400)
            mkrecv.set_header("buffer_size", 128000000)
            mkrecv.set_header("dada_nslots", 3)
            mkrecv.set_header("slots_skip", 4)
            mkrecv.set_header("heap_nbytes", self.input_heapSize)
            mkrecv.set_header("sci_list", 2)
            mkrecv.set_header("ibv_if", nic_params['ip'])
            mkrecv.set_item("timestep", "index", 1)
            mkrecv.set_item("timestep", "step", self.input_heapSize)
            if self._config['idx1_modulo'] == 'auto':
                mkrecv.set_item("timestep", "modulo", self._config['fft_length'] * self._config['naccumulate'])
            else:
                mkrecv.set_item("timestep", "modulo", self._config['idx1_modulo'])
            if self._config['dummy_input']:
                fname = mk.write_recv_header(mkrecv.header, mkrecv.items)
                capture_proc = dada.JunkDb(self._dada_buffers[i*2].key, fname, desc["sample_rate"], 3600).start()
            else:
                capture_proc = mkrecv.start()
            self._mkrec_proc.append(capture_proc)
            self._subprocessMonitor.add(capture_proc, self._subprocess_error)
            wd = SensorWatchdog(self._polarization_sensors[streamid]["input-buffer-total-write"], 10, self.watchdog_error)
            wd.start()
            self.__watchdogs.append(wd)
        await asyncio.sleep(self._integration_time_status.value)


    @state_change(target="idle", allowed=["streaming"], intermediate="capture_stopping")
    async def capture_stop(self):
        """
        Stop streaming of data.

        Stop all sub-processes. Also triggers a deconfigure.
        """
        _log.info("Stopping spectrometer")

        await self.deconfigure()


    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """
        Deconfigure the gated spectrometer pipeline.

        Clears all dada buffers.
        """
        _log.info("Deconfiguring EDD backend")
        for wd in self.__watchdogs:
            wd.stop_event.set()

        if self._subprocessMonitor is not None:
            self._subprocessMonitor.stop()

        # stop mkrecv process
        _log.debug("Stopping mkrecv processes ...")
        for proc in self._mkrec_proc:
            proc.terminate()

        for proc in self._subprocesses:
            proc.terminate()

        _log.debug("Destroying dada buffers")

        tasks = []
        for buf in self._dada_buffers:
            tasks.append(asyncio.create_task(buf.destroy()))
        await asyncio.gather(*tasks)

        self._mkrec_proc = []
        self._dada_buffers = []
        self.__coreManager = []


if __name__ == "__main__":
    asyncio.run(launchPipelineServer(GatedSpectrometerPipeline))
