from dataclasses import dataclass, asdict

@dataclass
class MkItem():
    """A dataclass representing the properties of a Mkitem
    """
    # The item's name
    name: str = ""
    # The item's address - Only used for Mksend
    addr: int = None
    # The item's index for the placement
    index: int = None
    # The item list (range or string)
    list: str = None
    # The masking of the item allows filtering (hex string)
    mask: str = None
    # Modulo allows further filtering (Only valid for IDX1 - first Mkrecv item)
    modulo: int = None
    # The step size of the item (Only valid for IDX1 - first Mkrecv item)
    step: int = None
    # The side-channel index - Only used for Mksend
    sci: int = None

# pylint: disable=too-many-instance-attributes
@dataclass
class MkHeader:
    """A data class representing the properties of a MkHeader
    """
    # ---------------------- #
    # Dada header parameters # - may moved to another data class
    # ---------------------- #
    header: str = 'DADA'
    hdr_version: str = 1
    hdr_size: int = 4096
    dada_version: float = 1.0
    dada_key: str = 'dada'              # PSRDADA ring-buffer key
    file_size: int = 67108864
    utc_start: str = "unset"
    dim: int = 1
    nbit: int = 8
    bytes_per_second: int = "unset"
    # ------------------------------- #
    # Observation specific parameters #
    # ------------------------------- #
    telescope: str = "unset"
    receiver: str = "unset"
    instrument: str = "unset"
    obs_offset : str = "unset"
    resolution: str = "unset"
    # ----------------- #
    # MK CLI parameters #
    # ----------------- #
    # dada mode (0 = artificial data, 1 = data from dada ringbuffer)
    dada_mode: int = "unset"
    # Maximum packet size to use for UDP
    packet_size: int = 8400
    # Network buffer size. System settings can be tuned with "sysctl net.core.wmem_max=NUMBER" and "sysctl net.core.rmem_max="
    buffer_size: int = 16777216
    # The heap size. Usually the byte size of the payload data (n samples * bytes size)
    # heap_size: int = 0
    # Number of heap groups (heaps with the same timestamp) going into the data space. (optional)
    ngroups_data: int = "unset"
    # The ADC sync epoch - used for time stamping of the data
    sync_time: int = "unset"
    # virtual sample clock used for calculations
    sample_clock: int = "unset"
    # The start of the sample counter, is set by mkrecv. The first received dictates the START
    sample_clock_start: int = 0
    # Interface address for ibverbs
    ibv_if: str = "unset"
    # Interface address for UDP
    udp_if: str = "unset"
    # Interrupt vector (-1 for polled)
    ibv_vector: int = -1
    # Maximum number of times to poll in a row
    ibv_max_poll: int = 10
    # The port on which to capture data
    port: int = "unset"
    # Number of worker threads
    nthreads: int = 1
    # Maximum number #of active heaps
    nheaps: int = 64

    # ---------------- #
    # Mksend specifics #
    # ---------------- #

    # Use non_temporal memcpy
    memcpy_nt: any = "unset"
    # Number of slots send via network during simulation, 0  means infinite.
    nslots: int = 0
    # network mode (0 = no network, 1 = full network support)
    network_mode: int = 1
    # Maximum number #of hops
    nhops: int = 1
    # Network use #rate
    rate: float = .0
    # Size of a network burst [Bytes]
    burst_size: int = 65536
    # ?
    burst_ratio: float = 1.05
    # The heap size. Usually the byte size of the payload data (n samples * bytes size)
    heap_size: int = 0
    # First used heap id
    heap_id_start: int = 0
    # offset of heap id (different streams)
    heap_id_offset: int = 0
    # difference between two heap ids (same stream)
    heap_id_step: int = 1
    # number of consecutive heaps going into the same stream
    heap_group: int = 1
    # Number of #item pointers in the side_channel
    nsci: int = 0
    # Number of #item pointers in a SPEAD heap
    nitems: int = 0

    # ---------------- #
    # Mkrecv specifics #
    # ---------------- #

    # NUmber of open dada slots to put heaps in
    dada_nslots: int = 3
    # Number of dada slots to skip at the beginning
    slots_skip: int = 0
    # Number of items used for computing the position of the heap in the dada slot
    nindices: int = 0
    # Byte size of a single dada slot
    slot_nbytes: int = "unset"
    # Payload size of a heap
    heap_nbytes: int = "unset"
    # ?
    level_data: any = "unset"
    # Number of SCI items to put into the dada slot. Dada buffer must be sized accordingly
    sci_list: any = "unset"
    # Number of multicast groups per stream
    multi_stream_threads: any = "unset"
    # Auxiliary thread core (main, header creation)
    aux_thread_core: any = "unset"


    def __init__(self, input_dict: dict=None, mapping: dict=None):
        if input_dict is None:
            return
        temp_dict = {}
        if isinstance(mapping, dict):
            for old_key, new_key in mapping.items():
                if old_key in input_dict:
                    temp_dict[new_key] = input_dict[old_key]
        temp_dict.update({key: value for key, value in input_dict.items() if key in asdict(self)})
        self.__dict__.update(temp_dict)
