import spead2
import tempfile
import io
from typing import List
from dataclasses import asdict

from mpikat.core.logger import getLogger

from .mkheader import MkHeader, MkItem
from .mkbase import MkBase

_LOG = getLogger("mpikat.utils.mk_tools.mksend")

mksend_mapping = {
    "sample_rate":"sample_clock",
    "nic":"ibv_if",
    "bit_depth":"nbit",
}

def create_mksend_items(items: List[spead2.Item]) -> List[MkItem]:
    """Creates a list of MkSendItems

    Args:
        items (List[spead2.Item]): List of spead2.Items

    Returns:
        List[MkItem]: List of MksendItems
    """
    res: List[MkItem] = []
    for item in items:
        res.append(MkItem(name=item.name, addr=item.id))
    return res

def write_send_item(fid: io.TextIOWrapper, item: MkItem, num: int):
    """Writes an item to a opend file as it is expected by Mksend

    Args:
        fid (io.TextIOWrapper): The file pointer to the opened file
        item (MkItem): The item to write to the file
    """
    for key, val in asdict(item).items():
        if val is None: continue
        if key == "name": continue
        if key == "addr": key = "ID"; val = f"{val} # {item.name}"
        fid.write(f"ITEM{num}_{key.upper()} {val}\n")
    fid.write("\n")


def write_send_header(header: MkHeader, items: List[MkItem]=None, ignore_unset=True) -> str:
    """Creates a temporary file containing Mk header information

    Returns:
        str: The path of the file
    """
    if items is None:
        items = []
    with tempfile.NamedTemporaryFile(delete=False, mode="w") as f:
        for key, val in asdict(header).items():
            if val == "unset" and ignore_unset: continue
            f.write(f"{key.upper()} {val}\n")
        if items:
            f.write("# Spead Items\n")
            for i, item in enumerate(items):
                write_send_item(f, item, i+1)
    return f.name


class Mksend(MkBase):

    def __init__(self, streams: List[dict], key: str, physcpu: str="",
                 ibv: bool=True, mapping=mksend_mapping):
        """The Mkrecv wrapper object.

        Args:
            streams (List[dict]): The data streams from which information are taken
            key (str): The dada key to connect to
            physcpu (str, optional): The physical CPU to run the CLI on. Defaults to "".
            ibv (bool, optional): Use ibv or udp -> ibv for maximum performance. Defaults to True.
            mapping (dict, optional): A mapping dictionary allowing to translate streams
                to mkheader. Defaults to None.
        """
        super().__init__(streams, key, physcpu, ibv, mapping)
        self.items: List[MkItem] = create_mksend_items(self.spead_items)
        self.items.sort(key=lambda x: x.addr)

    def command(self) -> str:
        """Constructs the Mksend command

        Returns:
            str: The Mksend command
        """
        self.header.nitems = len(self.items)
        self.header.nsci = len([item for item in self.items if item.sci is not None])
        if self.header.rate == 0 and self.header.sample_clock != "unset":
            self.header.rate = self.header.sample_clock * 1.1
        self.header_file = write_send_header(self.header, self.items)
        self.cmd += f"mksend --quiet --header {self.header_file} {','.join(self.ip_list)}"
        return self.cmd

    def set_item(self, name: str, key: str, val: str):
        """Sets the a property of an item by its name and key
            The property must exists in the Mkitem-dataclass

        Args:
            name (str): The item's name
            key (str): The key/property to set
            val (any): The value of the property
        """
        if key in ["mask", "modulo"]:
            _LOG.warning("Item property %s not used for mkrecv", key);return
        super().set_item(name, key, val)

