import json
import asyncio
import aiokatcp

from mpikat.core import logger
from mpikat.core.katcp_client import KATCPClient


_log = logger.getLogger("mpikat.core.product_controller")


class ProductController:
    """
    Interface for pipeline instances using katcp.

    Args:
        product_id:     A unique identifier for this product
        address:        IP Address of the product
        port:           Port of the product
        ignore_errors:  If true, exceptions and failures in the pipeline will be ignored.
    """
    def __init__(self, product_id, address, port, ignore_errors=False):
        _log.debug("Installing controller for %s at %s:%i", product_id, address, port)
        self.ip = address
        self.port = int(port)
        self._client = KATCPClient(address, port)
        self.ignore_errors = ignore_errors
        self._product_id = product_id

    def close(self):
        """Close connection to server"""
        self._client.close()

    async def _safe_request(self, request_name, *args, **kwargs):
        timeout = kwargs.get('timeout', 2)
        p = await self.ping(timeout)
        if not p:
            if self.ignore_errors:
                _log.warning("Cannot connect to product: %s@[%s:%i], but errors in product are ignored", self._product_id, self.ip. self.port)
                return
            raise RuntimeError(f"Cannot connect to product: {self._product_id}@[{self.ip}:{self.port}]")

        _log.trace("Sending request '{}' to {} with arguments {}".format(request_name, self._product_id, args))
        try:
            response = await self._client.request(request_name, *args, **kwargs)
        except (aiokatcp.FailReply, TimeoutError, asyncio.exceptions.TimeoutError) as E:
            _log.error("%s in %s request: %s:\n%s", E.__class__.__name__, self._product_id, request_name, E)
            if self.ignore_errors:
                _log.warning(" -> Error ignored due to config setting!")
                return
            raise E
        return response

    async def deconfigure(self):
        """
        Deconfigure the product
        """
        await self._safe_request('deconfigure', timeout=120.0)

    async def configure(self, config=None):
        """
        A no-op method for supporting the product controller interface.
        """
        if config is None:
            config = {}
        _log.debug("Send cfg to %s", self._product_id)
        await self._safe_request("configure", json.dumps(config), timeout=120.0)

    async def capture_start(self):
        """
        A no-op method for supporting the product controller interface.
        """
        await self._safe_request("capture-start", timeout=120.0)

    async def capture_stop(self):
        """
        A no-op method for supporting the product controller interface.
        """
        await self._safe_request("capture-stop", timeout=120.0)

    async def measurement_prepare(self, config=None):
        """
        A no-op method for supporting the product controller interface.
        """

        if config is None:
            config = {}
        await self._safe_request("measurement-prepare", json.dumps(config), timeout=120.0)

    async def measurement_start(self):
        """
        A no-op method for supporting the product controller interface.
        """
        await self._safe_request("measurement-start", timeout=60.0)

    async def measurement_stop(self, val=False, timeout=60.):
        """
        A no-op method for supporting the product controller interface.
        """
        res, _ = await self._safe_request("measurement-stop", val, timeout=timeout)
        if val and res:
            return res[0].decode('ascii')


    async def set(self, config):
        """
        A no-op method for supporting the product controller interface.
        """
        _log.debug("Send set to %s", self._product_id)
        await self._safe_request("set", json.dumps(config), timeout=120.0)


    async def provision(self, config):
        """
        A no-op method for supporting the product controller interface.
        """
        _log.debug("Send provision to %s", self._product_id)
        await self._safe_request("provision", config, timeout=300.0)


    async def deprovision(self):
        """
        A no-op method for supporting the product controller interface.
        """
        _log.debug("Send deprovision to %s", self._product_id)
        await self._safe_request("deprovision", timeout=300.0)


    async def getConfig(self, timeout=3):
        """
        A no-op method for supporting the product controller interface.
        """
        _log.debug("Send get config to %s", self._product_id)
        try:
            _, informs = await self._safe_request("sensor-value", "current-config", timeout=timeout)
            data = json.loads(informs[0].arguments[-1].decode())
            return data
        except Exception as E:
            if self.ignore_errors:
                _log.exception(E)
                _log.warning("Error retrieving config ignored!")
                return "{}"
            raise E

    async def getState(self, timeout=3):
        """
        Return current state of the server
        """
        status = 'UNREACHABLE'
        try:
            _, informs = await self._safe_request("sensor-value", "pipeline-status", timeout=timeout)
            status = informs[0].arguments[-1].decode()
            _log.trace("Received status %s", status)
        except (aiokatcp.FailReply, TimeoutError, asyncio.exceptions.TimeoutError) as E:
            _log.warning("Error retrieving state: %s", E)
        return status

    async def ping(self, timeout=2, retries=2):
        """
        Test if connection to product is running / alive. Will create a new client and retry if first attempt fails.
        """
        _log.trace("Ping product %s at %s:%i", self._product_id, self.ip, self.port)
        for i in range(retries):
            _log.trace("Waiting for connection to %s (%s:%i) - attempt %i/%i", self._product_id, self.ip, self.port, i+1, retries)
            task = self._client.wait_connected()
            try:
                await asyncio.wait_for(task, timeout=timeout)
            except (TimeoutError, asyncio.exceptions.TimeoutError):
                # install a new client and retry
                self._client = KATCPClient(self.ip, self.port)
                continue

            return True
        _log.error("%s: Timeout Reached. Product inactive", self._product_id)
        return False
