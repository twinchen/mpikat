import time
import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.utils.timetamp_handler")

class TimestampHandler:
    """
    Handles timestamp rollovers due to limited precision in counter
    """
    def __init__(self, sync_time, sampling_frequency, nbits=48):
        """
        Args:
            sync_time: Reference time of the timesamps
            sampling_frequency: sampling rate
            nbits: number of bits for timestamp values
        """
        self._nbits = nbits
        self._sync_time = sync_time

        now = time.time()

        epf = (now - self._sync_time) * sampling_frequency / (2**self._nbits)
        self._epoch = int(epf)

        if epf - self._epoch > 2./3:
            self._active_third = 3
        elif epf - self._epoch > 1./3:
            self._active_third = 2
        else:
            self._active_third = 1

        _log.debug("Initialized timespamp handler at %i from sync_time %i.\
                For %i bit timestamps at %E this corresponds to epoch %i, active third: %i",
                   now, self._sync_time, self._nbits, sampling_frequency, self._epoch,
                   self._active_third)

    def _updateEpoch(self, timestamp):
        """
        Updates the epoch depending on the timestamp and the current epoch, accounting for
        timestamps beeing out of order for at most 1/3 of the period given by
        the bit_depth. From previous calls, the current
        """

        epoch_fraction = timestamp / (2**self._nbits)
        _log.trace("Updating epoch from timestamp %i with fraction %.2f", timestamp, epoch_fraction)

        effective_epoch = self._epoch
        if epoch_fraction > 2./3:
            if self._active_third == 2:
                _log.debug("Updating fraction from 2 -> 3")
                self._active_third = 3
            elif self._active_third == 1:
                _log.trace("Stray from previous epoch")
                effective_epoch = self._epoch - 1
        elif epoch_fraction > 1./3:
            if self._active_third == 1:
                _log.debug("Updating fraction from 1 -> 2")
                self._active_third = 2
        else:
            if self._active_third == 3:
                _log.debug("Updating fraction from 3 -> 1")
                self._active_third = 1
                self._epoch += 1
                _log.debug("Reached a new epoch: %i", self._epoch)
                effective_epoch = self._epoch
        return effective_epoch


    def updateTimestamp(self, timestamp):
        """
        Updates a timestamp depending on the current epoch accounting for
        timestamps beeing out of order for at most 1/3 of the period given by
        the bit_depth. From previous calls, the current
        """
        effective_epoch = self._updateEpoch(timestamp)

        return timestamp + effective_epoch * 2**self._nbits
