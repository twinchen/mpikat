"""
The sub-module 'mk_tool' provides wrapper, data classes and utilities to interface with the
high speed network transceiver CLIs 'mkrecv' and 'mksend'.

The mk_tool sub-module abstracts significant parts to run the CLIs making it easier to use,
improve code readablity and reduce code duplication.

"""

from mpikat.utils.mk_tools.mkheader import MkHeader, MkItem
from mpikat.utils.mk_tools.mkrecv import Mkrecv, create_mkrecv_items, write_recv_header, write_recv_item, auto_tune_cores
from mpikat.utils.mk_tools.mksend import Mksend, create_mksend_items, write_send_header, write_send_item
from mpikat.utils.mk_tools.mkutils import read_header, get_item, get_index_mask, get_index_list
from mpikat.utils.mk_tools.mksensor import MkrecvSensors, mkrecv_stdout_parser

__all__ = [
    # Data classes
    "MkItem",
    "MkHeader",
    # Mkrecv
    "Mkrecv",
    "create_mkrecv_items",
    "write_recv_header",
    "write_recv_item",
    "auto_tune_cores",
    # Mksend
    "Mksend",
    "create_mksend_items",
    "write_send_header",
    "write_send_item",
    # Mkutils
    "read_header",
    "get_item",
    "get_index_mask",
    "get_index_list",
    # Mksensors
    "MkrecvSensors",
    "mkrecv_stdout_parser"
]
