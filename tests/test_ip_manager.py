# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring

import unittest

from mpikat.utils.ip_manager import IPManager
from mpikat.core import logger

_log = logger.getLogger('mpikat.tests.test_ip_manager')

class TestIPManager(unittest.TestCase):

    def test_allocation_of_matching_size(self):
        """Check some allocations of full networks"""
        ipm = IPManager('239.11.1.148/32')
        nw = ipm.allocate(1)
        self.assertEqual(nw, '239.11.1.148')

        ipm = IPManager('239.11.1.148/31')
        nw = ipm.allocate(2)
        self.assertEqual(nw, '239.11.1.148+1')

        ipm = IPManager('239.11.1.0/24')
        nw = ipm.allocate(256)
        self.assertEqual(nw, '239.11.1.0+255')

    def test_out_of_size_allocation(self):
        """Requesting a network larger than the managed one is an error and raises an exception"""
        with self.assertRaises(RuntimeError):
            ipm = IPManager('239.11.1.148/32')
            ipm.allocate(2)

        with self.assertRaises(RuntimeError):
            ipm = IPManager('239.11.1.0/30')
            ipm.allocate(8)

    def test_subnet_recursion(self):
        # We should be able to get two sub-networks ..
        ipm = IPManager('239.11.1.0/29')
        nw1 = ipm.allocate(4)
        nw2 = ipm.allocate(4)
        self.assertEqual(nw1, '239.11.1.0+3')
        self.assertEqual(nw2, '239.11.1.4+3')

        # ... and then no third
        nw3 = ipm.allocate(4)
        self.assertIsNone(nw3)

    def test_mixed_size(self):
        """Ensure that allocations of sub-networks of mixed size are possible"""
        ipm = IPManager('239.11.1.0/29')
        self.assertTrue(isinstance(ipm.allocate(1), str))
        self.assertTrue(isinstance(ipm.allocate(4), str))
        self.assertIsNone(ipm.allocate(4))
        self.assertTrue(isinstance(ipm.allocate(1), str))
        self.assertTrue(isinstance(ipm.allocate(2), str))
        self.assertIsNone(ipm.allocate(1))

    def test_free(self):
        # Allocate whole network
        ipm = IPManager('239.11.1.0/29')
        nw1 = ipm.allocate(4)
        nw2 = ipm.allocate(4)
        self.assertEqual(nw1, '239.11.1.0+3')
        self.assertEqual(nw2, '239.11.1.4+3')

        # After freeing the second subnet, it should be able to reallocate it
        ipm.free(nw2)
        nw3 = ipm.allocate(4)
        self.assertEqual(nw3, '239.11.1.4+3')
        # now we should not get a single IP again
        self.assertIsNone(ipm.allocate(1))

        # but if we free another part, we should be able get some more
        ipm.free(nw1)
        self.assertTrue(isinstance(ipm.allocate(2), str))
        self.assertTrue(isinstance(ipm.allocate(2), str))
        # Until we have allocated all again
        self.assertIsNone(ipm.allocate(1))

    def test_block(self):
        ipm = IPManager('239.11.1.0/29')
        ipm.block('239.11.1.0+3')
        # First allocation thus should be second subnet, and second allocation
        # should be None as all was allocated
        nw1 = ipm.allocate(4)
        nw2 = ipm.allocate(4)
        self.assertEqual(nw1, '239.11.1.4+3')
        self.assertIsNone(nw2)

        # Same if only one address of first block is allocated
        ipm = IPManager('239.11.1.0/29')
        ipm.block('239.11.1.0')
        # First allocation thus should be second subnet, and second allocation
        # should be None as all was allocated
        nw1 = ipm.allocate(4)
        nw2 = ipm.allocate(4)
        self.assertEqual(nw1, '239.11.1.4+3')
        self.assertIsNone(nw2)


    def test_large_net(self):
        ipm = IPManager('239.32.0.0/16')

        _log.debug('Set log level temporarily to INFO to avoid too much output')
        log_level = logger.logger.level
        logger.setLevel('INFO')

        for i in range(8192):
            ipm.allocate(4)

        for i in range(128):
            ipm.allocate(64)
        logger.setLevel(log_level)
        _log.debug('Restored log level')


if __name__ == '__main__':
    unittest.main()
