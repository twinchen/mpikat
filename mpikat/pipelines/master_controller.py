"""
The Master Controller provides a single interface to all pipeline components in the EDD. State
changes are transmitted in parallel to the individual pipeline components. On
configuration, their dependencies are taken into account.

Configuration Settings
----------------------

The configuration dict contains the configurations of the individual product
pipelines, using their id as key.

In addition, the connection to the data store can be configured here with
the entry ``"data_store" = {"ip": "aaa.bbb.ccc.ddd", port:6347}``

"""
import json
import os
import io
import tempfile
import time
import datetime
import asyncio
import copy
import traceback
import git
import yaml
import fnmatch
import networkx as nx
import ipaddress
from subprocess import Popen, PIPE

from aiokatcp.connection import FailReply
from aiokatcp.server import RequestContext
from aiokatcp.sensor import Sensor

from mpikat.core.product_controller import ProductController
from mpikat.core.edd_pipeline_aio import EDDPipeline, getArgumentParser, launchPipelineServer, state_change, StateChangeFail
from mpikat.core.datastore import EDDDataStore
from mpikat.core import value_list
from mpikat.core import logger as logging
from mpikat.core.katcp_client import KATCPClient
from mpikat.utils.process_tools import command_watcher, ProcessException
from mpikat.utils import ip_manager

_log = logging.getLogger("mpikat.pipelines.EddMasterController")


async def gather_and_throw(tasks):
    """
    Gather tasks results. Waits until all tasks are completed and checks then
    if an error was thrown. Any error is converted to a runtime error.
    """
    res = await asyncio.gather(*tasks, return_exceptions=True)
    _log.trace("Results: %s", res)
    if errors := [c for c in res if isinstance(c, Exception)]:
        msg = "Error:\n" + ";;;;;;\n ".join([repr(r) + " in " + "".join(traceback.format_list(traceback.extract_tb(r.__traceback__))) for r in errors])
        _log.error(msg)
        raise RuntimeError(msg)
    return res



def config_update(cfg, update):
    """
    Updates the config dictionary cfg with values in update. Keys in update containing *,? are applied to all cfgs that match the glob pattern
    """
    result = copy.deepcopy(cfg)
    for k, v in update.items():
        match = fnmatch.filter(cfg.keys(), k)
        for m in match:
            result[m].update(v)
    return result


class ConfigurationGraph:
    def __init__(self):
        self.graph_sensor = Sensor(
            str, "configuration_graph",
            description="Graph of configuration",
            initial_status=Sensor.Status.UNKNOWN)

        self.dag = nx.DiGraph()

    def clear(self):
        self.dag.clear()
        self.graph_sensor.set_value("")

    def update(self, product_list):
        """
        Builds dag of list of (connected) products.
        """
        # Data streams are only filled in on final configure as they may
        # require data from the configure command of previous products. As example, the packetizer
        # data stream has a sync time that is propagated to other components
        # The components are thus configured following the dependency tree,
        # which is a directed acyclic graph (DAG)
        _log.debug("Build DAG from config")
        for product, product_config in product_list.items():
            _log.debug("Adding node: %s", product)
            self.dag.add_node(product)
            if "input_data_streams" in product_config:
                for stream in value_list(product_config["input_data_streams"]):
                    if not stream["source"]:
                        _log.warning("Ignoring stream without source for DAG from %s", product)
                        continue
                    source_product = stream["source"].split(":")[0]
                    if source_product not in product_list:
                        raise FailReply("{} requires data stream of unknown product {}".format(product, stream["source"]))
                    _log.debug("Connecting: %s -> %s", source_product, product)
                    self.dag.add_edge(source_product, product)

        txt_graph = "\n".join(["  {} --> {}".format(k[0], k[1]) for k in self.dag.edges()])
        _log.info("Dependency graph of products:\n%s", txt_graph)
        self.dag.graph['edges'] = {'arrowsize': '4.0'}
        self.dag.graph['node'] = {'shape': 'box'}
        self.dag.graph['graph'] = {'rankdir': 'LR'}

        ggraph = nx.nx_agraph.to_agraph(self.dag)
        with io.BytesIO() as f:
            ggraph.draw(f, format='svg', prog='dot')
            f.seek(0)
            self.graph_sensor.set_value(f.read())
        return self.dag

    def has_cycle(self):
        """
        returns true if the current graph has a cycle.
        """
        _log.debug("Checking for loops in graph")
        try:
            nx.find_cycle(self.dag)
            return True
        except nx.NetworkXNoCycle:
            return False


def get_ansible_fail_message(output):
    """
    Parses ansible output to get the fail message
    """
    error_message = "unknown error"

    if isinstance(output, str):
        try:
            output = json.loads(output)
        except json.decoder.JSONDecodeError as E:
            _log.error(E)
            return error_message

    for h in output['plays'][-1]['tasks'][-1]['hosts'].values():
        if h['failed']:
            error_message = h['msg']

    return error_message



async def get_ansible_inventory(inventory: str, host: str="localhost") -> dict:
    """Returns the ansible inventory as dictionary

    Args:
        inventory (str): The directory of the inventory
        host (str, optional): the host. Defaults to "localhost".

    Returns:
        dict: The inventory as dictionary
    """
    cmd = ["ansible-inventory", "-i", inventory, "--host", host]
    stdout, stderr = await command_watcher(cmd, timeout=10)
    if stderr:
        _log.error("Command %s end with error %s", cmd, stderr)
    try:
        res = json.loads(stdout)
    except json.JSONDecodeError:
        _log.error("Failed to parse inventory"); return
    return res


class EddMasterController(EDDPipeline):
    """
    The main KATCP interface to the EDD backend.
    """

    def __init__(self, ip, port, redis_ip, redis_port,
                 edd_ansible_git_repository_folder, inventory,
                 multicast_managed_range="239.0.0.0/16",
                 loop=None):
        """
        Args:

          ip:           The IP address on which the server should listen
          port:         The port that the server should bind to
          redis_ip:     IP for connection to the EDD data-store
          redis_port:   Port for the connection to the EDD data-store
          multicast_managed_range: Range of multicast addresses from which ranges are assigned automatically
          edd_ansible_git_repository_folder:
                        Directory of a (checked out) edd_ansible git repository
                        to be used for provisioning inventory to use for Ansible
        """
        self.configuration_graph = ConfigurationGraph()
        super().__init__(
            ip, port,
            {"data_store": dict(ip=redis_ip, port=redis_port),
             "id":"There can be only one.",
             "multicast_managed_range": multicast_managed_range,
             "products": {}
             },
            loop=loop
        )
        self.state = "unprovisioned"
        self.__controller = {}
        self.edd_data_store = EDDDataStore(redis_ip, redis_port)
        self.__edd_ansible_git_repository_folder = edd_ansible_git_repository_folder
        self.__inventory = inventory
        if not os.path.isdir(self.__edd_ansible_git_repository_folder):
            _log.warning("%s is not a readable directory", self.__edd_ansible_git_repository_folder)

        self.__provisioned = None
        self._inventory_description = {}

    # ------------- #
    # Basic methods #
    # ------------- #
    def setup_sensors(self):
        """
        Setup katcp monitoring sensors.
        """
        super().setup_sensors()

        self.sensors.add(self.configuration_graph.graph_sensor)
        self.sensors.add(Sensor(
            str, "provision",
            description="Current provision configuration",
            initial_status=Sensor.Status.UNKNOWN))

        self.sensors.add(Sensor(
            str, "product-status-summary",
            description="Status of all controlled products",
            default="{}",
            initial_status=Sensor.Status.UNKNOWN))

        self.sensors.add(Sensor(
            str, "provision-repository-version",
            description="Version of the provision repository",
            default="Unknown",
            initial_status=Sensor.Status.UNKNOWN))

        self.sensors.add(Sensor(
            str, "provision_time",
            description="Time the system was provisioned",
            default="Unknown",
            initial_status=Sensor.Status.UNKNOWN))

        self.sensors.add(Sensor(
            int, "configure_counter",
            description="Counts the number of configure calls within one provision",
            default=0,
            initial_status=Sensor.Status.NOMINAL))

        self.sensors.add(Sensor(
            int, "measurement_prepare_counter",
            description="Counts the number of measurement_prepare calls within one configuration",
            default=0,
            initial_status=Sensor.Status.NOMINAL))

    def get_edd_id(self):
        """
        Return a unique id for the measurement
        """
        return f"EDDID_{self.sensors['provision_time'].value}_{self.sensors['configure_counter'].value}.{self.sensors['measurement_prepare_counter'].value}"


    async def start(self):
        """
        Reimplement start function: We need to add a service task
        """
        await super().start()
        self.add_service_task(asyncio.create_task(self.periodicProductStusSummaryUpdate())) # Check for correctness


    @classmethod
    def auto_assign_ip_addresses_to_outputs_streams(self, config, ipmanager=None):
        """
        Return config with very output stream with 'ip': 'auto+X' has assigned a unique stream destination. from the range of the ipmanager.

        If no ip manager is specified, one is constructed using the 'multicast_managed_range' option in the config.
        """
        _log.debug('Assigning multicast IPs automatically to output streams. ')
        # Create a deep copy to ensure that the input is not modified
        config = copy.deepcopy(config)
        if not ipmanager:
            ipmanager = ip_manager.IPManager(config['multicast_managed_range'])
        _log.debug('Taking addresses from nework %s', ipmanager.network)

        _log.debug('Blocking manually allocated streams')
        for product, productcfg in config['products'].items():
            if not 'output_data_streams' in productcfg:
                _log.debug('Product %s does not have output_data_streams, nothing to block', product)
                continue
            for o in value_list(productcfg['output_data_streams']):
                if not 'ip' in o or not o['ip']:
                    _log.debug('Product %s output_data_stream without ip', product)
                    continue

                if o['ip'].startswith('auto'):
                    _log.debug('Stream of %s does not have output_data_streams, nothing to block', product)
                    # We do this later after we have blocked all manually
                    # assigned streams
                    continue
                try:
                    ipmanager.block(o['ip'])
                except ipaddress.AddressValueError:
                    _log.warning("Error processing IP '%s' of product %s ", o['ip'], product)

        _log.debug('Assigning IPs')
        for product, productcfg in config['products'].items():
            if not 'output_data_streams' in productcfg:
                _log.debug('Product %s does not have output_data_streams, nothing to assign', product)
                continue
            for o in value_list(productcfg['output_data_streams']):
                if not 'ip' in o:
                    _log.debug('Product %s output_data_streams without ip', product)
                    continue
                if o['ip'].startswith('auto'):
                    _log.debug('Stream of %s requests auto assigned IP', product)
                    if "+" not in o['ip']:
                        N = 1
                    else:
                        N = int(o['ip'].split('+')[1]) + 1
                    dest = ipmanager.allocate(N)
                    _log.debug("Allocated IP %s to stream of product '%s'", dest, product)
                    o['ip'] = dest

        return config




    def updateProvisionRepositorySensor(self):
        """
        Check the provision repository and updates the sensor
        """
        repo = git.Repo(self.__edd_ansible_git_repository_folder)
        comm = repo.commit()
        if repo.is_dirty():
            mod = "+localMODIFICATIONS"
        else:
            mod = ""

        vs = "{sha}{mod} - {date}".format(sha=comm.hexsha, mod=mod, date=comm.authored_datetime.isoformat())

        self.sensors['provision-repository-version'].set_value(vs)

    @classmethod
    def _sanitizeConfig(cls, config):
        """
        Ensures config products are a dict with product['id'] as key.
        """
        _log.debug("Sanitze config")
        if not 'products' in config:
            config["products"] = {}
        elif isinstance(config['products'], list):
            d = {p['id']:p for p in config['products']}
            config['products'] = d
        return config


    # ------------------------ #
    # Common request functions #
    # ------------------------ #
    async def request_list_provisions(self, ctx: RequestContext):
        """
        List all provision descriptions available in the
        edd_ansible_git_repository_folder.
        """
        self.updateProvisionRepositorySensor()
        all_files = os.listdir(os.path.join(self.__edd_ansible_git_repository_folder, "provison_descriptions"))
        yml_files = [f for f in all_files if f.endswith('.yml')]
        l = [" - {}".format(l[:-4]) for l in yml_files if l[:-4] + '.json' in all_files]
        ctx.reply("ok", "\nAvailable provision descriptions:\n" +"\n".join(l))


    async def request_load_basic_config(self, ctx: RequestContext, name: str):
        """
        Loads a provision configuration and dispatch it to Ansible and sets the
        data streams for all products

        Note:
            The provision request already loads the basic configuration so that
            this request does NOT need to be send - it is intended as an
            development tool only.
        """
        _log.info("load-basic-config request received")
        descr_subfolder = os.path.join(self.__edd_ansible_git_repository_folder, "provison_descriptions")
        basic_config_file = os.path.join(descr_subfolder, name)
        try:
            await self._loadBasicConfig(basic_config_file)
        except FileNotFoundError as E:
            raise FailReply(f'No such file: {basic_config_file}')


    async def request_provision_update(self, ctx: RequestContext):
        """
        Clones or pulls updates for the git repository

        """
        await self.provision_update()


    # -------------------------------------- #
    # Requests coroutines for state changes  #
    # -------------------------------------- #
    async def request_provision(self, ctx: RequestContext, name: str):
        """
        Loads a provision configuration and dispatch it to Ansible, sets and
        connects the data streams for all products and load their initial
        configuration.
        """
        _log.info("Provision request received")

        self.updateProvisionRepositorySensor()
        await self.provision(name)


    async def request_deprovision(self, ctx: RequestContext):
        """
        Deprovision EDD - stop all Ansible containers launched in recent
        provision cycle and removes all controllers  from the data base.
        """
        _log.info("Deprovision request received")
        self._cancel_pipeline_tasks()
        try:
            if self.state != 'idle':
                await self.deconfigure()
        except Exception as E:
            _log.warning('Error during deconfiguring! Continue with deprovision as requested!\n%s', E)
        await self.deprovision()


    # -------------------------- #
    # Common coroutines (public) #
    # -------------------------- #
    async def updateProductStatusSummary(self):
        """
        Checks if all controlled products are reachable and writes the pipeline
        status as a JSON object to the sensor.
        """

        tasks = []
        status_summary = {}

        for product_id, controller in self.__controller.items():
            _log.trace('Creating task for %s', product_id)
            status_summary[product_id] = "UNREACHABLE"
            tasks.append(asyncio.create_task(controller.getState()))
        res = await asyncio.gather(*tasks, return_exceptions=True)
        # do not loop over controllers again as they might have changed
        # during async wait
        for product_id, r in zip(status_summary.keys(), res):
            if isinstance(r, Exception):
                _log.error('Exception during retrival of status')
                _log.exception(r)
                continue
            if product_id not in self.__controller:
                _log.warning('Controller changed during status update: %s unknown', product_id)
                continue
            status_summary[product_id] = r

        _log.trace('Status summary: %s', status_summary)
        for product_id, status in status_summary.items():
            if status == "UNREACHABLE":
                _log.warning("Error getting status of product %s, product unreachable", product_id)

        self.sensors['product-status-summary'].set_value(json.dumps(status_summary, indent=4))


    async def periodicProductStusSummaryUpdate(self, max_time: float = 5):
        """Periodically updates status summary

        Args:
            max_time (float): the maximum time between consecutive product updates
                -> used in tests/test_master_controller.py to reduce test time
        """
        while True:
            if self.state in ["provisioning", "deprovisioning"]:
                _log.trace("Skipping pipeline update due to provisioning action")
                await asyncio.sleep(1)
                continue

            start = time.time()
            await self.updateProductStatusSummary()
            duration = time.time() - start
            _log.trace("Updated product status. Retrival took %.2f s", duration)
            await asyncio.sleep(max(max_time - duration, 0.5))



    async def set(self, config_json: str) -> None:
        """
        Sets the configuration of the MasterController object
        """
        try:
            cfg = json.loads(config_json)
        except:
            _log.error("Error parsing json")
            raise FailReply("Cannot handle config string {} - Not valid json!".format(config_json))

        if not self.__provisioned:
            _log.debug("Not provisioned. Using full config.")
            # Do not use set here, as there might not be a basic config from
            # provisioning
            cfg = self._sanitizeConfig(cfg)
            self._config = cfg
        else:
            await super().set(cfg)


    async def provision_update(self):
        """
        Clones or pulls updates for the git repository
        # ToDo: Shall we check whether the repo is readable MC (e.g. folder provision_description/ exists)
        """

        if not os.path.isdir(self.__edd_ansible_git_repository_folder):
            raise RuntimeError(f"Directory {self.__edd_ansible_git_repository_folder} does not exist!")
        repo = git.Repo(self.__edd_ansible_git_repository_folder)

        if repo.is_dirty():
            raise RuntimeError("Trying to update dirty repository. Please fix manually!")

        comm = repo.remote().pull()[0].commit
        _log.info("Updated to latest commit: %s, %s\n    %s\n\n    %s",
                comm.hexsha, comm.authored_datetime.ctime(), comm.author,
                comm.message)

        self.updateProvisionRepositorySensor()


    # ----------------------------- #
    # Helper coroutines (protected) #
    # ----------------------------- #
    async def _ansible_subplay_executor(self, play, additional_args=""):
        """
        Uses ansible-playbook to execute the given play by writing it in a tempfile.
        Args:
            play                The play to be executed
            additional_args     Additional args added to the Ansible execution, e.g. --tags=stop
        """
        playfile = tempfile.NamedTemporaryFile(delete=False, mode='w')
        _log.debug("Dumping subplay to temporary file %s", playfile.name)
        yaml.dump([play], playfile)
        playfile.close()

        _log.debug("Running subplay")
        try:
            await command_watcher(
                    f'ansible-playbook -i {self.__inventory} {playfile.name} {additional_args}', timeout=300, env={"ANSIBLE_CALLBACK_WHITELIST": "json", "ANSIBLE_STDOUT_CALLBACK": "json"}
            )
        finally:
            os.unlink(playfile.name)


    async def _installController(self, config=None):
        """
        Ensures that a controller for all products known to Redis exists. In
        case a config is provided it is also ensures that the controller exists for
        all products in the config.

        Note:
            If a config contains a product unknown to the data store, the
            configuration must provide **IP** and **port** of the product.
        """
        if config is None:
            config = {}
        await self._installControllerByDataStore()
        if config:
            await self._installControllerByConfig(config)


    async def _installControllerByDataStore(self) -> None:
        """
        Ensures that a controller for all products known to Redis exists.
        """
        _log.debug("Installing controller for {} registered products.".format(len(self.edd_data_store.products)))
        for product in self.edd_data_store.products:
            if product['id'] == self._config["id"]:
                _log.debug("Ignoring self for control: %s", product['id'])
                continue

            if product['id'] in self.__controller:
                _log.debug("Controller for %s already installed.", product['id'])
                controller = self.__controller[product['id']]
                if (product['ip'] == controller.ip) and (product['port'] == controller.port):
                    # No action needed as identical product
                    _log.debug("Ip and port matching, doing nothing.")
                    continue

                _log.debug("Ip and port not matching, Checking controller health.")
                ping = await controller.ping()
                if ping:
                    _log.warning("Controller for %s already present at %s:%i and reachable. Not updating with new controller for product at %s:%s", product, controller.ip, controller.port, product['ip'], product['port'])
                    continue
                _log.warning("Controller for %s already present but not reachable at %s:%i. Replacing with new controller for product at %s:%i", product, controller.ip, controller.port, product['ip'], product['port'])

            controller = ProductController(product['id'], product["ip"], product["port"])
            ping = await controller.ping()
            if ping:
                _log.debug("Reached product %s at %s:%i.", product['id'], product["ip"], int(product["port"]))
                self.__controller[product['id']] = controller
            else:
                _log.debug("Cannot reach product %s at %s:%i. Removing product from Redis.", product['id'], product["ip"], product["port"])
                self.edd_data_store.removeProduct(product)


    async def _installControllerByConfig(self, config=None):
        """
        In case a config is provided it is also ensures that the controller exists for
        all products in the config.
        """
        if config is None:
            config = {}
        _log.debug("Checking products for config")
        for product in config.values():
            if product['id'] not in self.__controller and not product.get('ignore_pipeline_errors'):
                _log.warning('Product %s found in configuration, but no product registered. Manually adding controller for product.', product['id'])
                if ('ip' not in product) or ('port' not in product):
                    raise RuntimeError("No controller for {} could be installed because pipele was not registered. Manual config requires IP and PORT, which were not provided!".format(product['id']))
                _log.warning('Manually installed product %s at %s:%i', product['id'], product['ip'], product['port'])
                self.__controller[product['id']] = ProductController(product['id'], product["ip"], product["port"])
            # In case the pipeline ignores pipeline errors, the product is not in the self.__controller dictionary.
            # It seems this is false.
            self.__controller[product['id']].ignore_errors = product.get('ignore_pipeline_errors')

            ping = await self.__controller[product['id']].ping()
            if not ping and not product.get('ignore_pipeline_errors'):
                raise RuntimeError('Product {} required by config but not reachable.'.format(product['id']))


    async def _loadBasicConfig(self, basic_config_file: dict):
        """
        Actually loads the basic configuration, called by provision and load
        basic configuration requests.
        Args:
            basic_config_file (dict): A JSON encoded file to load
        """
        with open(basic_config_file, encoding='utf-8') as cfg:
            basic_config = json.load(cfg)
        _log.debug("Read basic config: %s", json.dumps(basic_config, indent=4))

        basic_config = self._sanitizeConfig(basic_config)

        await self._installController(basic_config['products'])

        # Retrieve default configs from products and merge with basic config to
        # have full config locally.
        self._config = self._default_config.copy()

        self._config["products"] = {}

        for product in basic_config['products'].values():
            _log.debug("Retrieve basic config for %s", product["id"])
            controller = self.__controller[product["id"]]

            _log.debug("Checking basic config %s", json.dumps(product, indent=4))
            await controller.set(product)
            cfg = await controller.getConfig()
            _log.debug("Got: %s", json.dumps(cfg, indent=4))

            cfg['data_store'] = self._default_config['data_store']
            self._config["products"][cfg['id']] = cfg

        self._configUpdated()
        self.configuration_graph.update(self._config['products'])


    # ----------------------- #
    # State change coroutines #
    # ----------------------- #
    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Configure the EDD backend
        """
        _log.info("Configuring EDD backend for processing")
        self.sensors['configure_counter'].value += 1
        self.sensors['measurement_prepare_counter'].value = 0
        self._config = self.auto_assign_ip_addresses_to_outputs_streams(self._config)

        await self._installController(self._config['products'])

        cfs = json.dumps(self._config, indent=4)
        _log.debug("Starting configuration:\n%s", cfs)

        self.configuration_graph.update(self._config['products'])

        configure_futures = []
        configure_results = {}

        async def __process_node(node):
            """
            Wrapper to parallelize configuration of nodes. Any Node will wait for its predecessors to be done.
            """
            #Wait for all predecessors to be finished
            _log.debug("DAG Processing {}: Waiting for {} predecessors"\
                .format(node, len(list(self.configuration_graph.dag.predecessors(node))))
            )
            for pre in self.configuration_graph.dag.predecessors(node):
                _log.debug('DAG Processing {}: waiting for {}'.format(node, pre))
                while not pre in configure_results:
                    # python3 asyncio co-routines would not run until awaited,
                    # so we could build the graph up front and then execute it
                    # without waiting
                    await asyncio.sleep(0.5)
                _log.debug('DAG Processing {}: Predecessor {} done.'.format(node, pre))
                if not configure_results[pre]:
                    _log.error('DAG Processing {}: fails due to error in predecessor {}'.format(node, pre))
                    configure_results[node] = False
                    return
                _log.debug('DAG Processing {}: Predecessor {} was successfull.'.format(node, pre))

            _log.debug("DAG Processing {}: All predecessors done.".format(node))
            try:
                _log.debug("DAG Processing {}: Checking input data streams for updates.".format(node))
                if "input_data_streams" in self._config['products'][node]:
                    _log.debug('DAG Processing {}: Update input streams'.format(node))
                    for stream in value_list(self._config['products'][node]["input_data_streams"]):
                        product_name, stream_name = stream["source"].split(":")
                        if product_name not in self._config['products']:
                            raise RuntimeError("Unknown product: {}".format(product_name))
                        if stream_name not in self._config['products'][product_name]["output_data_streams"]:
                            raise RuntimeError("Unknown stream {} for product {}".format(stream_name, product_name))
                        stream.update(self._config['products'][product_name]["output_data_streams"][stream_name])

                _log.debug('DAG Processing {}: Set Final config'.format(node))
                await self.__controller[node].set(self._config['products'][node])
                _log.debug('DAG Processing {}: Staring configuration'.format(node))
                await self.__controller[node].configure()
                _log.debug("DAG Processing {}: Getting updated config".format(node))
                cfg = await self.__controller[node].getConfig()
                _log.debug("Got: {}".format(json.dumps(cfg, indent=4)))
                self._config["products"][node] = cfg

            except Exception as E:
                _log.error('DAG Processing: {} Exception cought during configuration:\n {}:{}'.format(node, type(E).__name__, E))
                configure_results[node] = False
            else:
                _log.debug('DAG Processing: {} Successfully finished configuration'.format(node))
                configure_results[node] = True

        _log.debug("Creating processing futures")
        for node in self.configuration_graph.dag.nodes():
            configure_futures.append(asyncio.create_task(__process_node(node)))
        await gather_and_throw(configure_futures)

        self._configUpdated()
        _log.debug("Final configuration:\n '{}'".format(json.dumps(self._config, indent=2)))
        failed_prcts = [k for k in configure_results if not configure_results[k]]
        if failed_prcts:
            _log.error("Failed products: %s", ",".join(failed_prcts))
            raise FailReply("Failed products: {}".format(",".join(failed_prcts)))
        _log.info("Updating data streams in database")
        for productname, product in self._config["products"].items():
            _log.debug(" - Checking {}".format(productname))
            if "output_data_streams" in product and isinstance(product["output_data_streams"], dict):
                for stream, streamcfg in product["output_data_streams"].items():
                    key = "{}:{}".format(productname, stream)
                    self.edd_data_store.addDataStream(key, streamcfg)

        _log.info("Successfully configured EDD")


    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """
        Implementation of the de-configuration request.
        """
        _log.info("Deconfiguring all products:")
        _log.debug("Sending deconfigure to %i products: %s", len(self.__controller.keys()), "\n - ".join(self.__controller.keys()))
        self.sensors['measurement_prepare_counter'].value = 0

        tasks = []
        for __, controller in self.__controller.items():
            tasks.append(asyncio.create_task(controller.deconfigure()))
        await gather_and_throw(tasks)
        # After deconfigure, there are no more data-streams
        _log.debug("Clearing datastreams")
        self.edd_data_store._dataStreams.flushdb()
        _log.debug("Flushing notes")
        self.flush_notes()
        _log.debug("Done deconfiguring")


    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        """
        Implementation of the capture-start.
        """
        _log.debug("Sending capture_start to {} products: {}".format(len(self.__controller.keys()), "\n - ".join(self.__controller.keys())))
        tasks = []
        for controller in self.__controller.values():
            tasks.append(asyncio.create_task(controller.capture_start()))
        await gather_and_throw(tasks)


    @state_change(target="idle", allowed=["streaming", "deconfiguring"], intermediate="capture_stopping")
    async def capture_stop(self):
        """
        Stop the EDD backend processing
        """
        _log.debug("Sending capture_stop to {} products: {}".format(len(self.__controller.keys()), "\n - ".join(self.__controller.keys())))
        tasks = []
        for controller in self.__controller.values():
            tasks.append(asyncio.create_task(controller.capture_stop()))
        await gather_and_throw(tasks)


    @state_change(target="set", allowed=["ready"], intermediate="measurement_preparing")
    async def measurement_prepare(self, cfg=None):
        """
        Preparing the measurement

        Args:
            cfg: dictionary of configuration for individual pipelines with pipeline_id as key.

            An item with key '_TMI' will be passed to the TMIServer and may be
            used to actually set values in the datastore on ensure_update.
        """
        self.sensors['measurement_prepare_counter'].value += 1
        if cfg is None:
            cfg = {}

        _log.debug('Connecting to TMI Server')
        tmi_connection_info = self.edd_data_store.getTelescopeDataItem("TMIServer")
        _log.debug('TMI Server@%s', tmi_connection_info)
        if not tmi_connection_info:
            raise RuntimeError("No connection informaton for TMI server")
        tmi_connection = await KATCPClient.connect(*tmi_connection_info.split(':'), timeout=10)
        await tmi_connection.request('ensure-update', json.dumps(cfg.get("_TMI", {})), timeout=60)

        _log.debug("Sending measurement_prepare to {} products: {}".format(len(self.__controller.keys()), "\n - ".join(self.__controller.keys())))

        update_cfg = config_update({k: {} for k in self.__controller}, cfg)

        tasks = []
        for cid, controller in self.__controller.items():

            cidcfg = update_cfg[cid] 
            cidcfg['eddid'] = self.get_edd_id()
            _log.debug("Sending measurement_prepare to %s with %s", cid, cidcfg)
            tasks.append(asyncio.create_task(controller.measurement_prepare(cidcfg)))

        for k in cfg:
            if k not in self.__controller and k != '_TMI':
                self.add_note("WARNING: Measurement prepare requested for unprovisioned product '{}'".format(k))
        await gather_and_throw(tasks)


    @state_change(target="measuring", allowed=["set"], intermediate="measurement_starting")
    async def measurement_start(self):
        _log.debug("Sending measurement_start to {} products: {}".format(len(self.__controller.keys()), "\n - ".join(self.__controller.keys())))
        tasks = []
        for controller in self.__controller.values():
            tasks.append(asyncio.create_task(controller.measurement_start()))
        await gather_and_throw(tasks)


    @state_change(target="ready", allowed=["measuring", "set"], ignored=['ready'], intermediate="measurement_stopping")
    async def measurement_stop(self):
        _log.debug("Sending measurement_stop to %i products: %s", len(self.__controller.keys()), "\n - ".join(self.__controller.keys()))
        tasks = []
        for controller in self.__controller.values():
            tasks.append(asyncio.create_task(controller.measurement_stop(True)))
        res = await gather_and_throw(tasks)
        try:
            db_path = os.path.join(self._inventory_description["data_base_path"], 'pipeline_data')
        except KeyError:
            _log.error("Could not get 'data_base_path' from the used inventory")
            db_path = ""
        for name, rj in zip(self.__controller, res):
            try:
                r = json.loads(rj)
            except (TypeError, json.JSONDecodeError) as E:
                _log.error('Error decoding json reply from %s', name)
                _log.error('Reply: %s', rj)
                _log.exception(E)
                continue
            for f, k in r.items():
                fn = os.path.join(db_path, name, f)
                k.setdefault('pipeline_id', name)
                self.add_output_file(fn, k)


    @state_change(target="idle", intermediate="provisioning", allowed=['unprovisioned'], timeout=300)
    async def provision(self, description: str):
        """
        Provision the EDD using the provided provision description.

        Args:
            description: description of the provision. This has to be a string of format

                - ::`NAME`                 to load NAME.json and NAME.yml, or
                - ::`NAME1.yml;NAME2.json` to load different YAML / JSON configs
        """
        self.sensors['provision_time'].set_value(datetime.datetime.utcnow().replace().isoformat(), Sensor.Status.NOMINAL)
        os.chdir(self.__edd_ansible_git_repository_folder)
        _log.debug("Reading provision description %s from directory %s", description, os.getcwd())
        if description.startswith('"'):
            description = description.lstrip('"')
            description = description.rstrip('"')

        descr_subfolder = "provison_descriptions"
        if ";" in description:
            description = description.split(';')
            if description[0].endswith("yml"):
                playbook_file = os.path.join(descr_subfolder, description[0])
                basic_config_file = os.path.join(descr_subfolder, description[1])
            else:
                playbook_file = os.path.join(descr_subfolder, description[1])
                basic_config_file = os.path.join(descr_subfolder, description[0])
        else:
            playbook_file = os.path.join(descr_subfolder, description + ".yml")
            basic_config_file = os.path.join(descr_subfolder, description + ".json")

        _log.debug("Loading provision description files: %s and %s", playbook_file, basic_config_file)
        if not os.path.isfile(playbook_file):
            raise StateChangeFail("idle", f"Cannot find playbook file {playbook_file} - EDD is NOT provisioned")
        if not os.path.isfile(basic_config_file):
            raise StateChangeFail("idle", f"Cannot find config file {basic_config_file} - EDD is NOT provisioned")

        with open(playbook_file, 'r', encoding='utf-8') as f:
            provision_playbook = yaml.load(f, Loader=yaml.SafeLoader)

        _log.debug("Executing playbook as %i seperate subplays in parallel", len(provision_playbook))

        self.__provisioned = playbook_file
        self.sensors['provision'].set_value(playbook_file)
        subplay_tasks = []

        for play in provision_playbook:
            subplay_tasks.append(asyncio.create_task(self._ansible_subplay_executor(play)))
        result = await asyncio.gather(*subplay_tasks, return_exceptions=True)

        failed_products = []
        for r, p in zip(result, provision_playbook):
            if isinstance(r, Exception):
                _log.error("Error in provisioning thrown by Ansible:\n%s from subplay:\n%s", r, p)
                msg = str(r)
                if isinstance(r, ProcessException):
                    msg = get_ansible_fail_message(r.stdout)

                if not 'roles' in p:
                    failed_products.append(('unknownrole', msg))

                    continue
                for role in p['roles']:
                    if isinstance(role, dict):
                        role = role.get('container_name', '')
                    failed_products.append((role, msg))

        if failed_products:
            msg = "\n".join([f"{r[0]}: {r[1]}" for r in failed_products])
            raise FailReply(f"Error in provisioning - failed products:\n{msg}")

        self._inventory_description = await get_ansible_inventory(self.__inventory)

        await self._loadBasicConfig(basic_config_file)


    @state_change(target="unprovisioned", intermediate="deprovisioning", timeout=240)
    async def deprovision(self):
        """
        Deprovision the EDD.
        """
        _log.debug("Deprovision %s", self.__provisioned)

        for k, c in self.__controller.items():
            _log.debug("Closing connection to %s", k) # Explicitly close connection as there is a reference kept somewhere - the controller are not garbage collected!
            c.close()

        self.__controller = {}
        self._inventory_description = {}
        self.edd_data_store._products.flushdb()
        self.edd_data_store._dataStreams.flushdb()
        if self.__provisioned:
            with open(self.__provisioned, 'r', encoding='utf-8') as f:
                provision_playbook = yaml.load(f, Loader=yaml.SafeLoader)

            self.__provisioned = None

            subplay_tasks = []
            _log.debug("Executing playbook as %i seperate subplays in parallel", len(provision_playbook))
            try:
                for play in provision_playbook:
                    subplay_tasks.append(asyncio.create_task(
                        self._ansible_subplay_executor(play, "--tags=stop")
                    ))
                await gather_and_throw(subplay_tasks)
            except Exception as E:
                raise FailReply("Error in deprovisioning thrown by Ansible: {}".format(E))


        self.sensors['provision'].set_value("Unprovisioned")
        self.sensors['provision_time'].set_value('Unknown', Sensor.Status.UNKNOWN)
        self.sensors['configure_counter'].value = 0
        self.sensors['measurement_prepare_counter'].value = 0
        self.configuration_graph.clear()
        self._config = self._default_config.copy()


if __name__ == "__main__":
    parser = getArgumentParser()

    parser.add_argument('--edd_ansible_repository', dest='edd_ansible_git_repository_folder',
                        type=str, default=os.path.join(os.getenv("HOME"), "edd_ansible"),
                        help='The path to a git repository for the provisioning data')

    parser.add_argument('--edd_ansible_inventory', dest='inventory', type=str,
                        default="effelsberg", help='The inventory to use with the Ansible setup')

    parser.add_argument('--multicast_network', type=str,
                        default="239.0.0.0/16", help='Address bloc to use for automatic multicast stream assignment')

    args = parser.parse_args()
    logging.setLevel(args.log_level.upper())

    async def wrapper():
        server = EddMasterController(
            args.host, args.port,
            args.redis_ip, args.redis_port,
            args.edd_ansible_git_repository_folder, args.inventory, args.multicast_network
        )
        await launchPipelineServer(server, args)
    asyncio.run(wrapper())
