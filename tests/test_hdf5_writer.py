import unittest
import tempfile
import os
import asyncio

from mpikat.pipelines.hdf5_writer import HDF5Writer
from mpikat.core import logger
from mpikat.utils import testing, get_port

_log = logger.getLogger("mpikat.testHDF5Writer")

class TestHDF5Writer(unittest.IsolatedAsyncioTestCase):

    def setUp(self) -> None:
        self.server_thread = testing.launch_server_in_thread(HDF5Writer, "127.0.0.1", get_port())

    async def asyncTearDown(self) -> None:
        self.server_thread.stop().result()
        self.server_thread.join(timeout=5)


    async def test_file_creation(self):
        """
        File should be created during measurement start-sop cycle
        """
        self.assertEqual(self.server_thread.server.state, 'idle')

        with tempfile.TemporaryDirectory() as tmpdirname:
            await self.server_thread.server.set(dict(use_date_based_subfolders=False))

            await self.server_thread.server.configure()
            self.assertEqual(self.server_thread.server.state, 'configured')
            await self.server_thread.server.capture_start()
            self.assertEqual(self.server_thread.server.state, 'ready')
            fname = os.path.join(tmpdirname, 'foo.h5')
            await self.server_thread.server.measurement_prepare({"filename": fname, "new_file": True})
            self.assertEqual(self.server_thread.server.state, 'set')
            await self.server_thread.server.measurement_start()
            self.assertEqual(self.server_thread.server.state, 'measuring')
            await self.server_thread.server.measurement_stop()
            self.assertTrue(os.path.isfile(fname))
        await self.server_thread.server.deconfigure()
        self.assertEqual(self.server_thread.server.state, 'idle')

if __name__ == '__main__':
    unittest.main()
