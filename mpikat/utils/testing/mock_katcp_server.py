import asyncio
import unittest.mock

import aiokatcp

from mpikat.core import logger
from mpikat.utils import get_port

class MockKatcpServer(aiokatcp.DeviceServer):
    """
    Mock a generic katcp device.

    Attributes:
        fail [Boolean] if True, a FailReply will be raised for all requests
    """

    def __init__(self, name, requests=None, *args, **kwargs):
        """
        Args:
            name: Name of the server to mock
            requests: List of requests the service should mock
        """
        self.VERSION = f"mock_{name}-api1.0"
        self.BUILD_STATE = f"mock_{name}-1.0.1"

        self.log = logger.getLogger(f"mpikat.testing.mock{name}")

        self.port = get_port()

        self.log.debug("Binding to port %i", self.port)
        aiokatcp.DeviceServer.__init__(self, "127.0.0.1", self.port, *args, **kwargs)
        self.mock = unittest.mock.Mock()

        if requests is None:
            requests = {}

        for r in requests:
            self.add_request(r)

        self.fail = False

    def add_request(self, name, method=None):
        """
        Add a new request to the mock server.
        Calls a mock if no alternative method are given.
        """
        f = self.mock.__getattr__(name)
        self.log.debug("Adding request %s", name)

        async def wrapper(self, ctx, msg):
            f(*[a.decode("utf-8") for a in msg.arguments])
            self.log.debug(
                "Request %s called - call_count is now %i", name, f.call_count
            )
            if self.fail:
                self.log.debug("Request %s returns FailReply as requested", name)
                raise aiokatcp.FailReply("failed, as requested")

        if method:
            self._request_handlers[name.replace("_", "-")] = method
        else:
            self._request_handlers[name.replace("_", "-")] = wrapper


def MockEDDMasterController(*args, **kwargs):
    """
    Return a MockKATCPServer to mock an EDD MasterController.
    """
    mockEDD = MockKatcpServer(
        "EDD_MasterController",
        requests=[
            "deconfigure",
            "configure",
            "capture_start",
            "capture_stop",
            "measurement_start",
            "measurement_stop",
            "measurement_prepare",
        ], *args, **kwargs
    )
    mockEDD.sensors.add(aiokatcp.Sensor(str, "provision", default="UNPROVISIONED"))
    mockEDD.sensors.add(aiokatcp.Sensor(str, "current-config", default='{"products":{}}'))
    mockEDD.sensors.add(aiokatcp.Sensor(str, "configuration_graph", default='{"products":{}}'))
    mockEDD.sensors.add(
        aiokatcp.Sensor(str, "pipeline-status", default="UNPROVISIONED")
    )

    async def deprovision_request(self, *args):
        self.log.trace("Deprovision called: %s")
        self.sensors["provision"].set_value("Unprovisioned")
        self.mock.deprovision()

    mockEDD.deprovision_request = deprovision_request
    mockEDD.add_request("deprovision", mockEDD.deprovision_request)


    async def provision_request(self, *args):

        self.log.debug("Mock provision called with arguments %s", args)
        prov = args[1].arguments[-1].decode("utf-8")
        self.log.trace("Provision called: %s", prov)
        self.sensors["provision"].set_value(prov + ".yml")
        self.mock.provision(prov)
        if self.fail:
            raise aiokatcp.FailReply("provision failed, as requested")

    mockEDD.provision_request = provision_request
    mockEDD.add_request("provision", mockEDD.provision_request)
    return mockEDD


if __name__ == "__main__":

    async def main():
        """
        Example usage
        """
        server = MockKatcpServer(name="test_server", requests=["foo", "bar"])
        asyncio.create_task(server.start())
        client = await aiokatcp.Client.connect("127.0.0.1", server.port)
        await client.request("foo", timeout=4)
        await client.request("bar")
        await client.request("foo")

    asyncio.run(main())
