# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring

import unittest
import io
import contextlib
import pickle
import tempfile

import aiokatcp

from mpikat.utils.testing.provision_tests import register, Stage, run_tests, run_test, TerminalOutput, EDDTest, EDDTestResult, StageSummary, HTMLOutput, MultiOutput, EDDTestContext, TestState
from mpikat.utils.testing.provision_tests.registry import Registry
from mpikat.utils.testing import mock_katcp_server, server_thread
from mpikat.pipelines.pulsar_processing_blank_image import BLANK_IMAGE


class TestRegistry(unittest.TestCase):
    def setUp(self):
        self.registry = Registry()

    def test_add_and_clear_pipeline_name(self):
        """ Test adding and clearing tests to the registry"""
        for s in Stage:
            self.assertEqual(len(self.registry.get_tests_for_stage(s)), 0)

        def dummy():
            pass
        t = EDDTest(Stage.provision, pipeline_name='foo', func=dummy)
        self.registry.add(t)
        self.assertEqual(len(self.registry.get_tests_for_stage(Stage.provision)), 1)

        self.registry.clear()
        self.assertEqual(len(self.registry.get_tests_for_stage(Stage.provision)), 0)

        self.assertEqual(len(self.registry.tests_classes), 0)

    def test_decorator_by_pipeline_name(self):
        """ Test adding via decorator"""
        self.assertEqual(len(self.registry.get_tests_for_stage(Stage.provision)), 0)

        @register(stage=Stage.provision, pipeline_name='foo', registry=self.registry)
        async def test_foo(context):  # pylint: disable=unused-argument, unused-variable
            pass

        self.assertEqual(len(self.registry.get_tests_for_stage(Stage.provision)), 1)

    def test_decorator_by_class(self):
        self.assertEqual(len(self.registry.tests_classes), 0)

        @register(stage=Stage.provision, class_name='foo', registry=self.registry)
        async def test_foo(context):  # pylint: disable=unused-argument, unused-variable
            pass

        self.assertEqual(len(self.registry.tests_classes), 1)
        self.assertEqual(len(self.registry.get_tests_for_stage(Stage.provision)), 0)


        self.registry.add_class_tests({'bar':{'type': 'foo'}, 'gna':{'type': 'foo'}, 'spam':{'type': 'eggs'}})

        self.assertEqual(len(self.registry.tests_classes), 1)
        self.assertEqual(len(self.registry.get_tests_for_stage(Stage.provision)), 2)


    def test_decorator_multiple_classes(self):
        self.assertEqual(len(self.registry.tests_classes), 0)

        @register(stage=Stage.provision, class_name=['foo', 'bar'], registry=self.registry)
        async def test_foo(context):  # pylint: disable=unused-argument, unused-variable
            pass

        self.assertEqual(len(self.registry.tests_classes), 2)
        self.assertEqual(len(self.registry.get_tests_for_stage(Stage.provision)), 0)





class TestTestExecution(unittest.IsolatedAsyncioTestCase):
    async def test_successfull_test(self):
        async def func(ctx): # pylint: disable=unused-argument
            pass

        result = await run_test(EDDTest(None, func, 'foo'), EDDTestContext(output=['foo']))
        self.assertEqual(result.state, TestState.OK)
        self.assertIn('foo', result.output )

        result = await run_test(EDDTest(None, func, 'foo', tbc=True), EDDTestContext(output=['foo']))
        self.assertEqual(result.state, TestState.TBC)
        self.assertIn('foo', result.output )

    async def test_failing_test(self):
        async def func(ctx): # pylint: disable=unused-argument
            assert False

        result = await run_test(EDDTest(None, func, 'spam'), EDDTestContext())
        self.assertEqual(result.state, TestState.FAIL)

        result = await run_test(EDDTest(None, func, 'eggs', tbc=True), EDDTestContext())
        self.assertEqual(result.state, TestState.FAIL)


    async def test_exception_in_test(self):
        async def func(ctx): # pylint: disable=unused-argument
            raise RuntimeError

        result = await run_test(EDDTest(None, func, 'ham'), EDDTestContext())
        self.assertEqual(result.state, TestState.ERROR)

        result = await run_test(EDDTest(None, func, 'bacon', tbc=True), EDDTestContext())
        self.assertEqual(result.state, TestState.ERROR)




class TestTestRunner(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.mc_thread = server_thread.launch_server_in_thread(mock_katcp_server.MockEDDMasterController)
        #self.pipe_thread = server_thread.launch_server_in_thread(mock_katcp_server.MockEDDMasterController)

    def tearDown(self):
        self.mc_thread.stop().result()
        self.mc_thread.join(timeout=5)
        #self.pipe_thread.stop()

    async def test_mock_server_setup(self):
        """
        Confirm that the mock setup for this test is valid
        """
        con = await aiokatcp.Client.connect('localhost', self.mc_thread.server.port)
        await con.request("provision", "foo")
        self.assertEqual(self.mc_thread.server.sensors['provision'].value, 'foo.yml')



    async def test_stage_calls(self):
        """
        On run, the test runner should call all stages to the master controller.
        """

        parameters = {Stage.provision: 'dummy'}

        await run_tests(output=unittest.mock.Mock(),
                inventory_data={'mastercontroller': {'ip': 'localhost', 'port': f'{self.mc_thread.server.port}'}, 'status_server': {'ip': 'localhost', 'port': f'{self.mc_thread.server.port}'}},
                stage_parameters=parameters)

        self.mc_thread.server.mock.provision.assert_called_once()
        self.mc_thread.server.mock.configure.assert_called_once()
        self.mc_thread.server.mock.capture_start.assert_called_once()
        self.mc_thread.server.mock.measurement_prepare.assert_called_once()
        self.mc_thread.server.mock.measurement_start.assert_called_once()
        self.mc_thread.server.mock.measurement_stop.assert_called_once()
#        self.mc_thread.server.mock.capture_stop.assert_called_once()
        self.mc_thread.server.mock.deconfigure.assert_called_once()
        self.assertEqual(self.mc_thread.server.mock.deprovision.call_count, 2)


    async def test_pickle_summary(self):
        """
        summary should be pickleable
        """
        parameters = {Stage.provision: 'dummy'}

        rs = await run_tests(output=unittest.mock.Mock(),
                inventory_data={'mastercontroller': {'ip': 'localhost', 'port': f'{self.mc_thread.server.port}'}, 'status_server': {'ip': 'localhost', 'port': f'{self.mc_thread.server.port}'}},
                stage_parameters=parameters)

        with tempfile.TemporaryFile() as f:
            pickle.dump(rs, f)
            self.assertGreater(f.tell(), 0)



class TestTerminalOutput(unittest.TestCase):

    def setUp(self):
        self.output = TerminalOutput()
        self.term = io.StringIO()

    def test_print_test_result(self):
        test = EDDTestResult('foo', 'foo')
        with contextlib.redirect_stdout(self.term):
            self.output.print_test_result(test)
        self.assertIn("foo", self.term.getvalue())

        test = EDDTestResult('bar', 'bar', state=TestState.OK)
        with contextlib.redirect_stdout(self.term):
            self.output.print_test_result(test)
        self.assertIn("bar", self.term.getvalue())


    def test_print_test_stage(self):
        test = StageSummary(Stage.provision, n_tests=5)
        with contextlib.redirect_stdout(self.term):
            self.output.print_stage(test)
        self.assertIn('provision', self.term.getvalue())


class TestHTMLOutput(unittest.TestCase):
    def setUp(self):
        self.output = HTMLOutput()

    def test_print_head(self):

        stage_parameters = {Stage.provision: 'test', Stage.configure: '{"foo": "bar"}'}
        inventory_data = {'mastercontroller': {'ip': '123', 'port': '123'}, 'version_tag': 'TEST', 'edd_inventory_folder': f'/dev/null'}

        run_summary = {'inventory_data': inventory_data, 'stage_parameters': stage_parameters, 'duration': 123.4,
                'stage_results': [], 'configuration_graph': "<svg> </svg>", 'pipeline_versions': {}}
        self.output.print_head(run_summary)

        html = self.output.to_string()
        self.assertIn('test', html)

#    def test_print_test_result(self):
#        test = EDDTestResult('foo')
#        f = self.output.print_test_result(test)
#
#        self.assertIn("foo", self.term.getvalue())
#
#        test = EDDTestResult('bar', success=True)
#        with contextlib.redirect_stdout(self.term):
#            self.output.print_test_result(test)
#        self.assertIn("bar", self.term.getvalue())


    def test_print_test_stage(self):
        test = StageSummary(Stage.provision, n_tests=5)

        test_result = EDDTestResult('foo_with_image', 'foo', duration=234567, state=TestState.OK)
        test_result.output.append(BLANK_IMAGE)
        test.add(test_result)

        test_result = EDDTestResult('bar_with_error', 'foopipeline', duration=234567)
        test_result.output.append('This is an error message')
        test.add(test_result)

        self.output.print_stage(test)

        html = self.output.to_string()
        self.assertIn('bar_with_error', html)
        self.assertIn('foopipeline', html)
        self.assertIn('234567', html)



class TestMultiOutput(unittest.TestCase):
    def setUp(self):
        self.output = MultiOutput(unittest.mock.Mock(), unittest.mock.Mock())
    def test_print_test_result(self):
        test = EDDTestResult('foo', 'bar')
        self.output.print_test_result(test)
        for mock in self.output.outputs:
            mock.print_test_result.assert_called()

    def test_print_test_stage(self):
        test = StageSummary(Stage.provision, n_tests=5)
        self.output.print_stage(test)
        for mock in self.output.outputs:
            mock.print_stage.assert_called()



if __name__ == "__main__":
    unittest.main()



