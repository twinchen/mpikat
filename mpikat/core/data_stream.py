from abc import ABC
from typing import List

import spead2
import numpy as np

import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.core.data_stream")

def convert48_64(arr: list) -> int:
    """
    Converts 48 bit to 64 bit integers.

    Args:
        arr (list):  array of 6 bytes.

    Returns:
        64bit integer representation of the 48 bit list
    """
    assert len(arr) == 6
    npd = np.array(arr, dtype=np.uint64)  # pylint: disable=no-member
    # We need to cast the result to python int, as numpy default behavior is 
    # uint64 + int -> float64, see: https://github.com/numpy/numpy/issues/7126
    # The 48 bit can, however, be safely  stored in a python int.

    return int(np.sum(256**np.arange(0, 6, dtype=np.uint64)[::-1] * npd))  # pylint: disable=no-member

def convert64_48(num: int) -> np.ndarray:
    """
    Converts 64 bit number to 48 bit number

    Args:
        num (int): 64-bit number

    Returns:
        array of lower 6 bytes
    """
    return np.frombuffer(num.to_bytes(6, byteorder='big'), dtype=np.uint8)


def edd_spead(addr, name, description="", shape=(6,), dtype='>u1') -> spead2.Item:
    """
    Construct an spead2 item with defaults for EDD packetizer.
    """
    return spead2.Item(addr, name, description=description, shape=shape, dtype=dtype)


class DataStream(ABC):
    """
    Interface for data streams.

    Attributes:
        version:       Version of the format specification
        data_items:    List of data items of the spead2 format stream
        stream_meta:   Dict of meta information for the stream
    """
    version: float = 1
    data_items: List[spead2.Item] = []
    stream_meta: dict = {}

    @classmethod
    def get_data_item(cls, **kwargs) -> spead2.Item:
        """
        Get a data item by id, name or description via .get_data_item(id=1234) or .get_data_item(name='foo')
        """
        if len(kwargs) != 1:
            raise RuntimeError('Expect exactly on property to search for')

        k, v = list(kwargs.items())[0]
        for d in cls.data_items:
            if getattr(d, k) == v:
                return d
        raise ValueError(f'No item with {k} = {v}')

    @classmethod
    def ig_update(cls, items, ig) -> bool: # pylint: disable=unused-argument
        """
        Update the item group depending on the received items. Useful to
        dynamically modify the group based on the first received item.

        Returns:
            True if the item-group was modified, False otherwise.
        """
        return False

    @classmethod
    def add_item2group(cls, item: spead2.Item, item_group: spead2.ItemGroup):
        """
        Add a item to the item-group.
        """
        item_group.add_item(item.id, item.name, item.description, item.shape, item.dtype)

    @classmethod
    def create_item_group(cls) -> spead2.ItemGroup:
        """
        Create a spead2 item group for the format.
        """
        ig = spead2.ItemGroup()

        for d in cls.data_items:
            if d.shape:
                cls.add_item2group(d, ig)
        return ig

    @classmethod
    def validate_items(cls, items: spead2.ItemGroup) -> bool:
        """
        Validate that the received spead2 items match the item group.

        Returns False if heap is invalid.
        """
        _log.trace("Checking missing keys.")
        if len(items.keys()) != len(cls.data_items):
            missing_keys = []
            for i in cls.data_items:
                if i.name not in items:
                    missing_keys.append(i.name)
            _log.warning("Received invalid heap, containing only %i/%i keys. Missign keys: %s.",
                         len(items), len(cls.data_items), ",".join(missing_keys))
            return False
        return True


class DataStreamRegistry:
    """
    Registry to manage data streams.
    """
    _data_streams = {}
    _stream_classes = {}

    @classmethod
    def register(cls, inp):
        """
        Register data-stream-format by meta dict or class.
        """
        if isinstance(inp, dict):
            format_name = inp['type']
            stream_meta = inp
        elif isinstance(inp, type) and issubclass(inp, DataStream):
            format_name = inp.stream_meta['type']
            stream_meta = inp.stream_meta
            cls._stream_classes[format_name] = inp
        else:
            raise RuntimeError(f'Cannot register format from type {type(inp)}, value: {inp}')

        _log.debug('Registering format %s', format_name)
        cls._data_streams[format_name] = stream_meta

    @classmethod
    def get(cls, format_name):
        """
        Return data stream with given name.
        """
        _log.debug('Retrieveing format %s', format_name)
        return cls._data_streams[format_name]

    @classmethod
    def get_class(cls, format_name):
        """
        Return data stream with given name.
        """
        _log.debug('Retrieveing DataStream class %s', format_name)
        return cls._stream_classes[format_name]

    @classmethod
    def clear(cls):
        """
        Clear the registry.
        """
        cls._data_streams.clear()
