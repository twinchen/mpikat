import unittest
import time
import shutil
import os
from dataclasses import asdict
import numpy as np

from mpikat.core.data_stream import DataStream
from mpikat.utils.core_manager import CoreManager
from mpikat.utils import dada_tools as dada
import mpikat.utils.mk_tools as mk
from mpikat.utils.data_streams import PacketizerStreamFormat
from mpikat.pipelines.gated_spectrometer.format import GatedSpectrometerDataStreamFormat

def dummy_function(param1: int, param2: str, param3: float=0):
    return param1, param2, param3

LOCALHOST = "127.0.0.1"
MULTICAST = "239.0.0.0+3"
BASE_PATH = os.path.abspath("mktesting/")
I_FILE = os.path.join(BASE_PATH, "ifile.dat")
O_PATH = os.path.join(BASE_PATH, "output/")
N_READ = 14

BASE_CONF = {
    "nic":LOCALHOST,
    "ip":MULTICAST,
    "heap_size": 4096,
    "port":8125,
    "sample_rate":4096
}

class Test_Mkutils(unittest.TestCase):

    def setUp(self) -> None:
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_valid_index_list_range(self):
        res = mk.get_index_list(range(0,166,2))
        self.assertEqual(res, "0:166:2")

    def test_valid_index_list_string(self):
        res = mk.get_index_list("0,1,2")
        self.assertEqual(res, "0,1,2")
        res = mk.get_index_list("0:2:1")
        self.assertEqual(res, "0:2:1")

    def test_invalid_index_list_string(self):
        self.assertEqual(mk.get_index_list("2"), None)

    def test_valid_index_mask_int(self):
        res = mk.get_index_mask(15)
        self.assertEqual(res, "0xf")
        res = mk.get_index_mask(1)
        self.assertEqual(res, "0x1")

    def test_valid_index_mask_string(self):
        res = mk.get_index_mask("0x000ffff")
        self.assertEqual(res, "0x000ffff")

    def test_invalid_index_mask_string(self):
        self.assertEqual(mk.get_index_mask("dummy"), None)


class Test_MkrecvUtils(unittest.TestCase):

    def setUp(self) -> None:
        self.sformat = GatedSpectrometerDataStreamFormat
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_create_mkrecv_items(self):
        """Should test the creation of MkItems"""
        mkitems = mk.create_mkrecv_items(self.sformat.stream_meta, self.sformat.data_items)
        self.assertEqual(len(mkitems), len(mkitems))
        spead_item_names = [item.name for item in self.sformat.data_items]
        mkrec_item_names = [item.name for item in mkitems]
        self.assertEqual(spead_item_names, mkrec_item_names)

    def test_write_read_header(self):
        """Should test that the DADA header file contains all default key values"""
        fname = mk.write_recv_header(mk.MkHeader(), ignore_unset=False)
        res_d = mk.read_header(fname)
        for key, value in asdict(mk.MkHeader()).items():
            self.assertEqual(res_d[key], str(value))

    def test_write_read_header_with_items(self):
        """Should test that the DADA header file contains all default key values"""
        items = [mk.MkItem(f"IDX{i}", i, 0x1600+i) for i in range(5)]
        fname = mk.write_recv_header(mk.MkHeader(), items, ignore_unset=False)
        res_d = mk.read_header(fname)
        for key, value in asdict(mk.MkHeader()).items():
            self.assertEqual(res_d[key], str(value))

    def test_auto_tune_cores(self):
        ip_list = ['225.0.0.1', '225.0.0.2', '226.0.0.0', '226.0.0.1', '226.0.0.2', '226.0.0.3']
        # 7 cores -> 1 mst: 1 stream per thread, 1 auxillary thread (1st core)
        mst, ptc, atc = mk.auto_tune_cores(ip_list, list(range(10, 17, 1)))
        self.assertEqual(mst, 1)
        self.assertEqual(ptc, -1)
        self.assertEqual(atc, 10)
        # 8 cores -> 1 mst: 1 stream per thread, 1 auxillary thread (1st core), 1 switching thread (2nd core)
        mst, ptc, atc = mk.auto_tune_cores(ip_list, list(range(10, 18, 1)))
        self.assertEqual(mst, 1)
        self.assertEqual(ptc, 11)
        self.assertEqual(atc, 10)
        # 4 cores -> 2 mst: 3 streams per thread, 1 auxillary thread (1st core)
        mst, ptc, atc = mk.auto_tune_cores(ip_list, list(range(11, 15, 1)))
        self.assertEqual(mst, 2)
        self.assertEqual(ptc, -1)
        self.assertEqual(atc, 11)
        # 3 cores -> 3 mst: 2 streams per thread
        mst, ptc, atc = mk.auto_tune_cores(ip_list, list(range(11, 14, 1)))
        self.assertEqual(mst, 2)
        self.assertEqual(ptc, -1)
        self.assertEqual(atc, -1)
        mst, ptc, atc = mk.auto_tune_cores(ip_list, list(range(0, 1)))
        self.assertEqual(mst, 6)
        self.assertEqual(ptc, -1)
        self.assertEqual(atc, -1)

class Test_Mkrecv(unittest.TestCase):

    def setUp(self) -> None:
        self.setUpBase(PacketizerStreamFormat)
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def setUpBase(self, sformat: DataStream):
        self.sformat = sformat

    def test_construction_unmodified_streams(self):

        streams = []
        for __ in range(10):
            streams.append(dict(self.sformat.stream_meta))
        mkrecv = mk.Mkrecv(streams, "dadc")
        self.assertEqual(len(mkrecv.formats), 1)
        self.assertEqual(len(mkrecv.ip_list), 0)
        self.assertEqual(len(mkrecv.items), len(self.sformat.data_items))

    def test_construction_modified_streams(self):

        streams = []
        item_id = np.random.randint(0, len(self.sformat.data_items))
        for i in range(10):
            item = self.sformat.data_items[item_id]
            stream = dict(self.sformat.stream_meta)
            stream["ip"] = f"225.0.0.{i}"
            stream[item.name] = i
            streams.append(stream)
        mkrecv = mk.Mkrecv(streams, "dadc")
        mkitem = mkrecv.get_item(item.name)
        self.assertEqual(len(mkrecv.formats), 1)
        self.assertEqual(len(mkrecv.ip_list), 10)
        self.assertEqual(len(mkrecv.items), len(self.sformat.data_items))
        self.assertEqual(mkitem.list, ",".join(map(str, range(10))))


class Test_MkrecvAsync(unittest.IsolatedAsyncioTestCase):

    def setUp(self) -> None:
        self.setUpBase(PacketizerStreamFormat)
        return super().setUp()

    async def asyncSetUp(self) -> None:
        self.buffer = dada.DadaBuffer(4096*16)
        await self.buffer.create()
        return await super().asyncSetUp()

    async def asyncTearDown(self) -> None:
        await self.buffer.destroy()
        return await super().asyncTearDown()

    def tearDown(self) -> None:
        return super().tearDown()

    def setUpBase(self, sformat: DataStream):
        self.sformat = sformat

    def test_start_insufficient_conf(self):
        mkrecv = mk.Mkrecv(self.sformat.stream_meta, self.buffer.key)
        mkrecv.start()
        time.sleep(2)
        self.assertFalse(mkrecv.is_alive())

    def test_start_sufficient_conf(self):
        item = self.sformat.data_items[np.random.randint(0, len(self.sformat.data_items))]
        stream = dict(self.sformat.stream_meta)
        stream.update(BASE_CONF)
        mkrecv = mk.Mkrecv(stream, self.buffer.key)
        mkrecv.set_item(item.name, "index", 1)
        mkrecv.set_item(item.name, "step", 4096)
        mkrecv.start()
        self.assertTrue(mkrecv.is_alive())
        mkrecv.stop()
        self.assertFalse(mkrecv.is_alive())


class Test_MkrecvPacketizerStreamFormat(Test_Mkrecv):

    def setUp(self):
        self.setUpBase(PacketizerStreamFormat)

class Test_MkrecvGatedSpectrometerDataStreamFormat(Test_Mkrecv):

    def setUp(self):
        self.setUpBase(GatedSpectrometerDataStreamFormat)




class Test_Mksend_Mkrecv(unittest.IsolatedAsyncioTestCase):

    def setUp(self) -> None:
        if os.path.exists(BASE_PATH):
            shutil.rmtree(BASE_PATH)
        os.makedirs(O_PATH)
        self.stream = PacketizerStreamFormat()
        self.size = self.stream.samples_per_heap * 8
        self.data_rate = 4096 * 8 * N_READ
        np.random.randint(-64,64, size=self.size).astype(np.int8).tofile(I_FILE)
        self.send_buffer = dada.DadaBuffer(self.size)
        self.recv_buffer = dada.DadaBuffer(self.size)
        self.disk_reader = dada.DiskDb(self.send_buffer.key, I_FILE, n_reads=N_READ, data_rate=self.data_rate)
        self.disk_writer = dada.DbDisk(self.recv_buffer.key, O_PATH)
        self.core_manage = CoreManager()

    async def asyncSetUp(self):
        await self.send_buffer.create()
        await self.recv_buffer.create()

    async def asyncTearDown(self) -> None:
        if self.disk_writer.is_alive():
            self.disk_writer.stop()
        if self.disk_reader.is_alive():
            self.disk_reader.stop()
        await self.send_buffer.destroy()
        await self.recv_buffer.destroy()

    def tearDown(self) -> None:
        shutil.rmtree(BASE_PATH)

    def test_send_receive(self):
        stream = dict(PacketizerStreamFormat().stream_meta)
        stream["nic"] = LOCALHOST
        stream["ip"] = MULTICAST
        stream["sample_rate"] = self.data_rate
        stream["file_size"] = self.size
        stream["port"] = 7149
        stream["sync_time"] = int(time.time())
        stream["heap_size"] = 4096
        # stream.stream_meta["ngroups_data"] = 2
        mkrecv = mk.Mkrecv(stream, self.recv_buffer.key, ibv=False)
        mksend = mk.Mksend(stream, self.send_buffer.key, ibv=False)
        mkrecv.set_item("timestep", "index", 1)
        mkrecv.set_item("timestep", "step", 4096)
        mkrecv.set_item("timestep", "modulo", self.size)
        mkrecv.set_item("polarization", "index", 2)
        mkrecv.set_item("polarization", "list", "0,1")
        mksend.set_item("timestep", "index", 1)
        mksend.set_item("timestep", "step", 4096)
        mksend.set_item("polarization", "index", 2)
        mksend.set_item("polarization", "list", "0,1")
        self.disk_writer.start()
        mkrecv.start()
        mksend.start()
        self.disk_reader.start()
        self.disk_reader.wait() # Wait until all data was read from file
        mksend.stop()
        mkrecv.stop()
        expected = np.fromfile(I_FILE, dtype=np.int8)
        outfiles = os.listdir(O_PATH)
        d = mk.read_header(mkrecv.header_file)
        self.assertEqual(len(outfiles), N_READ - 4)
        valid = 0
        for f in outfiles[:-1]:
            actual = np.fromfile(os.path.join(O_PATH, f), offset=4096, dtype=np.int8)
            valid += np.all(expected == actual)
        self.assertTrue(valid > 7) # Might happen that heaps are lost accpet 7 of 10 correct slots


if __name__ == "__main__":
    unittest.main()
