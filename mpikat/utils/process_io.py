import multiprocessing as mp
import queue
import mpikat.core.logger as logging

_log = logging.getLogger("mpikat.utils.process_io")

class ProcessIO(mp.Process):
    """
    Plotter for spectrum data.
    """
    def __init__(self, callback: callable, iqueue_size=1024, oqueue_size=1024, timeout=1):
        """
        """
        super().__init__()
        self._manager = mp.Manager()
        self._iqueue: mp.Queue = self._manager.Queue()
        self._oqueue: mp.Queue = self._manager.Queue()
        self._quit = mp.Event()
        self._callback = callback
        self._iqueue_size: int = iqueue_size
        self._oqueue_size: int = oqueue_size
        self._timeout: int = timeout
        self._exc: str = ""

    @property
    def iqueue(self) -> mp.Queue:
        return self._iqueue
    @property
    def oqueue(self) -> mp.Queue:
        return self._oqueue
    @property
    def isize(self) -> int:
        return self._iqueue.qsize()
    @property
    def osize(self) -> int:
        return self._oqueue.qsize()

    def add(self, item: any):
        """
        Add a new heap to the queue
        """
        if not self.is_alive():
            _log.warning("Process not running, can not add items"); return
        self._iqueue.put(item)

    def get(self, block: bool=True, timeout: int=None) -> any:
        return self._oqueue.get(block, timeout)

    def stop(self, timeout: float=None):
        """
        Stop plotting
        """
        if self.is_alive():
            self._quit.set()
            self.join(timeout=timeout)
            self._quit.clear()

    def run(self):
        while not self._quit.is_set():
            # In case the queue is full, it will be flushed.
            if self.isize >= self._iqueue_size:
                _log.warning("Process can not keep up, flushing queue.. (queue size %i)", self.isize)
                while True: # not self._iqueue.empty()   ## According to documentation empty is not reliable, https://docs.python.org/3/library/multiprocessing.html
                    try:
                        self._iqueue.get(False)
                    except queue.Empty:
                        _log.debug("Queue flushed")
                        break
            try:
                item = self._iqueue.get(block=True, timeout=self._timeout)
            except queue.Empty:
                _log.debug("Timed out when pulling from queue");continue
            # Probably a timeout of the callback function makes sense here
            try:
                res = self._callback(item)
            except Exception as e:
                _log.error("Callable raised exception %s", str(e))
                raise Exception(f"Callable raised exception {str(e)}")

            if self.osize >= self._oqueue_size:
                _log.warning("Outputqueue is full, won't add item");continue
            if res is not None:
                self._oqueue.put(res)
        _log.info("ProcessIO stopped")
