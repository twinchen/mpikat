import unittest
import socket
import time
import shutil
import os
from queue import Queue
import numpy as np
import tempfile

from mpikat.utils import get_port
from mpikat.utils import fits


def create_packet():
    """Create  a test packet"""
    test_packet = fits.FitsPacket()
    test_packet.payload = fits.FitsPayload().fromarray([np.zeros(5, dtype='float32'), np.zeros(6, dtype='float32')])
    test_packet.header.timestamp = fits.getFitsTime(time.time() + 100)
    return test_packet


class TestFitsSection(unittest.TestCase):
    def test_construction_with_data(self):
        f = fits.FitsSection(1, data=np.ones(123))
        self.assertEqual(f.nchannels, 123)
        self.assertEqual(f.data[3], 1)

    def test_construction_with_nchannels(self):
        f = fits.FitsSection(1, nchannels=23)
        self.assertEqual(f.nchannels, 23)
        self.assertEqual(f.data.size, 23)

    def test_construction_with_both(self):
        with self.assertRaises(AssertionError):
            f = fits.FitsSection(1, nchannels=23, data=np.zeros(5))
        f = fits.FitsSection(1, nchannels=23, data=np.zeros(23))


class TestFitsPayload(unittest.TestCase):
    def test_from_list_of_arrays(self):
        pl = fits.FitsPayload().fromarray([np.zeros(5), np.zeros(6)])

        self.assertEqual(len(pl.sections), 2)
        self.assertEqual(pl.sections[0].nchannels, 5)
        self.assertEqual(pl.sections[1].nchannels, 6)

    def test_from_array(self):
        pl = fits.FitsPayload().fromarray(np.zeros([2, 8]))

        self.assertEqual(len(pl.sections), 2)
        self.assertEqual(pl.sections[0].nchannels, 8)
        self.assertEqual(pl.sections[1].nchannels, 8)


    def test_get_section_by_id(self):
        pl = fits.FitsPayload().fromarray([np.ones(3), np.ones(3) * 2, np.ones(3) * 3])
        self.assertEqual(pl.section(1).data[1], 1)
        self.assertEqual(pl.section(2).data[1], 2)
        self.assertEqual(pl.section(3).data[1], 3)


class Test_FitsPacket(unittest.TestCase):
    def setUp(self) -> None:
        self.tobj = fits.FitsPacket()
        return super().setUp()

    def test_set_payload(self):
        """Should update header""" 

        pl = fits.FitsPayload().fromarray([np.zeros(5, dtype='float32'), np.zeros(6, dtype='float32')])
        self.tobj.payload = pl

        self.assertEqual(self.tobj.header.nsections, 2)
        # 11 bytes of data + 2 (sections) * 2 (variables) * 4 bytes
        self.assertEqual(self.tobj.payload_size, 11*4 + 2 * 2 * 4)

    def test_set_payload_wrong_size(self):
        """Should raise a ValueError when passed bytes-object does not match expected size
        """
        self.tobj.setHeader(bytes(fits.create_header()))
        with self.assertRaises(ValueError):
            self.tobj.setPayload(b'cheese')

    def test_set_header_wrong_size(self):
        """Should raise a ValueError when passed bytes-object does not match expected size
        """
        with self.assertRaises(ValueError):
            self.tobj.setHeader(b'cheese')

    def test_from_bytes(self):
        """Should test if a packet can be created with default config"""
        test_packet = create_packet() 
        self.tobj.frombytes(bytes(test_packet))
        self.assertEqual(bytes(self.tobj), bytes(test_packet))

    def test_from_bytes_with_config(self):
        """Should test if a packet can be created with a different config
        """
        test_packet = create_packet() 
        self.tobj.frombytes(bytes(test_packet))
        self.assertEqual(bytes(self.tobj), bytes(test_packet))



class Test_FitsSendReceiver(unittest.TestCase):

    def setUp(self) -> None:
        self._ADDRESS_ = ("localhost", get_port())
        self._DIRECTORY_ = tempfile.TemporaryDirectory()
        self.receiver = fits.Receiver(self._ADDRESS_, self._DIRECTORY_.name, Queue())
        self.sender = fits.Sender(self._ADDRESS_[0], self._ADDRESS_[1], delay=0)
        self.sender.start()

    def tearDown(self) -> None:
        if self.receiver.is_alive():
            self.receiver.stop()
            self.receiver.join()
        if self.sender.is_alive():
            self.sender.stop()
            self.sender.join()
        self._DIRECTORY_.cleanup()

    def test_recevier_connect_no_sender(self):
        """Should test a failure on connect if no endpoint is available
        """
        self.sender.stop()
        self.sender.join()
        with self.assertRaises(socket.error):
            self.receiver.connect()

    def test_receiver_connect_disconnect(self):
        """Should test if Receiver can succesfully connect & disconnect
        """
        self.receiver.connect()
        self.sender.wait_connected()
        self.assertTrue(self.receiver.is_connected())
        self.assertTrue(self.sender.is_connected())
        self.receiver.disconnect()
        self.assertFalse(self.receiver.is_connected())
        #self.assertFalse(self.sender.is_connected()) -> Won't register the disconnected receiver

    def test_start_stop(self):
        """Should test if Receiver starts and stops correctly
        """
        self.receiver.wait_start()
        self.assertTrue(self.receiver.is_alive())
        self.receiver.stop()
        self.receiver.join()
        self.assertFalse(self.receiver.is_alive())

    def test_send_recv_packet(self):
        """Should test if the Receiver receives a sent FitsPacket
        """
        self.receiver.wait_start()
        self.sender.is_measuring.set()
        self.sender.wait_connected()
        for __ in range(5):
            test_packet = create_packet()
            self.sender.put(test_packet)
            recv_packet = self.receiver.queue.get(timeout=1)
            self.assertEqual(str(test_packet), str(recv_packet))
            self.assertEqual(bytes(test_packet), bytes(recv_packet))

    def test_send_delay(self):
        """Should test if the delay on sender side is correctly applied
        """
        self.sender.stop()
        self.sender.join()
        self.sender = fits.Sender(self._ADDRESS_[0], self._ADDRESS_[1], delay=4)
        self.sender.start()
        self.sender.is_measuring.set()
        self.receiver.wait_start()
        self.sender.wait_connected()
        test_packet = create_packet()
        start = time.time()
        self.sender.put(test_packet)
        recv_packet = self.receiver.queue.get()
        self.assertGreaterEqual(time.time()-start, 4)
        self.assertEqual(bytes(test_packet), bytes(recv_packet))

    def test_file_write_receiver(self):
        self.receiver.wait_start()
        self.sender.is_measuring.set()
        self.sender.wait_connected()
        test_packet = create_packet()
        self.sender.put(test_packet)
        self.receiver.queue.get() # queue.get() blocks until the packet is received
        fname = f"FWP_{(test_packet.header.timestamp).decode('utf-8').replace(' ','')}.dat"
        with open(os.path.join(self._DIRECTORY_.name,  fname), "rb") as fid:
            data = fid.read()
        self.assertEqual(bytes(test_packet), data)

if __name__ == "__main__":
    unittest.main()
