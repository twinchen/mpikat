# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring

import unittest
import time

import numpy as np

from mpikat.core.data_stream import convert64_48
from mpikat.pipelines.gated_spectrometer import GatedSpectrometerDataStreamFormat
from mpikat.pipelines.gated_spectrometer import GatedSpectrometerSpeadHandler

class TestGatedSpectrometerSpeadhandler(unittest.TestCase):
    def setUp(self):
        self.handler = GatedSpectrometerSpeadHandler()

    def test_initialize(self):
        items = {d.name: d for d in GatedSpectrometerDataStreamFormat.data_items}

        D = items.pop('data')

        # initialize installes a timestamp handler based on given sync time and
        # sampling rate if data is not in items (i.e. on first valid item)
        items['sync_time'].value = convert64_48(123)
        items['sampling_rate'].value = convert64_48(4_000_000_000)
        items['fft_length'].value = convert64_48(4_096)

        self.assertIsNone(self.handler.timestamp_handler)
        self.assertTrue(self.handler.initialize(items))
        self.assertIsNotNone(self.handler.timestamp_handler)

        items['data'] = D
        self.assertFalse(self.handler.initialize(items))

    def test_build_packet(self):
        items = {d.name: d for d in GatedSpectrometerDataStreamFormat.data_items}
        D = items.pop('data')

        items['sync_time'].value = convert64_48(int(time.time()))
        items['sampling_rate'].value = convert64_48(4_000_000_000)
        items['fft_length'].value = convert64_48(4_096)
        items['naccumulate'].value = convert64_48(2_048)

        items["number_of_input_samples"].value  = convert64_48(45)
        items["timestamp_count"].value = convert64_48(123)
        items["polarization"].value = convert64_48(1)
        items["noise_diode_status"].value = convert64_48(1)
        items["number_of_input_samples"].value = convert64_48(42)
        items["number_of_saturated_samples"].value = convert64_48(23)
        items["type"] .value= convert64_48(0)

        D.value = np.zeros(4_096)
        self.handler.initialize(items)
        items['data'] = D

        packet = self.handler.build_packet(items)



if __name__ == '__main__':
    unittest.main()
