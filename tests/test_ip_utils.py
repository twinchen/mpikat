# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring

import unittest
import ipaddress

from mpikat.utils import ip_utils


class Test_split_ip_string(unittest.TestCase):
    def test_invalid_ip(self):
        with self.assertRaises(Exception):
            ip_utils.split_ipstring("foo")
        with self.assertRaises(Exception):
            ip_utils.split_ipstring("123.456.789.123")

    def test_invalid_port(self):
        with self.assertRaises(ValueError):
            ip_utils.split_ipstring("123.0.0.1:foo")
        with self.assertRaises(ValueError):
            ip_utils.split_ipstring("123.0.0.1+4:foo")

    def test_invalid_range(self):
        with self.assertRaises(Exception):
            ip_utils.split_ipstring("123.0.0.1+foo")
        with self.assertRaises(Exception):
            ip_utils.split_ipstring("123.0.0.1+foo:1234")

    def test_split(self):
        ip = "123.0.0.1"
        port = 42
        N = 8
        ips = "{}+{}:{}".format(ip, N-1, port)
        a, b, c = ip_utils.split_ipstring(ips)
        self.assertEqual(ip, a)
        self.assertEqual(N, b)
        self.assertEqual(port, c)


class Test_multicast_ranges(unittest.TestCase):
    def test_multicast(self):
        self.assertTrue(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("225.0.0.0")))
        self.assertFalse(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("127.0.0.1")))

    def test_range(self):
        self.assertTrue(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("225.0.0.0+1")))
        self.assertTrue(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("225.0.0.0+7")))
        self.assertTrue(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("225.0.0.0+15")))
        self.assertFalse(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("225.0.0.0+2")))
        self.assertFalse(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("225.0.0.0+6")))

    def test_block_start(self):
        self.assertTrue(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("225.0.0.0+1")))
        self.assertTrue(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("225.0.0.8+7")))
        self.assertTrue(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("225.0.0.4+3")))
        self.assertFalse(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("225.0.0.4+7")))
        self.assertFalse(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring("225.0.0.7+15")))


class Test_ipstring_from_list(unittest.TestCase):
    def test_range(self):
        s = ip_utils.ipstring_from_list(["225.0.0.1", "225.0.0.2", "225.0.0.3"])
        self.assertEqual(s, "225.0.0.1+2")

    def test_noncont_range(self):
        with self.assertRaises(Exception):
            ip_utils.ipstring_from_list(["225.0.0.1", "225.0.0.3"])


class Test_ipstring_to_list(unittest.TestCase):
    def test_range(self):
        s = ip_utils.ipstring_to_list("225.0.0.1+5")
        self.assertEqual(len(s), 6)
        self.assertEqual(s[0], "225.0.0.1")
        self.assertEqual(s[1], "225.0.0.2")
        self.assertEqual(s[2], "225.0.0.3")
        self.assertEqual(s[3], "225.0.0.4")
        self.assertEqual(s[4], "225.0.0.5")
        self.assertEqual(s[5], "225.0.0.6")

    def test_zero_range(self):
        s = ip_utils.ipstring_to_list("225.0.0.1")
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0], "225.0.0.1")


class Test_mac(unittest.TestCase):
    def test_true_multicast_mac(self):
        self.assertFalse(ip_utils.is_valid_nic_mac('01:00:5e:00:00:98'))

    def test_non_multicast_mac(self):
        self.assertTrue(ip_utils.is_valid_nic_mac('12:4d:01:00:5a:00'))

    def test_non_mac(self):
        self.assertFalse(ip_utils.is_valid_nic_mac('no_a_mac'))


class Test_network_ipstring_conversion(unittest.TestCase):
    def test_network2ipstring(self):
        s = ip_utils.network2ipstring(ipaddress.ip_network('239.11.1.148/32'))
        self.assertNotIn('+', s)

        s = ip_utils.network2ipstring(ipaddress.ip_network('239.11.1.148/31'))
        self.assertIn('+', s)
        a, N = s.split('+')
        self.assertEqual(a, '239.11.1.148')
        self.assertEqual(int(N), 1)

    def test_ipstring2network(self):
        n = ip_utils.ipstring2network('239.11.1.148')
        self.assertEqual(n, ipaddress.ip_network('239.11.1.148/32'))

        n = ip_utils.ipstring2network('239.11.1.148+1')
        self.assertEqual(n, ipaddress.ip_network('239.11.1.148/31'))

        n = ip_utils.ipstring2network('239.11.1.0+8')
        self.assertEqual(n, ipaddress.ip_network('239.11.1.0/29'))

class Test_is_valid_multicast(unittest.TestCase):

    def test_invalid_address_str(self):
        self.assertFalse(ip_utils.is_valid_multicast("I.m.a.bad.address"))

    def test_valid_address_str(self):
        self.assertTrue(ip_utils.is_valid_multicast("225.0.0.1"))

    def test_invalid_address_int(self):
        self.assertFalse(ip_utils.is_valid_multicast(-1))

    def test_valid_address_int(self):
        self.assertTrue(ip_utils.is_valid_multicast(0))

class Test_concatenate_ips(unittest.TestCase):

    def test_empty_list(self):
        self.assertEqual(ip_utils.concatenate_ips([]), [])

    def test_mixed_ips(self):
        ip_list = [['225.0.0.1','225.0.0.2','I.m.a.bad.address'], '226.0.0.0+4', 'I.m.a.worse.address', 10, -1]
        result = ip_utils.concatenate_ips(ip_list)
        expected = ['0.0.0.10', '225.0.0.1', '225.0.0.2', '226.0.0.0', '226.0.0.1', '226.0.0.2', '226.0.0.3', '226.0.0.4']
        self.assertEqual(result, expected)

    def test_list_ips(self):
        ip_list = [['225.0.0.1','225.0.0.2','I.m.a.bad.address']]
        result = ip_utils.concatenate_ips(ip_list)
        expected = ['225.0.0.1', '225.0.0.2']
        self.assertEqual(result, expected)

    def test_string_ips(self):
        ip_list = ['226.0.0.0+4', 'I.m.a.worse.address']
        result = ip_utils.concatenate_ips(ip_list)
        expected = ['226.0.0.0', '226.0.0.1', '226.0.0.2', '226.0.0.3', '226.0.0.4']
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
