import asyncio
from mpikat.core.edd_pipeline_aio import EDDPipeline

class FailPipeline(EDDPipeline):
    """
    A pipeline where every command fails with an exception
    """
    async def set(self, config_json):
        raise RuntimeError

    async def capture_start(self):
        raise RuntimeError

    async def capture_stop(self):
        raise RuntimeError

    async def start(self):
        await super().start()
