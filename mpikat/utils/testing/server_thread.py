from concurrent.futures import ThreadPoolExecutor   # import is needed as otherwise there are random crashes due to a circular import ? However, these only occur when script started repeatedly without delay   pylint: disable=unused-import
import asyncio
import threading

from mpikat.core import logger

_log = logger.getLogger('mpikat.utils.testing.server_thread')


class ServerThread(threading.Thread):
    """Launch a server in a separate thread"""
    def __init__(self, server_type, *args, **kwargs):
        """
        Get type of the server
        """
        self.server_type = server_type
        self.args = args
        self.kwargs = kwargs
        super().__init__()
        self.loop = asyncio.new_event_loop()
        self.server = None
        self.server_created = threading.Event()

    def run(self):
        _log.debug('Starting server of type %s in %s - loop %s', self.server_type, threading.current_thread().name, self.loop)
        asyncio.set_event_loop(self.loop)

        self.server = self.server_type(*self.args, **self.kwargs, loop=self.loop)

        async def wrapper():
            await self.server.start()
            self.server_created.set()
            _log.debug('Running loop forever in %s - %s', threading.current_thread().name, self.loop)
            await self.server.join()
            _log.debug('Server of type %s has stopped in %s', self.server_type, threading.current_thread().name)

        self.loop.run_until_complete(wrapper())
        _log.debug('Wrapper done in loop - %s - closing loop', self.loop)
        self.loop.close()

    def stop(self):
        """
        Stop the server and thereby the thread
        """
        _log.debug('Stop server requested from %s', threading.current_thread().name)
        async def wrapper():
            _log.debug('Stopping server in %s', threading.current_thread().name)
            await self.server.stop()

        return asyncio.run_coroutine_threadsafe(wrapper(), self.loop)


def launch_server_in_thread(server_type, *args, **kwargs):
    """
    Create server and run it in a new thread. Returns running thread.
    """
    t = ServerThread(server_type, *args, **kwargs)
    t.start()
    t.server_created.wait()
    return t
