"""This module is the interface between TNRT TCS and MPIFR USB backend."""
import codecs
import re
import json
from copy import deepcopy

import katcp
from katcp import BlockingClient

from mpikat.core.datastore import EDDDataStore

import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.telescopes.tcs_usb_interface")

ESCAPE_SEQUENCE_RE = re.compile(r'''
    ( \\U........      # 8-digit hex escapes
    | \\u....          # 4-digit hex escapes
    | \\x..            # 2-digit hex escapes
    | \\[0-7]{1,3}     # Octal escapes
    | \\N\{[^}]+\}     # Unicode characters by name
    | \\[\\'"abfnrtv]  # Single-character escapes
    )''', re.UNICODE | re.VERBOSE)


class TcsUsbInterface(BlockingClient):

    def __init__(self, host, port, redis_host, redis_port):
        master_host = host
        master_port = port
        super(TcsUsbInterface, self).__init__(master_host, master_port)
        self.__eddDataStore = EDDDataStore(redis_host, redis_port)
        self.start()

    def __del__(self):
        super(TcsUsbInterface, self).stop()
        super(TcsUsbInterface, self).join()

    def unescape_string(self, s):
        def decode_match(match):
            return codecs.decode(match.group(0), 'unicode-escape')
        return ESCAPE_SEQUENCE_RE.sub(decode_match, s)

    def decode_katcp_message(self, s):
        return self.unescape_string(s).replace("\_", " ")

    def to_stream(self, reply, informs):
        log.info(self.decode_katcp_message(reply.__str__()))
        for msg in informs:
            log.info(self.decode_katcp_message(msg.__str__()))

    def start(self):
        """
        @brief      Start the blocking client
        """
        log.info("Starting TCS-USB blocking client interface")
        self.setDaemon(True)
        super(TcsUsbInterface, self).start()
        self.wait_protocol()

    def stop(self):
        """
        @brief      Stop the blocking client
        """
        super(TcsUsbInterface, self).stop()
        super(TcsUsbInterface, self).join()

    def deconfigure(self):
        """
        @brief      Send deconfigure command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending deconfigure to MASTER CONTROLLER")
        reply, informs = self.blocking_request(
            katcp.Message.request("deconfigure"), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def configure(self, config={}):
        """
        @brief      Send configure command to the server

        Args:
            config (dict): Config python dictionary.

        Returns:
            KATCP message object

        """
        log.info(
            "Sending configure with argument {} to MASTER CONTROLLER".format(config))
        reply, informs = self.blocking_request(katcp.Message.request(
            "configure", json.dumps(config)), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def capture_start(self):
        """
        @brief      Send capture_start command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending capture_start to MASTER CONTROLLER")
        reply, informs = self.blocking_request(
            katcp.Message.request("capture-start"), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def capture_stop(self):
        """
        @brief      Send capture_stop command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending capture_stop to MASTER CONTROLLER")
        reply, informs = self.blocking_request(
            katcp.Message.request("capture-stop"), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def measurement_prepare(self, config={}):
        """
        @brief      Send measurement_prepare command to the server

        Args:
            config (dict): Source specific python dictionary.

        Returns:
            KATCP message object
        """
        log.info("Sending measurement_prepare with argument: {} to MASTER CONTROLLER".format(
            config))
        reply, informs = self.blocking_request(katcp.Message.request(
            "measurement-prepare", json.dumps(config)), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def metadata_update(self, metadata_dict={}):
        """
        @brief      Send metadata information to the server

        Args:
            metadata_dict (dict): Metadata python dictionary in key:value pair format

        Returns:
            KATCP message object
        """
        log.info("Sending metadata python dict: {} to MASTER CONTROLLER".format(
            metadata_dict))
        reply, informs = self.blocking_request(katcp.Message.request(
            "metadata-update", json.dumps(metadata_dict)), timeout=120.0)
        self.to_stream(reply, informs)
        return reply


    def measurement_start(self):
        """
        @brief      Send measurement_start command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending measurement_start to MASTER CONTROLLER")
        reply, informs = self.blocking_request(
            katcp.Message.request("measurement-start"), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def measurement_stop(self):
        """
        @brief      Send measurement_stop command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending measurement_stop to MASTER CONTROLLER")
        reply, informs = self.blocking_request(
            katcp.Message.request("measurement-stop"), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def set(self, config):
        """
        @brief      Send set command to the server

        Args:
            config (dict): Config python dictionary.

        Returns:
            KATCP message object
        """
        log.info("Sending set with arguemnt {} to MASTER CONTROLLER".format(config))
        reply, informs = self.blocking_request(
            katcp.Message.request("set", json.dumps(config)), timeout=120.0)
        self.to_stream(reply, informs)
        return reply

    def provision(self, config):
        """
        @brief      Send provision command to the serveri

        Args:
            config (str): Provision name.

        Returns:
            KATCP message object
        """
        log.info(
            "Sending provision with argument {} to MASTER CONTROLLER".format(config))
        reply, informs = self.blocking_request(
            katcp.Message.request("provision", config), timeout=240.0)
        self.to_stream(reply, informs)
        return reply

    def deprovision(self):
        """
        @brief      Send deprovision command to the server

        Returns:
            KATCP message object
        """
        log.info("Sending deprovision to MASTER CONTROLLER")
        reply, informs = self.blocking_request(
            katcp.Message.request("deprovision"), timeout=240.0)
        self.to_stream(reply, informs)
        return reply
