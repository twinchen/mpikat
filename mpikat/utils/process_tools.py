import time
import os
import asyncio
import shlex
import psutil
from threading import Thread
from subprocess import Popen, PIPE

from mpikat.utils.pipe_monitor import PipeMonitor
from mpikat.core import logger

_log = logger.getLogger('mpikat.utils.process_tools')


class ProcessException(Exception):
    """Raised when the return value of the process is non zero"""
    def __init__(self, *args, stdout="", stderr="", **kwargs, ):
        super().__init__(*args, **kwargs)
        self.stdout = stdout
        self.stderr = stderr


async def command_watcher(cmd, env=None, umask=0o000, allow_fail=False, timeout=120):
    """
    Executes a command and watches the process result. Raises an error on non
    zero returncode (except allow_fail is given), and logs command output to
    debug, respectively eror output.

    Args:

    cmd:    string or list of commandline arguments used to create the subprocess
    env:    Dict of environemnet variables set for the process in addition to the current global env
    umask:  Default permissions for files created byh this subprocess. Defaults to globally read and writeable!
    """
    if env is None:
        env = {}

    _log.info("Executing command: %s", cmd)
    if isinstance(cmd, str):  # pylint: disable=undefined-variable,consider-merging-isinstance
        cmd = shlex.split(cmd)

    environ = os.environ.copy()
    environ.update(env)
    if env:
        _log.debug("Additional environment variabels set: %s", str(env))

    _log.trace("Environment variables for command:\n%s", environ)

    # Awaiting only the subprocess start
    try:
        process = await asyncio.create_subprocess_exec(*cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE, env=environ)
        stdout, stderr = await asyncio.wait_for(process.communicate(), timeout=timeout)
    except FileNotFoundError as E:
        if allow_fail:
            _log.warning("File %s not found, but command allowed to fail", " ".join(cmd))
            return None, None
        raise E
    except (TimeoutError, asyncio.exceptions.TimeoutError) as E:
        if allow_fail:
            _log.warning("Command %s timeoed out after %i s, but command allowed to fail", " ".join(cmd), timeout)
            return None, None
        raise E

    if process.returncode != 0:
        if allow_fail:
            _log.warning("Command %s returned exit code %i, but command allowed to fail", " ".join(cmd), process.returncode)
        else:
            name = ' '.join(cmd)
            error = ProcessException(f"Process returned non-zero returncode: {process.returncode} {name}",
                        stdout=stdout.decode('UTF8'),
                        stderr=stderr.decode('UTF8'))
            _log.error("Process STDOUT dump %s:\n%s", name, error.stdout)
            _log.error("Process STDERR dump %s:\n%s", name, error.stderr)
            raise error
    return stdout.decode('UTF8'), stderr.decode('UTF8')


class ManagedProcess(object):
    """
    Manages a long running process and passes stdout and stderr to handlers.
    """
    def __init__(self, cmdlineargs, env=None, umask=0o000, stdout_handler=None, stderr_handler=None):
        """
        Args:
            cmdlineargs:    string or list of commandline arguments used to create the subprocess
            env:            Dict of environemnet variables set for the process
            umask:          Default permissions for files created by this subprocess. Defaults to
                            globally read and writeable!
            stdout_handler: Handler for ouptut written to stdout
            stderr_handler: Handler for ouptut written to stderr
        """
        # cmdlineargs to list of strings
        _log.debug("Commandlineargs received: %s", cmdlineargs)
        if isinstance(cmdlineargs, str):  # pylint: disable=undefined-variable,consider-merging-isinstance
            cmdlineargs = cmdlineargs.split()
        cmdlineargs = list(map(str, cmdlineargs))
        _log.debug("Commandlineargs processed: %s", cmdlineargs)
        self._cmdl = " ".join(cmdlineargs)


        def preexec_fn():
            """Set umask for subprocess"""
            os.umask(umask)

        environ = os.environ.copy()
        if env:
            environ.update(env)

        self._proc = Popen(cmdlineargs, stdout=PIPE, stderr=PIPE,
                           shell=False, env=environ, close_fds=True, preexec_fn=preexec_fn)
        _log.info("Running command %s as pid %i", self._cmdl, self._proc.pid)
        if stdout_handler:
            self._stdout_handler = stdout_handler
        else:
            self._stdout_handler = self.default_output_handle
        if stderr_handler:
            self._stderr_handler = stderr_handler
        else:
            self._stderr_handler = self.default_output_handle
        self.stdout_monitor = None
        self.stderr_monitor = None
        self.eop_monitor = None
        self._start_monitors()

    @property
    def pid(self):
        """Pid code of the subprocess"""
        return self._proc.pid

    @property
    def returncode(self):
        """Return code of the subprocess"""
        return self._proc.returncode

    def default_output_handle(self, line: str):
        """Default handle for process outputs.

        Args:
            line (str): The line printed by the process
        """
        line = line.strip()
        if "err" in line.lower():
            _log.error(self._cmdl + ":\n   " + line)
        elif "warn" in line.lower():
            _log.warning(self._cmdl + ":\n   " + line)
        elif "info" in line.lower():
            _log.info(self._cmdl + ":\n   " + line)
        else:
            _log.debug(self._cmdl + ":\n   " + line)


    def is_alive(self):
        """Checks if the subprocess is alive."""
        if self._proc is not None:
            return self._proc.poll() is None
        return False

    def _start_monitors(self):
        """Start stout/stedd monitor"""
        self.stdout_monitor = PipeMonitor(
            self._proc.stdout, self._stdout_handler)
        self.stdout_monitor.start()
        self.stderr_monitor = PipeMonitor(
            self._proc.stderr, self._stderr_handler)
        self.stderr_monitor.start()

    def _stop_monitors(self):
        """Stop stout/stedd monitor"""
        if self.stdout_monitor is not None:
            self.stdout_monitor.stop()
            self.stdout_monitor.join(3)
            self.stdout_monitor = None
        if self.stderr_monitor is not None:
            self.stderr_monitor.stop()
            self.stderr_monitor.join(3)
            self.stderr_monitor = None

    def terminate(self, timeout=5):
        """
        Terminate the process.

        Args:
            timeout: Kill proces after timeout.
        """
        start = time.time()
        self._stop_monitors()
        _log.debug("Trying to terminate process %s ...", str(self._cmdl))
        if self._proc is None:
            _log.warning(" process already terminated!".format())
            return

        while self._proc.poll() is None:
            time.sleep(0.5)
            if (time.time() - start) > timeout:
                _log.debug("Reached timeout - Killing process")
                self._proc.kill()
                break
        self._proc.wait()
        self._proc.stdout.close()
        self._proc.stderr.close()
        self._proc = None  # delete to avoid zombie process


class ManagedProcessPool:
    """
    A pool of ManagedProcess-objects
    """
    def __init__(self):
        self._processes = {}

    def __len__(self) -> int:
        """Number of ManagedProcess objects

        Returns:
            int: the number of ManagedProcess objects
        """
        return len(self._processes)

    def __iter__(self) -> ManagedProcess:
        for proc in self._processes.values():
            yield proc

    def pid(self) -> list:
        return self._processes.keys()

    def alive(self) -> dict:
        alive = {}
        for pid in self._processes.keys():
            if psutil.pid_exists(pid):
                alive[pid] = True
            else:
                alive[pid] = False
        return alive

    def add(self, proc: ManagedProcess) -> None:
        """Adds a ManagedProcess to the process pool

        Args:
            process (ManagedProcess): The process to add
        """
        if isinstance(proc, ManagedProcess):
            if proc.pid not in self.pid():
                self._processes[proc.pid] = proc
            else:
                _log.error("Process with ID %d already in pool", proc.pid)
        else:
            _log.error("Passed object is not an instance of ManagedProcess")

    def terminate(self, timeout: int=5, threaded: bool=False) -> None:
        """Terminates all MagedProcess objects in the Pool

        Args:
            timeout (int, optional): The timeout for the termination. Defaults to 5.
            threaded (bool, optional): If set to True, threading is used for termination. Defaults to False.
        """
        if threaded:
            terminate_tasks = []
            for proc in self._processes.values():
                if proc.is_alive():
                    terminate_task = Thread(target=lambda: proc.terminate(timeout))
                    terminate_task.start()
                    terminate_tasks.append(terminate_task)
            for task in terminate_tasks:
                task.join()
        else:
            for proc in self._processes.values():
                if proc.is_alive():
                    proc.terminate(timeout)

    def clean(self) -> None:
        """
        Removes all dead processes from the pool
        """
        for key, val in self.alive().items():
            if val is False:
                self._processes.pop(key)

