#!/usr/bin/python3
"""
Command-line application to check if spead heaps are received 
"""
from argparse import ArgumentParser

from mpikat.core import logger
from mpikat.utils import numa

import spead2
import spead2.recv

spead_config = {
    "memory_pool": {
        "lower": 16384,
        "upper": (2*4*16*1024**2)+1024,
        "max_free": 128,
        "initial": 128},
    "threadpoolsize": 1,        # yes, as we only have one stream. Should use multiple streams otherwise
    "stream_config_max_heaps": 64,
    "ringstream_size": 128,
    }

def main():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('multicastgroups', type=str, help='Multicast groups to listen for for spead heaps', nargs='+')
    parser.add_argument('--ip', type=str, help='IP of the nic to use. Use fastest nic by default')
    parser.add_argument('--port', type=int, help='Port to listen to for MC traffic', default=7148)

    args = parser.parse_args()

    if args.ip is None:
        args.ip = numa.getFastestNic(numa_node=None, restrict_network=True)[1]['ip']

    print(f'Using IP for receiving: {args.ip}')
    print(f'Trying to receive data from {args.multicastgroups}')


    thread_pool = spead2.ThreadPool(threads=1)
    mem_pool = spead2.MemoryPool(lower=spead_config['memory_pool']['lower'], upper=spead_config['memory_pool']['upper'], max_free=spead_config['memory_pool']['max_free'], initial=spead_config['memory_pool']['initial'])
    stream_config = spead2.recv.StreamConfig(bug_compat=spead2.BUG_COMPAT_PYSPEAD_0_5_2,
            max_heaps=spead_config["stream_config_max_heaps"], memory_allocator=mem_pool,
            allow_out_of_order=True)     # pylint: disable=no-member
    ring_stream_config = spead2.recv.RingStreamConfig(heaps=spead_config["ringstream_size"],
            contiguous_only=False)     # pylint: disable=no-member
    stream = spead2.recv.Stream(thread_pool, stream_config, ring_stream_config)



    for mg in args.multicastgroups:
        #stream.add_udp_reader(mg, int(port), max_size = 9200L)
        stream.add_udp_reader(mg, int(args.port), max_size=9200, buffer_size=8388608, interface_address=args.ip)

    complete = 0
    incomplete = 0
    for heap in stream:
        if isinstance(heap, spead2.recv.IncompleteHeap):
            incomplete += 1
        else:
            complete += 1
        print(f"Received heap: {heap.cnt:8}     N complete heaps: {complete:4}      N incomplete heaps: {incomplete:4}")

if __name__ == "__main__":
    main()



