import asyncio
from mpikat.core import logger
from .registry import register
from .common import Stage


log = logger.getLogger(__name__)


@register(stage=Stage.measurement_prepare, class_name='PulsarPipeline')
async def test_set_source_name(context):
    """
    Setup source-name for pulsar pipeline test and setup noise diode
    """

    _, result = await context.status_server.request('sensor-control', 'project')
    _, result = await context.status_server.request('sensor-control', 'source-name')
    _, result = await context.status_server.request('sensor-set', 'project', 'TEST')
    _, result = await context.status_server.request('sensor-set', 'source-name', 'TEST_R')
    _, source_name = await context.status_server.request('sensor-value', 'source-name')
    context.output.append(source_name[0].arguments[4].decode('ascii'))


async def wait_and_append_result(context, key):
    await asyncio.sleep(60)
    _, result = await context.pipeline.request('sensor-value', key)
    context.output.append(result[0].arguments[4].decode('ascii'))


@register(stage=Stage.measurement_start, class_name='PulsarPipeline', tbc=True)
async def test_tscrunch(context):
    """
    The pipeline should produce a tscrunch PNG. Image needs to be checked manually.
    """
    await wait_and_append_result(context, 'tscrunch_PNG')


@register(stage=Stage.measurement_start, class_name='PulsarPipeline', tbc=True)
async def test_fscrunch(context):
    """
    The pipeline should produce a fscrunch PNG. Image needs to be checked manually.
    """
    await wait_and_append_result(context, 'fscrunch_PNG')


@register(stage=Stage.deconfigure, class_name='PulsarPipeline')
async def test_release_source_name(context):
    """
    Release source-name and project name
    """
    await context.status_server.request('sensor-release-all')

