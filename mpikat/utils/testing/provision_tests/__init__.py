import asyncio


from mpikat.core import logger

from . OutputProcessor import MultiOutput
from . HTMLOutput import HTMLOutput
from . TerminalOutput import TerminalOutput
from . registry import  register
from . common import *
from . execution  import *

from . import gated_spectrometer_tests
from . import gated_stokes_spectrometer_tests
from . import pulsar_processing_tests
from . import fits_interface_tests

_log = logger.getLogger('mpikat.utils.testing.provision_tests')

@register(stage=Stage.measurement_prepare, class_name='DigitizerController')
async def test_scg_timestamp_modulus(context):
    """
    The timestamp modulus should be between +/- 16
    """
    _log.debug('Checking timestamp modulus')
    _, result = await context.pipeline.request("sensor-value", 'packetizer_scg_time_stamps_modulus')
    _log.debug('Received: %s', result)

    tsm = int(result[0].arguments[4].decode('ascii'))
    _log.debug('Got timestamp modulous value of %i', tsm)
    assert abs(tsm) <= 16, f"Time stamp modulus should be in [-16, 16]. Actual value {tsm}"




