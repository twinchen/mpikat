"""
Determine the version of the code. If no version number is given, the version is deduced from git
"""
from importlib.metadata import version, PackageNotFoundError

import setuptools_scm

import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.core.version")


try:
    VERSION = setuptools_scm.get_version(version_scheme="release-branch-semver",
            root='../..', relative_to=__file__)
except LookupError:
    _log.debug("Cannot retrieve version from git, possibly because package was installed")
    try:
        VERSION = version("mpikat")
    except PackageNotFoundError:
        _log.warning("Cannot determine version")
        VERSION = '0.0.1+UNKNOWN'


_log.debug("VERSION: %s", VERSION)

if __name__ == "__main__":
    print(VERSION)
