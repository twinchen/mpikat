import unittest
import unittest.mock as mock
import multiprocessing as mp
import time
from mpikat.utils.testing import setup_redis
from mpikat.utils.data_streams import RedisJSONSender

IP = "127.0.0.1"
DB = 0
NAME = "TESTOBJ"

class Test_RedisJSONStream(unittest.TestCase):

    def setUp(self) -> None:
        self.queue = mp.Queue()
        self.redis_server = setup_redis(IP)
        self.port = self.redis_server.server_config["port"]
        self.tobj = RedisJSONSender(IP, self.port, DB, NAME, self.queue)

    def tearDown(self) -> None:
        if self.redis_server.running:
            self.redis_server.shutdown()

    def test_connect_server_reachable(self):
        self.tobj.connect()

    def test_connect_server_unreachable(self):
        self.redis_server.shutdown()
        with self.assertRaises(ConnectionError):
            self.tobj.connect()

    def test_send_not_connected(self):
        test_dict = {"cheese":"yammi"}
        with self.assertRaises(ConnectionError):
            self.tobj.send_json(test_dict)

    def test_send_bad_dictonary(self):
        test_dict = {"cheese":"yammi"}
        self.tobj.connect()
        with self.assertRaises(KeyError):
            self.tobj.send_json(test_dict)

    # Test below not supported by redislite instance because JSON.SET is not available
    # def test_send_good_dictonary(self):
    #     test_dict = {"name":"test","cheese":"yammi"}
    #     self.tobj.connect()
    #     self.tobj.send_json(test_dict)
    #     client = redis.Redis(IP, self.port, DB)
    #     self.assertEqual(test_dict, client.get(self.tobj.streams[-1]))

    def test_start_and_stop(self):
        self.tobj.start()
        self.assertEqual(self.tobj.is_alive(), True)
        self.tobj.stop()
        time.sleep(1)
        self.assertEqual(self.tobj.is_alive(), False)
        self.assertEqual(self.tobj.streams, set())

    def test_queue_fill(self):
        self.tobj.send_json = mock.MagicMock(name="send_json")
        self.tobj.start()
        self.assertEqual(self.tobj.is_alive(), True)
        for __ in range(10):
            self.queue.put({"name":"test","cheese":"yammi"})
        # self.assertEqual(self.tobj.send_json., 10)
        self.tobj.stop()
        time.sleep(1)
        self.assertEqual(self.tobj.is_alive(), False)


    # def test_send(self):


if __name__ == '__main__':
    unittest.main()
