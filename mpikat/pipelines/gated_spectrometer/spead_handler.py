import numpy as np
from mpikat.utils.timestamp_handler import TimestampHandler
from mpikat.pipelines.gated_spectrometer.format import GatedSpectrometerDataStreamFormat
from mpikat.utils.data_streams import RedisSpectrumStreamFormat, serialize_array
from mpikat.utils.spead_capture import SpeadPacket
from mpikat.core.data_stream import convert48_64
import mpikat.core.logger as logging


_log = logging.getLogger("mpikat.pipelines.gated_spectrometer.spead_handler")

class GatedSpectrometerSpeadHandler:
    """
    Parse heaps of gated spectrometer output from spead2 stream and returns a data object.
    """
    def __init__(self):
        self.timestamp_handler = None
        self.ig = GatedSpectrometerDataStreamFormat.create_item_group()

    def __call__(self, heap):
        """
        Crate an SpeadPacket object containing interpreted output data
        """
        _log.trace("Unpacking heap")
        items = self.ig.update(heap)

        if self.initialize(items):
            # Reprocess heap to get data
            items = self.ig.update(heap)

        return self.build_packet(items)

    def initialize(self, items):
        """
        Initialize handler on format update
        """
        if not GatedSpectrometerDataStreamFormat.ig_update(items, self.ig):
            return False
        _log.debug('Initializing handler')
        sync_time = convert48_64(items["sync_time"].value)
        sampling_rate = float(convert48_64(items["sampling_rate"].value))
        self.timestamp_handler = TimestampHandler(sync_time, sampling_rate)
        return True


    def build_packet(self, items):
        """
        Build a packet from the items
        """
        _log.trace('Building package')

        if not GatedSpectrometerDataStreamFormat.validate_items(items):
            return None
        packet = SpeadPacket(items)
        # Update timestamp count depending on epoch
        packet.timestamp_count = self.timestamp_handler.updateTimestamp(packet.timestamp_count)  # pylint: disable=no-member,attribute-defined-outside-init
        # Integration period does not contain efficiency of sampling as heaps may
        # be lost respectively not in this gate
        packet.integration_period = (packet.naccumulate * packet.fft_length) / float(packet.sampling_rate)  # pylint: disable=no-member
        # The reference time is in the center of the integration period
        packet.reference_time = float(packet.sync_time) + float(packet.timestamp_count) / float(packet.sampling_rate) + float(packet.integration_period/ 2.)  # pylint: disable=no-member
        return packet


def convert_to_redis_spectrum(idata: SpeadPacket, nchannels: int=1024) -> dict:
    """_summary_

    Args:
        idata (SpeadPacket): _description_

    Returns:
        tuple: _description_
    """
    streamtype_prefixes = {0: 'P', 1: 'S'}
    stream_format = RedisSpectrumStreamFormat
    stream_format.data_items["name"] = f"{streamtype_prefixes[idata.type]}{idata.polarization}_ND{idata.noise_diode_status}"
    spectrum = idata.data[1:].reshape([nchannels, (idata.data.size -1) // nchannels]).sum(axis=1)
    if np.isfinite(spectrum).all():
        stream_format.data_items["interval"] = idata.number_of_input_samples / idata.sampling_rate
        stream_format.data_items["timestamp"] = idata.reference_time
        stream_format.data_items["data"] = serialize_array(spectrum)
        stream_format.data_items["f_min"] = 0 #int(self.istream.stream_meta["central_freq"]) - int(idata.sampling_rate) / 4
        stream_format.data_items["f_max"] = 1024 #int(self.istream.stream_meta["central_freq"]) + int(idata.sampling_rate) / 4
        return stream_format.data_items
    return None