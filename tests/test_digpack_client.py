# pylint: disable=missing-class-docstring
import logging
import time
import unittest
import unittest.mock
import json

import aiokatcp
import redislite

from mpikat.core import datastore
from mpikat.utils import testing, get_port

from mpikat.pipelines.digpack_controller import DigitizerControllerPipeline, get_shared_sync_time


class TestDigpackPipeline(unittest.IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self.pipeline = DigitizerControllerPipeline("127.0.0.1", get_port())
        self.pipeline._client = unittest.mock.AsyncMock()
        self.pipeline._client._safe_request = unittest.mock.AsyncMock(return_value=([], []))
        self.redis_db = testing.setup_redis()
        self.edd_datastore = datastore.EDDDataStore(host="localhost", port=self.redis_db.server_config['port'])

    def tearDown(self):
        self.redis_db.shutdown()

    async def  test_pipeline_construction_and_shutdown(self):
        """Pipeline should be idle after construction"""
        self.assertEqual(self.pipeline.state, "idle")

    @unittest.mock.patch('mpikat.pipelines.digpack_controller.get_shared_sync_time')
    async def test_sync_time_error(self, mock_object):
        """After sync error sensor should be set to error"""
        mock_object.side_effect = ConnectionError
        await self.pipeline.set({'dummy_configure': True, "data_store": {'ip': "localhost", 'port': self.redis_db.server_config['port']}})
        await self.pipeline.configure()

        self.assertEqual(self.pipeline.sensors['noise_diode_sync_time'].status, aiokatcp.Sensor.Status.UNKNOWN)
        await self.pipeline.set_noise_diode_pattern(self.pipeline._config)

        self.assertEqual(self.pipeline.sensors['noise_diode_sync_time'].status, aiokatcp.Sensor.Status.WARN)
        self.assertEqual(self.pipeline.sensors['noise_diode_sync_time'].value, 0)


    async def test_noise_diode_update_in_datastore(self):
        self.pipeline._client.get_noise_diode_pattern = unittest.mock.AsyncMock(return_value=self.pipeline._config['noise_diode_pattern'])
        await self.pipeline.set({"data_store": {'ip': "localhost", 'port': self.redis_db.server_config['port']}, "dummy_configure": True})

        await self.pipeline.configure()
        await self.pipeline.capture_start()
        await self.pipeline.measurement_prepare({})

        ndp = self.edd_datastore.getTelescopeDataItem("noise_diode_pattern", "value")
        self.assertIsNotNone(ndp)
        d = json.loads(ndp)
        self.assertEqual(d, self.pipeline._config['noise_diode_pattern'])



class TestSharedSyncTime(unittest.TestCase):
    def setUp(self):
        self.client = redislite.StrictRedis()

    def tearDown(self):
        self.client.shutdown()

    def test_sync_time_repeat(self):
        """
        Multiple calls should yield same time
        """

        a = get_shared_sync_time(self.client, 'foo', 10)
        for _ in range(9):
            self.assertEqual(a, get_shared_sync_time(self.client, 'foo', 10))

    def test_sync_time_eqpire(self):
        """ After delay, sync time should not be the same but differ
        """
        a = get_shared_sync_time(self.client, 'foo', 1)
        self.assertEqual(a, get_shared_sync_time(self.client, 'foo', 1))
        time.sleep(1.1)
        self.assertNotEqual(a, get_shared_sync_time(self.client, 'foo', 1))

    def test_sync_time_starts_on_full_Second(self):
        a = get_shared_sync_time(self.client, 'foo', 1)
        self.assertLessEqual(abs(a - int(a)), 1E-9)






if __name__ == '__main__':
    unittest.main()
