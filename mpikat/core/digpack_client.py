"""
Interface to digitizer/packetizer.
"""
from abc import ABC, abstractmethod
import time
import json
import sys
import numpy as np
import aiokatcp
import asyncio
from mpikat.core import logger
from mpikat.utils import ip_utils

_log = logger.getLogger("mpikat.edd_digpack_client")

class DigitiserPacketiserError(Exception):
    pass

class PacketiserInterfaceError(Exception):
    pass

class PackerInterface(ABC):
    """
    Abstract interface to control digitizer/packetizer via a packer katcp
    server, or wrapper instance.
    """
    def __init__(self, client, command_prefix=""):
        """
            Args:
                host:  The host IP or name for the desired packetiser KATCP interface
                port:  The port number for the desired packetiser KATCP interface
                command_prefix: katcp commands will be prefixed by this string
        """
        self._client = client

        self._packer_command_prefix = command_prefix
        self._capture_started = False
        self._firmware = None


    async def check_packetizer_state(self):
        """
        Check if packetizer is ready. Raise DigitiserPacketiserError if not.
        """
        raise NotImplementedError


    async def _safe_request(self, prefix, request_name,  *args,  **kwargs):
        """
        Send a request to client and prints response OK /  error message.

        Args:
            request_name: Name of the request
            *args:        Arguments passed to the request.
        """
        timeout = kwargs.get('timeout', 2)
        p = await self.ping(timeout)
        if not p:
            raise RuntimeError(f"Cannot reach packetiser")
        _log.trace("Sending request '{}' with arguments {}".format(request_name, args))
        _log.debug("Adding request prefix: %s", self._packer_command_prefix)
        if prefix == True:
            request_name = self._packer_command_prefix + request_name
        else:
            request_name = request_name
        request_name = request_name.replace("_","-")
        _log.debug("Sending packetiser request '{}' with arguments {}".format(request_name, args))

        try:
            response = await self._client.request(request_name, *args, **kwargs)
        except (aiokatcp.FailReply, TimeoutError) as E:
            _log.error("'%s' request failed with error: %s", request_name, E)
            raise E
        return response

    async def ping(self, timeout=2):
        """
        Test if connection to product is running / alive.
        """
        task = self._client.wait_connected()
        try:
            await asyncio.wait_for(task, timeout=timeout)
        except asyncio.exceptions.TimeoutError:
            _log.error("%s: Timeout Reached. Packetiser inactive")
            return False
        return True

    async def query_packer_command_prefix(self):
        """
        Set the command prefix used for the communication.
        """
        raise NotImplementedError

    @abstractmethod
    def set_predecimation(self, factor):
        """
        Set a pre-decimation factor for the packetizer - for e.g. factor=2 only every second sample is used.
        """

    @abstractmethod
    def set_noise_diode_firing_pattern(self, percentage, period, starttime="now"):
        """
        Set noise diode frequency to given value.
        """

    @abstractmethod
    def set_sampling_rate(self, rate, retries=3):
        """
        Sets the sampling rate.

        Args:
            rate:    The sampling rate in samples per second (e.g. 2.6 GHz should be passed as 2600000000.0)


        To allow time for re-initialisation of the packetiser firmware during this call we enforce a 10
        second sleep before the function returns.
        """

    @abstractmethod
    def set_digital_filter(self, filter_number):
        """
        Sets the digital filter number.
        """

    @abstractmethod
    def set_bit_width(self, nbits):
        """
        Sets the number of bits per sample out of the packetiser

        Args:
            nbits:  The desired number of bits per sample (e.g. 8 or 12)
        """

    @abstractmethod
    def flip_spectrum(self, flip):
        """
        Flip spectrum flip = True/False to adjust for even/odd Nyquist zone
        """

    @abstractmethod
    def set_destinations(self, v_dest, h_dest):
        """
        Sets the multicast destinations for data out of the packetiser

        Args:
            v_dest:  The vertical polarisation channel destinations
            h_dest:  The horizontal polarisation channel destinations

        The destinations should be provided as composite stream definition
        strings, e.g. 225.0.0.152+3:7148 (this defines four multicast groups:
        225.0.0.152, 225.0.0.153, 225.0.0.154 and 225.0.0.155, all using
        port 7148). Currently the packetiser only accepts contiguous IP
        ranges for each set of destinations.
        """

    @abstractmethod
    def set_mac_address(self, intf, mac):
        """
        Sets the mac addresses of the source NICs of the packetiser

        Args:
            intf: The number of the NIC
            mac:  The mac of the NIC
        """

    @abstractmethod
    def set_predecimation_factor(self, factor):
        """
        Sets the predecimation_factor for data out of the packetiser

        Args:
            factor: (e.g. 1,2,4,8)

        """

    @abstractmethod
    def enable_snapshot(self, time_interval=5):
        """
        Enable spectrum snapshot data taking.
        """

    @abstractmethod
    def set_interface_address(self, intf, ip):
        """
        Set the interface address for a packetiser qsfp interface

        Args:

            intf:   The interface specified as a string integer, e.g. '0' or '1'
            ip:     The IP address to assign to the interface
        """

    async def capture_start(self):
        """
        Start data transmission for both polarisation channels

        This method uses the packetiser's 'capture-start' method which is an
        aggregate command that ensures all necessary flags on the packetiser
        and set for data transmission.  This includes the 1PPS flag required by
        the ROACH2 boards.
        """
        if not self._capture_started:
            #Only start capture once and not twice if received configure
            self._capture_started = True
            await self._safe_request(True, "capture_start", "vh")


    async def capture_stop(self):
        """
        Start data transmission for both polarisation channels on next capture start
        """
        _log.warning("Not stopping data transmission")
        self._capture_started = False
        await self._safe_request(True, "capture_stop", "vh")


    async def configure(self, config):
        """
        Applying configuration received in dictionary
        """
        self._capture_started = False
        await self._safe_request(True, "capture_stop", "vh")
        await self.set_sampling_rate(config["sampling_rate"])
        await self.set_predecimation(config["predecimation_factor"])
        await self.flip_spectrum(config["flip_spectrum"])
        await self.set_bit_width(config["bit_width"])
        await self.set_destinations(config["v_destinations"], config["h_destinations"])

        for interface, ip_address in config["interface_addresses"].items():
            await self.set_interface_address(interface, ip_address)
        if "sync_time" in config:
            await self.synchronize(config["sync_time"])
        else:
            await self.synchronize()

        if "noise_diode_frequency" in config:
            await self.set_noise_diode_frequency(config["noise_diode_frequency"])

        await self.capture_start()

    @abstractmethod
    def get_sync_time(self):
        """
        Get the current packetiser synchronisation epoch

        Return:
            The synchronisation epoch as a UNIX time float
        """

    @abstractmethod
    def get_snapshot(self):
        """
        Returns dictionary with snapshot data from the packetizer.
        """

    async def get_source_ip(self, interface):
        """
        Get the source IP of an interface .

        Return:
            The IP address.
        """

    async def get_source_mac(self, interface):
        """
        Get the source mac of an interface .

        Return:
            The MAC address.
        """

    def get_destination(self, interface):
        """
        Get the destination string  aaa.bbb.ccc.ddd+X:PPPP from the interface
        """


    async def synchronize(self, unix_time=None, offset_to_current=2):
        """
        Set the synchronisation epoch for the packetiser

        Args:
            unix_time:  The UNIX time to synchronise at. If no value is provided a
                               reasonable value will be selected.
            offset_to_current: Offset to the current time if no time is provided

        When explicitly setting the synchronisation time it should be a second
        or two into the future allow enough time for communication with the
        packetiser. If the time is in the past by the time the request reaches
        the packetiser the next 1PPS tick will be selected.  Users *must* call
        get_sync_time to get the actual time that was set.  This call will
        block until the sync epoch has passed (i.e. if a sync epoch is chosen
        that is 10 second in the future, the call will block for 10 seconds).
        """
        if not unix_time:
            unix_time = round(time.time() + offset_to_current)
        await self._safe_request(True, "synchronise", 0, unix_time)
        sync_epoch = await self.get_sync_time()
        if sync_epoch != unix_time:
            _log.warning("Requested sync time {} not equal to actual sync time {}".format(
                unix_time, sync_epoch))

    async def invert_1pps(self, invert_1pps):
        """
        Invert the 1pps signal

        Args:
            invert_1pps : True or False
        """
        if invert_1pps == False:
            await self._safe_request(True, "packetizer-1pps-invert-disable")
        else:
            await self._safe_request(True, "packetizer-1pps-invert-enable")

    async def set_noise_diode_frequency(self, frequency, starttime='now'):
        """
        Set noise diode frequency to given value.
        """
        if frequency == 0:
            await self.set_noise_diode_firing_pattern(0.0, 0.0, starttime)
        else:
            await self.set_noise_diode_firing_pattern(0.5, 1./frequency, starttime)


    def stop(self):
        """
        Stop client
        """
        self._client.stop()

class DigitiserPacketiserClient(PackerInterface):
    """
    Implements interface to packer v2 instances.
    """
    def __init__(self, client):
        """
        Wraps katcp commands to control a digitiser/packetiser.
        """
        PackerInterface.__init__(self, client, command_prefix="")

        self._sampling_modes = {
            6000000000: ("virtex", "6.0GHz"),
            5000000000: ("virtex", "5.0GHz"),
            4096000000: ("virtex7_dk769b", "4.096GHz,4.096GHz", "3,3"),
            4000000000: ("virtex7_dk769b", "4.0GHz,4.0GHz", "5,5"),
            3600000000: ("virtex7_dk769b", "3.6GHz,3.6Hz", "7,7"),
            3500000000: ("virtex7_dk769b", "3.5GHz,3.5GHz", "7,7"),
            3200000000: ("virtex7_dk769b", "3.2GHz,3.2GHz", "9,9"),
            2600000000: ("virtex7_dk769b", "2.6GHz,2.6GHz", "3,3"),
            1750000000: ("virtex7_dk769b_test146.mkt", "3.5GHz,3.5GHz", "7,7")  # This is  a special mode for the meerkat digital filter cores inside the EDD.
                            # An effective 1750 MHz sampling rate/ 875 MHz
                            # bandwidth  is achieved by digital filtering of
                            # the 3.5GHz sampled rate!
            }               # This is quite hacky, and the design of this client has to be  has to be improved. Possibly by having a client per firmware
        #self.logger = client.logger

    async def query_packer_command_prefix(self):
        _log.debug("Obtaining command prefix")

        response, informs = await self._safe_request(False, "hwinfo")
        config = json.loads(informs[0].arguments[0].decode('utf-8'))

        self._set_packer_command_prefix(config)
        self._set_firmware(config)

    def _set_packer_command_prefix(self, config):
        self._packer_command_prefix = config.get('nodename', '') + "_"
        _log.debug("Using node name: %s", self._packer_command_prefix)

    def _set_firmware(self, config):
        self._firmware = config.get('activecorefile', '')

    async def set_predecimation(self, factor):
        allowedFactors = [1, 2, 4, 8, 16] # Eddy Nussbaum, private communication
        if factor not in allowedFactors:
            raise RuntimeError("predicimation factor {} not in allowed factors {}".format(factor, allowedFactors))
        await self._safe_request(True,"packetizer_sproc_predecimation", factor)

    async def set_noise_diode_firing_pattern(self, percentage, period, starttime="now"):
        _log.debug("Set noise diode firing pattern")
        await self._safe_request(True, "noise_source", starttime, percentage, period)

    async def _check_operational_state(self):
        """
        Check if state of the packetizer is in normal operation state.
        """
        _log.debug("Checking packertizer status")
        #await self._client.until_synced()

        async def _check_packertizer_state():
            _log.debug("Checking state of packetizer.system-state")
            _, informs = await self._safe_request(False, "sensor-value", "edd.packetizer.system-state")
            status  = informs[-1].arguments[-1].decode()
            if not status == "normal operation":
                _log.warning("Packetizer is not in normal operation state, currently at '{}' state".format(status))
                raise Exception
            _log.debug("Packetizer is in operational state")
        await _check_packertizer_state()

    async def get_noise_diode_pattern(self):
        """
        Get the oise diode pattern from the packetizer
        """

        result = {}
        requests = [self._safe_request(False, "sensor-value", "edd.packetizer.noisediode.onfraction"), self._safe_request(False, "sensor-value", "edd.packetizer.noisediode.period")]
        res = await asyncio.gather(*requests)
        result['percentage'] = res[0][1][-1].arguments[-1].decode()
        result['period'] = res[1][1][-1].arguments[-1].decode()

        return result

    async def check_packetizer_state(self):
        """
        Check if packetizer is ready. Raise DigitiserPacketiserError if not.
        """
        _log.debug('Checking packetizer state')
        _, informs = await self._safe_request(False, "sensor-value", "edd.digitizer.is-available")
        _log.info(informs[-1].arguments[-1].decode())
        digitizer_available = informs[-1].arguments[-1].decode()
        _log.debug('Digitizer available: %s', digitizer_available)

        if digitizer_available.upper() == 'FALSE':
            _, informs = await self._safe_request(False, "sensor-value", "edd.packetizer.system-state")
            state = informs[-1].arguments[-1].decode()
            raise PacketiserInterfaceError(f'Digitzier not available: {state}')

    async def set_sampling_rate(self, rate, retries=60):
        """
        Sets the sampling rate.

        Args:
            rate:    The sampling rate in samples per second (e.g. 2.6 GHz should be passed as 2600000000.0)


        To allow time for re-initialisation of the packetizer firmware during this call we enforce a 10
        second sleep before the function returns.
        """

        try:
            args = self._sampling_modes[int(rate)]
        except KeyError as E:
            pos_freqs = "\n".join(["  - {} Hz ".format(f) for f in self._sampling_modes])
            error_msg = "Frequency {} Hz not in possible frequencies:\n{}".format(rate, pos_freqs)
            _log.error(error_msg)
            raise DigitiserPacketiserError(error_msg) from E

        await self._safe_request(True, "packetizer_system_reinit", *args)
        self._firmware = args[0]
        sleep_time = 0
        while True:
            _log.debug("Operational check %i / %i", sleep_time, retries)
            await asyncio.sleep(1)
            try:
                await self._check_operational_state()
            except Exception as error:
                if sleep_time >= retries:
                    raise error
                _log.warning("sleep for 1 second and check again")
                sleep_time += 1
            except Exception as error:
                _log.warning("Cought KATCpSensorError: %s", error)
                sleep_time += 1
            else:
                break

    async def set_digital_filter(self, filter_number):
        await self._safe_request(True, "packetizer_sproc_filter_selection_set", filter_number)

    async def set_bit_width(self, nbits):
        valid_modes = {
            8: "edd08",
            10: "edd10",
            12: "edd12"
        }
        _log.warning("Firmware switch for bit set mode!")
        if self._firmware == "virtex7_dk769b_test146.mkt":
            _log.debug("Firmware does not support setting bit rate!")
            return
        try:
            mode = valid_modes[int(nbits)]
        except KeyError as E:
            msg = "Invalid bit depth, valid bit depths are: {}".format(
                valid_modes.keys())
            _log.error(msg)
            raise DigitiserPacketiserError(msg) from E
        await self._safe_request(True, "packetizer_sproc_switchmode", mode)

    async def flip_spectrum(self, flip):
        """
        Flip spectrum flip = True/False to adjust for even/odd Nyquist zone
        """
        if flip:
            await self._safe_request(True, "packetizer_sproc_flipsignalspectrum", "on")
        else:
            await self._safe_request(True, "packetizer_sproc_flipsignalspectrum", "off")

    async def set_destinations(self, v_dest, h_dest):
        # packetizer 2 uses  0 / 1 notation for consistency reasons
        await self._safe_request(True, "capture_destination", "0", v_dest)
        await self._safe_request(True, "capture_destination", "1", h_dest)


    async def set_mac_address(self, intf, mac):
        if not ip_utils.is_valid_nic_mac(mac):
            raise RuntimeError(f"MAC '{mac}' is not valid.")
        await self._safe_request(True, "packetizer_dnet_source_mac_set", intf, mac)


    async def set_predecimation_factor(self, factor):
        await self._safe_request(True, "packetizer_sproc_predecimation", factor)


    async def enable_snapshot(self, time_interval=5):
        await self._safe_request(True, "packetizer_snapshot_enable_spec", time_interval)


    async def set_interface_address(self, intf, ip):
        await self._safe_request(True, "packetizer_dnet_source_ip_set", intf, ip)

    async def get_sync_time(self):
        _, informs = await self._safe_request(True, "packetizer_sproc_get_zero_time")
        _log.info(informs[-1].arguments[-1].decode())
        sync_epoch = float(informs[-1].arguments[-1].decode())
        return sync_epoch

    async def get_source_ip(self, interface):
        _, informs = await self._safe_request(True, "packetizer_dnet_source_ip_get", interface)
        return informs[-1].arguments[-1].decode()

    async def get_source_mac(self, interface):
        _, informs = await self._safe_request(True, "packetizer_dnet_source_mac_get", interface)
        return informs[-1].arguments[-1].decode()

    async def get_destination(self, interface):
        _, ip = await self._safe_request(True, "packetizer_dnet_destination_ip_get", interface, interface)
        _, port = await self._safe_request(True, "packetizer_dnet_destination_port_get", interface, interface)
        return "{}+3:{}".format(ip[-1].arguments[-1].decode('utf-8'), port[-1].arguments[-1].decode('utf-8'))

    async def get_snapshot(self):
        response = await self._safe_request(True, "packetizer_snapshot_get_spec")
        res = {}
        for message in response.informs:
            key = message.arguments[0].decode('ascii')
            if 'header' in key:
                res[key] = dict(
                        band_width=float(message.arguments[1]) * 1e3,
                        integration_time=float(message.arguments[2]),
                        num_channels=int(message.arguments[3]),
                        band_width_adc=float(message.arguments[4]),
                        spec_counter=int(message.arguments[5]),
                        timestamp=message.arguments[6])
            elif 'adc' in key:
                res[key] = np.fromstring(message.arguments[1], dtype=np.float32)
            elif 'level' in key:
                res[key] = np.fromstring(message.arguments[1], dtype=np.int32)

        return res

if __name__ == "__main__":
    print("CLI program is now separated from classes - use scripts/edd_digpack_cli.py")

