import asyncio
import unittest

import threading
import json

import aiokatcp
from aiokatcp.client import Client
from aiokatcp.connection import FailReply

from mpikat.core.edd_pipeline_aio import EDDPipeline, state_change, pipeline_task, StateChange, StateChangeFail
from mpikat.core import logger
from mpikat.core import datastore
from mpikat.utils import testing, get_port


_log = logger.getLogger("mpikat.testAsyncEddPipelines")

host = "127.0.0.1"


class Test_PipelineRegister(unittest.IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.client = await asyncio.wait_for(
            Client.connect(host, self.port), timeout=5.0
        )

    def setUp(self):
        self.port = get_port()
        self.server_thread = testing.launch_server_in_thread(EDDPipeline, host, self.port)
        self.server = self.server_thread.server
        self.redis_db = testing.setup_redis()

    def tearDown(self):
        self.client.close()
        self.redis_db.shutdown()
        self.server_thread.stop().result()
        self.server_thread.join(timeout=5)

    async def test_register(self):
        """
        Test test_register:
        """
        port = self.redis_db.server_config["port"]
        await self.server.set({'id': 'dummy'})
        await self.client.request('register', f'127.0.0.1:{port}')
        # after register the pipeline name should be in the Redis
        ds = datastore.EDDDataStore(port=self.redis_db.server_config['port'], host='127.0.0.1')

        self.assertEqual(len(ds.products), 1)
        self.assertEqual(ds.products[0]['id'], 'dummy')


class Test_RequestEDDPipeline(unittest.IsolatedAsyncioTestCase):

    async def asyncSetUp(self):
        self.client = await asyncio.wait_for(
            Client.connect(host, self.port), timeout=5.0
        )

    def setUp(self):
        self.port = get_port()
        self.server_thread = testing.launch_server_in_thread(EDDPipeline, host, self.port)
        self.server = self.server_thread.server

    def tearDown(self):
        self.client.close()
        self.server_thread.stop().result()
        self.server_thread.join(timeout=5)


    async def test_set_log_level(self):
        """
        Test request_set_log_level:
        1. Should test if the log-level is set properly
        2. Should raise an exception on faulty input value while actual value not change
        """
        await self.client.request('set-log-level', 'DEBUG')
        self.assertEqual(self.server.sensors['edd-log-level'].value, 'DEBUG')

        with self.assertRaises(FailReply):
            await self.client.request('set-log-level', 'CHEESE')
        self.assertEqual(self.server.sensors['edd-log-level'].value, 'DEBUG')

    async def test_log_level(self):
        """ Test request_log_level:
        """
        self.server.sensors['edd-log-level'].set_value("DEBUG")
        reply, __ = await self.client.request('log-level')
        self.assertEqual(self.server.sensors['edd-log-level'].value, reply[0].decode())

    async def test_set_katcp_internal_log_level(self):
        """
        Test test_set_katcp_internal_log_level:
        """
        reply, __ = await self.client.request('set-katcp-internal-log-level', 'DEBUG')
        self.assertEqual(reply[0].decode(), 'DEBUG')
        self.assertEqual(self.server._log_level, 'DEBUG')

    async def test_whoami(self):
        """
        Test test_whoami:
        """
        reply, __ = await self.client.request('whoami')
        expected = self.server._get_whoami()
        self.assertEqual(reply[0].decode(), expected)

    async def test_override_state(self):
        """
        Test test_override_state:
        """
        # Test existing state
        await self.client.request('override-state', 'configuring')
        self.assertEqual(self.server.state, 'configuring')
        # Test non existing state
        with self.assertRaises(FailReply):
            await self.client.request('override-state', 'cheese')
        self.assertEqual(self.server.state, 'configuring')



    async def test_set_default_config(self):
        """
        Test request_set_default_config, should test that the default config is set
        """
        reply, inform = await self.client.request('set-default-config')
        self.assertEqual(inform, [])
        self.assertEqual(reply, [])

    async def test_halt(self):
        """"
        Test request_halt, should test if the loop is closed on halt
            We need a new server client pair, otherwise the tear-down fails
        """
        port = get_port()
        thread = testing.launch_server_in_thread(EDDPipeline, host, port)
        server = thread.server
        client = await asyncio.wait_for(
            Client.connect(host, port), timeout=5.0
        )
        self.assertFalse(server.loop.is_closed())
        await client.request('halt')
        client.close()
        thread.join()
        self.assertTrue(server.loop.is_closed())


    async def test_set(self):
        # Test existing state
        await self.client.request('set', '{"id":"dummy"}')
        self.assertEqual(self.server._config["id"], 'dummy')
        # Expected to fail because key does not exist
        with self.assertRaises(FailReply):
            await self.client.request('set', '{"i_like":"cheese"}')

    async def test_configure(self):
        await self.client.request('configure', '{"id":"dummy"}')
        self.assertEqual(self.server._config["id"], 'dummy')
        self.assertEqual(self.server.state, 'configured')
        # Expected to fail because key not exists
        self.server.state = "idle"
        with self.assertRaises(FailReply):
            await self.client.request('configure', '{"i_like":"cheese"}')
        self.assertEqual(self.server.state, 'idle')
        # Expect to fail because not in allowed states
        self.server.state = "ready"
        with self.assertRaises(FailReply):
            await self.client.request('configure', '{"id":"dummy"}\n')
        self.assertEqual(self.server.state, 'ready')

    async def test_capture_start(self):
        self.server.state = "configured"
        await self.client.request('capture-start')
        self.assertEqual(self.server.state, 'ready')
        # Expect to fail because not in allowed states
        self.server.state = "ready"
        with self.assertRaises(FailReply):
            await self.client.request('capture-start')
        self.assertEqual(self.server.state, 'ready')


    async def test_capture_stop(self):
        self.server.state = "configured"
        await self.client.request('capture-stop')
        self.assertEqual(self.server.state, 'ready')
        # Expect to fail because not in allowed states
        self.server.state = "ready"
        with self.assertRaises(FailReply):
            await self.client.request('capture-start')
        self.assertEqual(self.server.state, 'ready')

    async def test_notes_sensor(self):
        self.assertEqual(self.server.sensors['notes'].value, "")

        self.server.add_note("FOO")
        self.assertEqual(self.server.sensors['notes'].value.count("FOO"), 1)

        self.server.add_note("FOO")
        self.assertEqual(self.server.sensors['notes'].value.count("FOO"), 1)

        self.server.add_note("BAR")
        self.assertEqual(self.server.sensors['notes'].value.count("FOO"), 1)
        self.assertEqual(self.server.sensors['notes'].value.count("BAR"), 1)
        self.server.clear_note("BAR")
        self.assertEqual(self.server.sensors['notes'].value.count("FOO"), 1)
        self.assertEqual(self.server.sensors['notes'].value.count("BAR"), 0)
        self.server.flush_notes()
        self.assertEqual(len(self.server.sensors['notes'].value), 0)

    async def testWatchDogError(self):
        """
        After watchdog error is called, pipeline should be in error
        """
        self.server.watchdog_error()
        self.assertEqual(self.server.state, 'error')

    async def test_outputfile_register(self):
        """List of output files should contain registered files"""
        self.assertEqual(len(self.server.get_output_files()), 0)
        self.server.add_output_file('foo')
        self.assertEqual(len(self.server.get_output_files()), 1)
        self.assertTrue("foo" in self.server.get_output_files())

    async def test_outputfile_register_strips_prefix(self):
        self.assertEqual(len(self.server.get_output_files()), 0)
        self.server.add_output_file('/mnt/foo')
        self.assertTrue("foo" in self.server.get_output_files())
        self.assertEqual(len(self.server.get_output_files()), 1)

    async def test_measurement_stop_returns_and_clears_list(self):
        self.server.state = 'measuring'
        self.server.add_output_file('/mnt/foo/bar')
        self.server.add_output_file('/mnt/bar')
        res = await self.client.request('measurement-stop', True)
        oflist = json.loads(res[0][0])

        self.assertIn('foo/bar', oflist)
        self.assertIn('bar', oflist)
        self.assertEqual(len(oflist), 2)
        for f in oflist.values():
            # id and type are in metadata of file
            self.assertIn("pipeline_id", f)
            self.assertIn("pipeline_type", f)

        self.assertEqual(len(self.server.get_output_files()), 0)



    async def test_measurement_stop_returns_file_list_only_when_requested(self):
        self.server.state = 'measuring'
        self.server.add_output_file('/mnt/foo/bar')
        self.server.add_output_file('/mnt/bar')
        res = await self.client.request('measurement-stop', False)
        self.assertEqual(len(res[0]), 0)



class TestStateChangeDecorator(unittest.IsolatedAsyncioTestCase):
    async def testOverrideByStateChangeException(self):
        """Pipeline state should be given by exception and not decorator"""

        class TestPipeline(EDDPipeline):
            @state_change(target="configured", allowed=["idle"], intermediate="configuring")
            async def configure(self):
                raise StateChange('measuring')

        pipeline = TestPipeline(host, 0)
        await pipeline.configure()
        self.assertEqual(pipeline.state, 'measuring')


    async def testOverrideByStateChangeExceptionWithError(self):
        """Pipeline state should be given by decorator and exception raised"""

        class TestPipeline(EDDPipeline):
            @state_change(target="configured", allowed=["idle"], intermediate="configuring")
            async def configure(self):
                raise StateChangeFail('measuring', "foo")

        pipeline = TestPipeline(host, 0)
        with self.assertRaises(aiokatcp.FailReply):
            await pipeline.configure()
        self.assertEqual(pipeline.state, 'measuring')





class Test_PipelineTasks(unittest.IsolatedAsyncioTestCase):

    async def asyncSetUp(self):
        self.client = await asyncio.wait_for(
            Client.connect(host, self.port), timeout=5.0
        )

    def setUp(self):

        class TestPipeline(EDDPipeline):
            """
            Test pipeline. The pipeline exposes three synchronisation events to communicate with the test.

            task_started: The event is set inside the server tasks when it is running
            task_continue : The task will wait until this event is set from the outside
            task_finished: The event is set after the task is finished.

            Tasks should be executed using the start_task from the calling thread.

            """
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                _log.debug('Constructing in %s', threading.current_thread().name)

                self.raise_error = False

                # task will wait for this bevent vefore finishing. This has to
                # be an asyncio event, as otherwise the loop will be stopped
                # and cancellation willnot be handled
                self.task_continue = asyncio.Event()
                # These have to be threading events, as for inter thread
                # communication
                self.task_started = threading.Event()
                self.task_finished = threading.Event()

            @pipeline_task(timeout=10)
            async def task(self):
                """
                A pipeline task
                """
                await self._task()


            async def _task(self):
                """ The actual task is the same for the configure request and the task"""
                _log.debug('Starting run task in %s', {threading.current_thread().name})
                self.task_started.set()
                _log.debug('task: waiting for task continue event')
                await self.task_continue.wait()

                _log.debug('task: received continue event')
                if self.raise_error:
                    raise RuntimeError('FOO')

            @state_change(target="configured", allowed=["idle"], intermediate="configuring")
            async def configure(self):
                _log.debug('Configuring [executing task]...')
                await self._run_task_wrapper()
                _log.debug('configured: [task executed]')

            async def _run_task_wrapper(self):
                """
                Runs the task and set the task finished event afterwards.
                """
                try:
                    _log.debug('Starting run_task_wrapper')
                    res = await self.task()
                    _log.debug('Setting finished task event in %s', threading.current_thread().name)
                    self.task_finished.set()
                    _log.debug('Done setting finished task event in %s', threading.current_thread().name)
                except Exception as E:
                    res = None
                    # Log+Reraise as exceptions may be lost otherwise if the
                    # task is not awaited / results are queried
                    _log.error('Exception in wrapper!')
                    _log.exception(E)
                    raise E
                return res

            def start_task(self):
                """Thread safe way to start the task using the run_task_wrapper"""
                f = asyncio.run_coroutine_threadsafe(self._run_task_wrapper(), loop=self.loop)
                _log.debug('Waiting for task start event in %s', threading.current_thread().name)
                self.task_started.wait()
                return f


        self.port = get_port()
        self.server_thread = testing.launch_server_in_thread(TestPipeline, host, self.port)
        self.server = self.server_thread.server

    def tearDown(self):
        self.client.close()
        self.server_thread.stop().result()
        self.server_thread.join(timeout=5)


    async def test_task_creation(self):
        """Verify that the rN_task task is added to the pipeline when it runs and is removed afterwards"""
        # Task should be added to the pipeline only when created
        self.assertEqual(len(self.server._pipeline_tasks), 0)
        self.server.start_task()
        self.assertEqual(len(self.server._pipeline_tasks), 1)

        _log.debug('Allow task to finish')
        self.server.loop.call_soon_threadsafe(self.server.task_continue.set)

        _log.debug('Waiting for task to finish in %s',  threading.current_thread().name)
        self.server.task_finished.wait(timeout=5)
        # Task should be removed when it is done
        self.assertEqual(len(self.server._pipeline_tasks), 0)

    async def test_exception_during_task(self):
        """ Exceptions occuring during the task should propagate to the caller"""
        # Cancelled tasks shoudl return None
        self.server.raise_error = True
        f = self.server.start_task()
        with self.assertRaises(RuntimeError):
            self.server.loop.call_soon_threadsafe(self.server.task_continue.set)
            f.result()
            self.server.task_finished.wait(timeout=5)

        # Canceled Task should be removed from the task list
        self.assertEqual(len(self.server._pipeline_tasks), 0)


    async def test_reply_on_cancellation(self):
        def cancellation_task():
            """
            Waits for the server to start and then cancels the task.
            """

            _log.debug('Waiting for task to start in %s',  threading.current_thread().name)
            self.server.task_started.wait()
            _log.debug('Issuing task cancellation from %s',  threading.current_thread().name)
            self.server.loop.call_soon_threadsafe(self.server._cancel_pipeline_tasks)
            _log.debug('Done with task cancellation in %s',  threading.current_thread().name)

        cancellation_thread = threading.Thread(target=cancellation_task)
        cancellation_thread.start()

        # in the main thread we run only the client and expect a meaningfull
        # failreply
        with self.assertRaisesRegex(FailReply, 'cancelled'):
            configure_result = await self.client.request('configure')

        cancellation_thread.join()


if __name__ == "__main__":
    unittest.main()
