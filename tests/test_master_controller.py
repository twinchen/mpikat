# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring

import unittest
from unittest import  mock
import asyncio
import json
import tempfile
import os
import shutil
import datetime
import git
import yaml

from aiokatcp.connection import FailReply
from aiokatcp.client import Client

from mpikat.core.datastore import EDDDataStore
from mpikat.core.edd_pipeline_aio import EDDPipeline

from mpikat.pipelines.master_controller import ConfigurationGraph, EddMasterController, get_ansible_fail_message, gather_and_throw, get_ansible_inventory, config_update
from mpikat.utils.process_tools import ProcessException

from mpikat.core import logger as logging
from mpikat.utils import testing, get_port, ip_manager, ip_utils

from mpikat.core.telescope_meta_information_server import TMIServer

_log = logging.getLogger("mpikat.test_master_controller")

# Constants
host = "127.0.0.1"
inventory = 'inventory'

MOCK_REDIS = None

def setUpModule():
    global MOCK_REDIS
    MOCK_REDIS = testing.setup_redis()

def tearDownModule():
    global MOCK_REDIS
    MOCK_REDIS.shutdown()



def initialize_mock_provision_directory():
    """
    initializes a directory so that it passes as a provision repository
    """
    repodir = tempfile.mkdtemp()

    repo = git.Repo.init(repodir)

    provision_dir = os.path.join(repodir, 'provison_descriptions')
    os.mkdir(provision_dir)
    example_yml = os.path.join(provision_dir, 'example.yml')
    with open(example_yml, 'w') as f:
        f.write("""
- hosts: gpu_server[1]
  vars:
    edd_group: 50000
  roles:
    - role: EDD.core.gated_spectrometer
      container_name: gated_spectrometer_2G

- hosts: gpu_server[0]
  vars:
    edd_group: 50000
  roles:
    - role: EDD.core.gated_spectrometer
      container_name: gated_spectrometer_3G
""")
        f.flush()

    example_json = os.path.join(provision_dir, 'example.json')
    with open(example_json, 'w') as f:
        f.write("{}")
        f.flush()

    inventory_file = os.path.join(repodir, inventory)
    with open(inventory_file, 'w') as f:
        f.write('---')
        f.flush()

    repo.index.add(['inventory', 'provison_descriptions/example.yml', 'provison_descriptions/example.json'])
    repo.index.commit('Added inventory + example')

    # Clone the fake repository to test provision updates
    repodirc = tempfile.mkdtemp()
    git.Repo.clone_from(repodir, repodirc)

    return repodirc



class TestConfigurationGraph(unittest.TestCase):
    """
    Test ConfigurationGraph-Class
    """
    def setUp(self):
        self.graph = ConfigurationGraph()

    def test_empty_default_graph(self):
        """
        Should test an empty graph
        """
        self.assertEqual(self.graph.dag.number_of_nodes(), 0)
        self.assertEqual(self.graph.dag.number_of_edges(), 0)
        self.assertEqual(self.graph.graph_sensor.value, "")

    def test_large_graph(self):
        """
        Should test a large graph
        """
        g = {'A0': {}, 'A1': {}}
        g.update({'B{}'.format(i): {'input_data_streams': [{'source': 'A0:out'}]} for i in range(10)})
        g.update({'C{}'.format(i): {'input_data_streams': [{'source': 'A1:out'}]} for i in range(10)})

        self.graph.update(g)
        self.assertEqual(self.graph.dag.number_of_nodes(), 22)
        self.assertEqual(self.graph.dag.number_of_edges(), 20)

        #sensor should contain SVG
        self.assertIn(b"<svg", self.graph.graph_sensor.value)

        self.graph.clear()

        self.assertEqual(self.graph.dag.number_of_nodes(), 0)
        self.assertEqual(self.graph.dag.number_of_edges(), 0)
        self.assertEqual(self.graph.graph_sensor.value, "")


class Test_MasterController_requests(unittest.IsolatedAsyncioTestCase):
    """
    Test that requests call implementation functions
    """
    def setUp(self) -> None:
        self.repodir = initialize_mock_provision_directory()
        self.mock_redis = testing.setup_redis()

        self.port = get_port()
        self.thread = testing.launch_server_in_thread(
            EddMasterController, '0.0.0.0', self.port,
            host, self.mock_redis.server_config['port'],
            self.repodir, inventory
        )
        self.dummy = self.thread.server
        self.client = None


    async def asyncSetUp(self) -> None:
        self.client = await asyncio.wait_for(
            Client.connect(host, self.port), timeout=5
        )

    async def asyncTearDown(self) -> None:
        """
        asyncTearDown
        """
        self.client.close()
        self.thread.stop().result()
        await self.client.wait_closed()
        self.thread.join(timeout=5)

        self.mock_redis.shutdown()
        shutil.rmtree(self.repodir)
        if self.thread.is_alive():
            _log.error('Thread still alive after join with timeout!')

    async def test_request_provision(self):
        """
        Tests if EddMasterController.provision() was called
        """
        self.dummy.provision = mock.AsyncMock()
        await self.client.request("provision", "whatever")
        self.dummy.provision.assert_awaited()

    async def test_request_provision_list(self):
        """
        Checks reply message of EddMasterController.request_provision_list
        """
        reply, __ = await self.client.request("list-provisions")
        self.assertIn("Available provision descriptions", reply[0].decode())

    async def test_request_loadBasicConfig(self):
        """
        Tests if EddMasterController._loadBasicConfig() was called
        """
        self.dummy._loadBasicConfig = mock.AsyncMock()
        await self.client.request("load-basic-config", "whatever")
        self.dummy._loadBasicConfig.assert_awaited()

    async def test_request_deprovision(self):
        """
        Tests if EddMasterController.deprovision() was called
        """
        self.dummy.deprovision = mock.AsyncMock()
        self.dummy.deconfigure = mock.AsyncMock()
        await self.client.request("deprovision")
        self.dummy.deprovision.assert_awaited()


    async def test_request_deprovision_should_call_deconfigure(self):
        """
        Dewprovision should also call deconfigure to ensure stop of streaming components.
        """
        self.dummy.deprovision = mock.AsyncMock()
        self.dummy.deconfigure = mock.AsyncMock()
        self.dummy.state = "ready"
        await self.client.request("deprovision")
        self.dummy.deconfigure.assert_awaited_once()

        # If state is already idle, deconfigure should not be called
        self.dummy.state = "idle"
        await self.client.request("deprovision")
        self.dummy.deconfigure.assert_awaited_once()


    async def test_request_deprovision_should_deprovision_despite_exception_on_configure(self):
        """
        Tests if EddMasterController.deprovision() was called
        """
        self.dummy.deprovision = mock.AsyncMock()
        self.dummy.deconfigure = mock.AsyncMock(side_effect=RuntimeError('Foo'))
        await self.client.request("deprovision")
        self.dummy.deprovision.assert_awaited()



    async def test_request_provision_update(self):
        """
        Tests if EddMasterController.provision_update() was called
        """
        self.dummy.provision_update = mock.AsyncMock()
        await self.client.request("provision-update")
        self.dummy.provision_update.assert_awaited()


class Test_MasterController_Base(unittest.IsolatedAsyncioTestCase):
    """
    All Test_MasterController classes share a common setUp() and tearDown().
    Hence, this class is used as base
    """
    def setUp(self) -> None:
        """
        setUp
        """
        self.repodir = initialize_mock_provision_directory()
        self.port = get_port()

        self.mock_redis = testing.setup_redis()
        self.tmi_server_thread = testing.launch_server_in_thread(TMIServer, '0.0.0.0', get_port(), datastore=EDDDataStore(host, self.mock_redis.server_config['port']))

        self.mastercontroller_thread = testing.launch_server_in_thread(EddMasterController, host, self.port, host, self.mock_redis.server_config['port'], self.repodir, inventory)
        self.server = self.mastercontroller_thread.server


    async def asyncTearDown(self) -> None:
        self.mastercontroller_thread.stop().result()
        self.tmi_server_thread.stop().result()
        self.tmi_server_thread.join(timeout=5)
        self.mock_redis.shutdown()


class Test_MasterController_UpdateProductStatusSummary(unittest.IsolatedAsyncioTestCase):
    """
    Test EddMasterController.updateProductStatusSummary()
    """
    def setUp(self) -> None:
        self.server = EddMasterController('0.0.0.0', 0, 'not_a_valid_host', 6347, '/tmp', 'foo')
        # Mock the ProductController object
        self.server._EddMasterController__controller["ctrl1"] = mock.AsyncMock()
        self.server._EddMasterController__controller["ctrl1"].ping = mock.AsyncMock(return_value=True)
        self.server._EddMasterController__controller["ctrl1"].getState = mock.AsyncMock(return_value="idle")

        self.server._EddMasterController__controller["ctrl2"] = mock.AsyncMock()
        self.server._EddMasterController__controller["ctrl2"].ping = mock.AsyncMock(return_value=True)
        self.server._EddMasterController__controller["ctrl2"].getState = mock.AsyncMock(return_value="error")

    async def test_reachable_product(self):
        """
        Pipeline status of reachable pipelines should be found in the summary.
        """

        await self.server.updateProductStatusSummary()
        status = self.server.sensors['product-status-summary'].value
        expected_status = {"ctrl1": "idle", 'ctrl2': 'error'}
        self.assertEqual(expected_status, json.loads(status))

    async def test_unreachable_product(self):
        """
        An unreachable produt should be marked as unreachable
        """
        async def timeout():
            raise TimeoutError

        self.server._EddMasterController__controller["ctrl1"].ping = mock.AsyncMock(return_value=False)
        self.server._EddMasterController__controller["ctrl1"].getState = timeout
        await self.server.updateProductStatusSummary()
        status = self.server.sensors['product-status-summary'].value
        expected_status = {"ctrl1": "UNREACHABLE", 'ctrl2': 'error'}
        self.assertEqual(expected_status, json.loads(status))

    async def test_unreachable_into_reachable_product(self):
        """
        Pipelines may become reachable again which should be reflected in the summary.
        """

        async def timeout():
            raise TimeoutError
        self.server._EddMasterController__controller["ctrl1"].getState = timeout
        await self.server.updateProductStatusSummary()
        status = self.server.sensors['product-status-summary'].value
        expected_status = {"ctrl1": "UNREACHABLE", 'ctrl2': 'error'}
        self.assertEqual(expected_status, json.loads(status))
        self.server._EddMasterController__controller["ctrl1"].getState = mock.AsyncMock(return_value='idle')
        await self.server.updateProductStatusSummary()
        status = self.server.sensors['product-status-summary'].value
        expected_status = {"ctrl1": "idle", 'ctrl2': 'error'}
        self.assertEqual(expected_status, json.loads(status))


class Test_MasterController_Set(Test_MasterController_Base):
    async def test_set_invalid_json_string(self):
        """
        Should test a raise condition when passing invalid JSON
        """
        with self.assertRaises(FailReply):
            await self.server.set("I'm an invalid JSON")

    async def test_set_valid_json_string(self):
        """
        Should test that the config was correctly updated
        """
        await self.server.set('{"id": "dummy"}')
        self.assertEqual(self.server._config["id"], "dummy")

    async def test_set_already_provisioned(self):
        """
        Should test that the config was correctly updated
            when the MC is already provisioned
        """
        self.server._EddMasterController__provisioned = True
        await self.server.set('{"id": "genius"}')
        self.assertEqual(self.server._config["id"], "genius")


class Test_MasterController_AnsiblePlayExecutor(Test_MasterController_Base):
    async def test_play_ping(self):
        """
        MasterController should execute a non-failing play
        """
        play_ping = {
            'hosts': host,
            'tasks': [{'ping': ''}]
        }
        await self.server._ansible_subplay_executor(play_ping)

    async def test_play_fail(self):
        """
        MasterController should throw on a failing play nd custom error should be contained in message
        """
        play_fail = {
            'hosts': host,
            'tasks': [{'fail': {'msg': 'custom_error'}}]
        }
        with self.assertRaises(ProcessException) as E:
            await self.server._ansible_subplay_executor(play_fail)

        print(E.exception.stdout)
        print(E.exception.stderr)
        msg = get_ansible_fail_message(E.exception.stdout)
        self.assertIn('custom_error', E.exception.stdout)



class Test_MasterController_Configure(Test_MasterController_Base):

    """
    Test EddMasterController.configure()
    """

    async def test_configure_bad_config(self):
        self.server.state = "idle"
        self.server._config = {}
        self.assertEqual(self.server.sensors['configure_counter'].value, 0)
        with self.assertRaises(KeyError):
            await self.server.configure()
        self.assertEqual(self.server.state, "error")
        self.assertEqual(self.server.sensors['configure_counter'].value, 1)

    async def test_configure_no_products(self):
        self.server.state = "idle"
        self.assertEqual(self.server.sensors['configure_counter'].value, 0)
        await self.server.configure()
        self.assertEqual(self.server.state, "configured")
        self.assertEqual(self.server.sensors['configure_counter'].value, 1)


class Test_MasterController_Deconfigure(Test_MasterController_Base):

    """
    Test EddMasterController.deconfigure()
    """
    async def test_notes_flushed(self):
        """
        Should test that notes are flushed
        """
        self.server.add_note("I'm a note")
        await self.server.deconfigure()
        self.assertEqual(self.server.sensors['notes'].value, '')

    async def test_datastreams(self):
        """
        Should test that _dataStreams.flushdb() is called
        """
        self.server.edd_data_store._dataStreams.flushdb = mock.Mock()
        await self.server.deconfigure()
        self.server.edd_data_store._dataStreams.flushdb.assert_called_once()

    async def test_measurement_prepare_coutner_reset(self):
        self.server.sensors['measurement_prepare_counter'].value = 23
        await self.server.deconfigure()
        self.assertEqual(self.server.sensors['measurement_prepare_counter'].value, 0)

class Test_MasterController_Provision(Test_MasterController_Base):
    """
    Test EddMasterController.provision():
    """
    def setUp(self) -> None:
        super().setUp()
        self.server.state = "unprovisioned"

    async def test_available_provision_description(self):
        """
        Should test the state 'idle' after a good provision description was passed
        """
        t0 = datetime.datetime.utcnow().replace()
        # Provision time sensor should be empty and unknown if unprovisioned
        self.assertEqual(self.server.sensors['provision_time'].value, "Unknown")
        self.assertEqual(self.server.sensors['provision_time'].status, self.server.sensors['provision_time'].Status.UNKNOWN)

        self.server._ansible_subplay_executor = mock.AsyncMock()
        self.server._loadBasicConfig = mock.AsyncMock()
        await self.server.provision("example")
        self.assertEqual(self.server.state, "idle")

        t1 = datetime.datetime.utcnow().replace()

        # Value should have changed and be nominal state
        self.assertNotEqual(self.server.sensors['provision_time'].value, "Unknown")
        self.assertEqual(self.server.sensors['provision_time'].status, self.server.sensors['provision_time'].Status.NOMINAL)
        tp = datetime.datetime.fromisoformat(self.server.sensors['provision_time'].value)

        # tp should be in between the state change
        self.assertLessEqual(t0, tp)
        self.assertGreaterEqual(t1, tp)


    async def test_non_available_provision_description(self):
        """
        Failreply should be raised on non existing provision description, but state should remain idle.
        """
        with self.assertRaisesRegex(FailReply, 'IDONOTEXIST'):
            await self.server.provision("IDONOTEXIST")
        self.assertEqual(self.server.state, "idle")

    async def test_bad_subplay(self):
        """
        If a subplay execution fails, the MC should be in error state and a
        FailReply be issued.
        """
        key1 = "PROG_NAME"
        key2 = "STDOUT"
        key3 = "STDERR"
        error = ProcessException(key1, stdout=key2, stderr=key3)

        self.server._ansible_subplay_executor = mock.AsyncMock(side_effect=error)
        self.server._loadBasicConfig = mock.AsyncMock()
        with self.assertRaises(FailReply) as E:
            await self.server.provision("example")

    async def test_bad_yaml_file(self):
        """
        Invalid YAML files should put master controller into an error state
        """
        with open(os.path.join(self.repodir, "provison_descriptions", "bad.yml"), "w") as f:
            f.write('I am not a valid yaml file')

        with open(os.path.join(self.repodir, "provison_descriptions", "bad.json"), "w") as f:
            f.write("I dont matter for this test")

        with self.assertRaises(FailReply):
            await self.server.provision("bad")
        self.assertEqual(self.server.state, "error")


class Test_MasterController_Deprovision(Test_MasterController_Base):
    """
    Test EddMasterController.deprovision():
    """
    async def test_available_provision_description(self):
        """
        Should return to unprovisioned state
        """
        self.server._EddMasterController__provisioned = os.path.join(self.repodir, "provison_descriptions", "example.yml")
        self.server._ansible_subplay_executor = mock.AsyncMock()
        self.server.sensors['provision_time'].set_value("Foo", 3)
        self.server.sensors['configure_counter'].value = 17
        await self.server.deprovision()
        self.assertEqual(self.server.state, "unprovisioned")
        self.assertEqual(self.server._config, self.server._default_config)

        self.assertEqual(self.server.sensors['provision_time'].value, "Unknown")
        self.assertEqual(self.server.sensors['provision_time'].status, self.server.sensors['provision_time'].Status.UNKNOWN)
        self.assertEqual(self.server.sensors['configure_counter'].value, 0)


    async def test_bad_subplay(self):
        """
        Should test the state 'error' and FailReply after a bad sub-play was executed
        """
        self.server._EddMasterController__provisioned = os.path.join(self.repodir, "provison_descriptions", "example.yml")
        self.server._ansible_subplay_executor = mock.AsyncMock(side_effect=RuntimeError)
        with self.assertRaises(FailReply):
            await self.server.deprovision()
        self.assertEqual(self.server.state, "error")


class Test_MasterController_MeasurementPrepare(Test_MasterController_Base):
    """
    Test EddMasterController.measurement_prepare():
    """
    def setUp(self) -> None:
        super().setUp()
        self.server.state = "ready"

        self.server._EddMasterController__controller["ctrl1"] = mock.AsyncMock()
        self.server._EddMasterController__controller["ctrl1"].ping = mock.AsyncMock(return_value=True)
        self.server._EddMasterController__controller["ctrl1"].getState = mock.AsyncMock(return_value="idle")

        self.server._EddMasterController__controller["ctrl2"] = mock.AsyncMock()
        self.server._EddMasterController__controller["ctrl2"].ping = mock.AsyncMock(return_value=True)
        self.server._EddMasterController__controller["ctrl2"].getState = mock.AsyncMock(return_value="idle")

    async def test_state_change(self):
        await self.server.measurement_prepare({})
        self.assertEqual(self.server.state, "set")

    async def test_controlled_pipelines_are_called(self):
        await self.server.measurement_prepare({})
        self.server._EddMasterController__controller["ctrl1"].measurement_prepare.assert_awaited()
        self.server._EddMasterController__controller["ctrl2"].measurement_prepare.assert_awaited()
        self.assertIn('eddid', self.server._EddMasterController__controller["ctrl1"].measurement_prepare.call_args[0][0])
        self.assertIn('eddid', self.server._EddMasterController__controller["ctrl2"].measurement_prepare.call_args[0][0])

    async def test_controlled_pipelines_get_parameters(self):
        await self.server.measurement_prepare({"ctrl1": {"foo": "spam"}, "ctrl2": {"foo": "eggs"}})
        self.assertEqual(self.server._EddMasterController__controller["ctrl1"].measurement_prepare.call_args[0][0]['foo'], 'spam')
        self.assertEqual(self.server._EddMasterController__controller["ctrl2"].measurement_prepare.call_args[0][0]['foo'], 'eggs')

    async def test_controlled_pipelines_glob_match(self):
        await self.server.measurement_prepare({"ctrl?": {"foo": "spam"}})
        self.assertEqual(self.server._EddMasterController__controller["ctrl1"].measurement_prepare.call_args[0][0]['foo'], 'spam')
        self.assertEqual(self.server._EddMasterController__controller["ctrl2"].measurement_prepare.call_args[0][0]['foo'], 'spam')

    async def test_counter(self):
        self.assertEqual(self.server.sensors['measurement_prepare_counter'].value, 0)
        await self.server.measurement_prepare({})
        self.assertEqual(self.server.sensors['measurement_prepare_counter'].value, 1)

    async def test_id_change(self):
        """EDD ID should change after measurement prepare"""
        id0 = self.server.get_edd_id()
        await self.server.measurement_prepare({})
        self.assertNotEqual(id0, self.server.get_edd_id())


class Test_MasterController_MeasurementStop(Test_MasterController_Base):
    """
    Test EddMasterController.measurement_stop():
    """
    def setUp(self) -> None:
        super().setUp()
        self.server.state = "measuring"
        self.mock_context = mock.AsyncMock()

    def add_controller(self, ctrl, ping_reply=True):
        self.server._EddMasterController__controller[ctrl] = mock.AsyncMock()
        self.server._EddMasterController__controller[ctrl].ping = mock.AsyncMock(return_value=ping_reply)
        self.server._EddMasterController__controller[ctrl].getState = mock.AsyncMock(return_value="measuring")
        self.server._EddMasterController__controller[ctrl].measurement_stop = mock.AsyncMock(return_value='{}')


    async def test_state_change(self):
        """
        Request should change state to ready
        """
        self.assertEqual(self.server.state, "measuring")
        await self.server.request_measurement_stop(self.mock_context)
        self.assertEqual(self.server.state, "ready")

    async def test_file_return(self):
        """
        master controller should return a list of file names with id of producer as meta data
        """
        self.add_controller("ctrl1")
        self.server._EddMasterController__controller["ctrl1"].measurement_stop = mock.AsyncMock(return_value='{"foo": {}}')
        res = await self.server.request_measurement_stop(self.mock_context, True)
        ofiles = json.loads(res)
        self.assertEqual(self.server.state, "ready")
        self.assertIn("ctrl1/foo", ofiles)
        self.assertEqual(ofiles["ctrl1/foo"]["pipeline_id"], "ctrl1")




class Test_MasterController_ProvisionUpdate(Test_MasterController_Base):

    """
    Test EddMasterController.provision_update()
    """
    async def test_nonexisting_git_repo(self):
        """
        Should raise an error due to non existing folder
        """
        self.server._EddMasterController__edd_ansible_git_repository_folder = "Idontexist"
        with self.assertRaises(RuntimeError):
            await self.server.provision_update()

    async def test_invalid_git_repo(self):
        """
        Should raise an error on invalid repository
        """
        self.server._EddMasterController__edd_ansible_git_repository_folder = "/tmp/"
        with self.assertRaises(git.exc.InvalidGitRepositoryError):  # pylint: disable=no-member
            await self.server.provision_update()

    async def test_good_repo(self):
        """
        ToDo: Should test correct update of repository
        """
        await self.server.provision_update()


#class Test_MasterController_StateChangeTests(Test_MasterController_Base):
#    def setUp(self) -> None:
#        super().setUp()
#        self.__controller = self.server._EddMasterController__controller
#        for i in range(3):
#            self.__controller[f'ctrl{i}'] = mock.Mock()
#            self.__controller[f'ctrl{i}'].configure = mock.AsyncMock()
#            self.__controller[f'ctrl{i}'].ping = mock.AsyncMock()
#            self.__controller[f'ctrl{i}'].capture_start = mock.AsyncMock()
#            self.__controller[f'ctrl{i}'].capture_stop = mock.AsyncMock()
#            self.__controller[f'ctrl{i}'].measurement_prepare = mock.AsyncMock()
#            self.__controller[f'ctrl{i}'].measurement_start = mock.AsyncMock()
#            self.__controller[f'ctrl{i}'].measurement_stop = mock.AsyncMock()
#            self.__controller[f'ctrl{i}'].deconfigure = mock.AsyncMock()
#            self.__controller[f"ctrl{i}"].getState = mock.AsyncMock(return_value="idle")
#
#    async def test_bad_state(self):
#        """
#        Should test state change function when MC is in not allowed states
#        """
#        with self.assertRaises(FailReply):
#            await self.server.configure()
#        with self.assertRaises(FailReply):
#            await self.server.capture_start()
#        with self.assertRaises(FailReply):
#            await self.server.capture_stop()
#        with self.assertRaises(FailReply):
#            await self.server.measurement_prepare()
#        with self.assertRaises(FailReply):
#            await self.server.measurement_start()
#        with self.assertRaises(FailReply):
#            await self.server.measurement_stop()
#
    # async def test_all_products_configured(self):
    #     """
    #     Should test that MC calls 'configure' of all products
    #     """
    #     self.server._config["products"] = {}
    #     self.server.state = "idle"
    #     await self.server.configure()
    #     for ctrl in self.__controller.values():
    #         self.assertTrue(ctrl.configure.called)
    #     self.assertEqual(self.server.state, "configured")


#    async def test_all_products_capture_started(self):
#        """
#        Should test that MC calls 'capture_start' of all products
#        """
#        self.server.state = "configured"
#        await self.server.capture_start()
#        for ctrl in self.__controller.values():
#            ctrl.capture_start.assert_awaited()
#        self.assertEqual(self.server.state, "ready")
#
#    async def test_all_products_capture_stopped(self):
#        """
#        Should test that MC calls 'capture_stop' of all products
#        """
#        self.server.state = "streaming"
#        await self.server.capture_stop()
#        for ctrl in self.__controller.values():
#            ctrl.capture_stop.assert_awaited()
#        self.assertEqual(self.server.state, "idle")

#    async def test_all_products_measurement_prepared(self):
#        """
#        Should test that MC calls 'measurement_prepare' of all products
#        """
#        self.server.state = "ready"
#        self.tmi_server_thread.server.ensure_update = mock.AsyncMock()
#
#        await self.server.measurement_prepare()
#        for ctrl in self.__controller.values():
#            ctrl.measurement_prepare.assert_awaited()
#        self.assertEqual(self.server.state, "set")
#        self.tmi_server_thread.server.ensure_update.assert_awaited()
#
#    async def test_measurement_prepare_error_on_missing_tmi_information(self):
#        # Fail message should contain hint to TMI
#        self.server.state = "ready"
#        #self.server.edd_data_store.flush()
#        with self.assertRaisesRegex(RuntimeError, 'TMI'):
#            await self.server.measurement_prepare()
#
#    async def test_all_products_measurement_started(self):
#        """
#        Should test that MC calls 'measurement_start' of all products
#        """
#        self.server.state = "set"
#        await self.server.measurement_start()
#        for ctrl in self.__controller.values():
#            ctrl.measurement_start.assert_awaited()
#        self.assertEqual(self.server.state, "measuring")
#
#    async def test_all_products_measurement_stopped(self):
#        """
#        Should test that MC calls 'measurement_stop' of all products
#        """
#        self.server.state = "measuring"
#        await self.server.measurement_stop()
#        for ctrl in self.__controller.values():
#            ctrl.measurement_stop.assert_awaited()
#        self.assertEqual(self.server.state, "ready")
#
#    async def test_all_products_deconfigured(self):
#        """
#        Should test that MC calls 'deconfigure' of all products and correct state
#        """
#        await self.server.deconfigure()
#        for ctrl in self.server._EddMasterController__controller.values():
#            ctrl.deconfigure.assert_awaited()
#        self.assertEqual(self.server.state, "idle")
#


class Test_MasterController_loadBasicConfig(Test_MasterController_Base):
    """
    Test EddMasterController._loadBasicConfig()
    """
    def setUp(self) -> None:
        super().setUp()

        self.thread = testing.launch_server_in_thread(EDDPipeline, host, get_port())
        self.dummy = self.thread.server
        self.dummy._config["ip"] = host # Manually set, since it will be overwritten in EDDPipeline.__init__()

    async def asyncTearDown(self) -> None:
        self.thread.stop().result()
        await super().asyncTearDown()

    async def test_nonexisting_config_file(self):
        """
        Should raise IOError when the config does not exist
        """
        with self.assertRaises(IOError):
            await self.server._loadBasicConfig("idontexisting.json")


    async def test_bad_config_file(self):
        """
        Should raise JSONDecodeError when the config is not a valid JSON
        """
        with tempfile.NamedTemporaryFile("w") as config:
            config.write("Im not a json")
            config.flush()
            with self.assertRaises(json.decoder.JSONDecodeError):
                await self.server._loadBasicConfig(config.name)

    async def test_product_install(self):
        """
        ToDo: Should test a matching config of the pipeline and product config
        """
        cs = f'{{"products":{{"{self.dummy._config["id"]}":{json.dumps(self.dummy._config)}}}}}'
        with tempfile.NamedTemporaryFile("w") as config_file:
            config_file.write(cs)
            config_file.flush()
            await self.server._loadBasicConfig(config_file.name)
        # self.dummy._config["datastore"] = host
        # print(self.dummy._config)
        # self.assertEqual(
        #     self.server._config["products"][self.dummy._config["id"]], self.dummy._config
        # )


class Test_MasterController_installController(Test_MasterController_Base):
    """
    Test EddMasterController._installControllerByDataStore()
    """
    def setUp(self) -> None:
        super().setUp()
        self.server._config["id"] = "its_me"  # Set the ID of the MasterController

    async def test_only_install_by_datastore(self):
        """
        Only EddMasterController._installControllerByDataStore should be called
        """
        self.server._installControllerByDataStore = mock.AsyncMock()
        self.server._installControllerByConfig = mock.AsyncMock()
        await self.server._installController()
        self.server._installControllerByDataStore.assert_awaited()
        self.server._installControllerByConfig.assert_not_awaited()

    async def test_install_by_datastore_and_config(self):
        """
        EddMasterController._installControllerByDataStore and
            EddMasterController._installControllerByConfig should be called
        """
        self.server._installControllerByDataStore = mock.AsyncMock()
        self.server._installControllerByConfig = mock.AsyncMock()
        await self.server._installController(config={"call": "me"})
        self.server._installControllerByDataStore.assert_awaited()
        self.server._installControllerByConfig.assert_awaited()


class Test_MasterController_InstallControllerByDataStore(Test_MasterController_Base):
    """
    Test EddMasterController._installControllerByDataStore()
    """
    def setUp(self) -> None:
        """
        setUp
        """
        super().setUp()
        self.server._config["id"] = "its_me"  # Set the ID of the MasterController


    async def test_no_products(self):
        """
        Should test an empty dict since no existing products in data store
        """
        await self.server._installControllerByDataStore()
        self.assertEqual(self.server._EddMasterController__controller, {})

    async def test_only_mastercontroller(self):
        """
        Should test an empty dict since only MC is registered
        """
        self.server.edd_data_store = mock.Mock(products=[{"id":"its_me"}])
        await self.server._installControllerByDataStore()
        self.assertEqual(self.server._EddMasterController__controller, {})

    async def test_with_reachable_product(self):
        """
        Should test registered and reachable product
        """
        port = get_port()
        # We need a running pipeline server / eventually mock.patch the ProductController
        server_thread = testing.launch_server_in_thread(EDDPipeline, host, port)
        test_server = server_thread.server
        self.server.edd_data_store = mock.Mock(
            products=[
                {"id":"its_me"},  # Master Controller
                {
                    "id": test_server._config["id"],
                    "ip": host,   # ToDo: The EddPipeline overwrites the actual IP with socket.gethostname()
                    "port": port
                }
            ]
        )
        await self.server._installControllerByDataStore()
        self.assertIn(test_server._config["id"], self.server._EddMasterController__controller)
        self.assertEqual(len(self.server._EddMasterController__controller), 1)
        server_thread.stop().result()
        server_thread.join(timeout=5)
        #server_thread.stop(),result() # Ensure pipeline is not running for test 4

    async def test_with_unreachable_product(self):
        """
        Should test the remove of an unreachable product
        """

        self.server.edd_data_store = mock.Mock(
            products=[
                {"id": "its_me"},  # Master Controller
                {
                    "id": "not pingable",
                    "ip": "any",
                    "port": get_port()
                }
            ]
        )
        await self.server._installControllerByDataStore()
        self.server.edd_data_store.removeProduct.assert_called_once()


class Test_MasterController_InstallControllerByConfig(Test_MasterController_Base):
    """
    Test EddMasterController._installControllerByConfig()
    """
    async def test_empty_dict(self):
        """
        No controller should be installed on an empty dict
        """
        await self.server._installControllerByConfig({})
        self.assertEqual(self.server._EddMasterController__controller, {})

    async def test_existing_controller(self):
        """
        Controller should not be installed twice
        """

        # Caution: We modify the internal variables here from a different
        # thread. E.g. the status update is running in background and accessing
        # the controller, which may cause random crashes!
        self.server._EddMasterController__controller["ctrl"] = mock.Mock()
        self.server._EddMasterController__controller["ctrl"].ping = mock.AsyncMock(return_value=True)
        self.assertEqual(len(self.server._EddMasterController__controller), 1)
        await self.server._installControllerByConfig({"product": {"id": "ctrl"}})
        self.assertEqual(len(self.server._EddMasterController__controller), 1)

    async def test_nonexisting_controller_without_ip(self):
        """
        Should raise a RuntimeError when product dict does not contain an 'ip'
        """
        with self.assertRaises(RuntimeError):
            await self.server._installControllerByConfig({"product": {"id": "ctrl", "port": 123}})

    async def test_nonexisting_controller_without_port(self):
        """
        Should raise a RuntimeError when product dict does not contain a 'port'
        """
        with self.assertRaises(RuntimeError):
            await self.server._installControllerByConfig({"product": {"id": "ctrl", "ip": 'foo'}})


    async def test_nonexisting_controller_without_id(self):
        """
        Should raise a KeyError when product dict does not contain an 'id'
        """
        with self.assertRaises(KeyError):
            await self.server._installControllerByConfig({"product": {"any": "any"}})

    async def test_nonexisting_controller_unreachable(self):
        """
        Should raise a RuntimeError because the product is unreachable
        """
        with self.assertRaises(RuntimeError):
            await self.server._installControllerByConfig({"product": {
                "id": "ctrl",
                "ip": host,
                "port": get_port()
            }})

    async def test_nonexisting_controller_reachable(self):
        """
        A new controller should be added for a reachable server
        """
        # We need a running pipeline server / eventually mock.patch the ProductController
        port = get_port()
        server_thread = testing.launch_server_in_thread(EDDPipeline, host, port)
        self.assertEqual(len(self.server._EddMasterController__controller), 0)
        await self.server._installControllerByConfig({"product": {
            "id": "ctrl",
            "ip": host,
            "port": port
        }})
        server_thread.stop().result()
        self.assertEqual(len(self.server._EddMasterController__controller), 1)


class TestAutoAssignmentOfMulticastDestinations(unittest.TestCase):
    def test_non_modification_of_non_stream_values(self):
        # Also output stream without IP should not crash
        cfg_in = {'foo': 'bar',
                  'products': {"p1": {'foo': 'bar', "output_data_streams": {"foo":{}}},
                               "p2": {'spam': 'eggs', "output_data_streams": {"foo": {"ip": "127.0.0.1"}}}}}
        cfg_out = EddMasterController.auto_assign_ip_addresses_to_outputs_streams(cfg_in, ip_manager.IPManager('239.32.0.0/16'))
        self.assertEqual(cfg_out, cfg_in)

    def test_outputdatastream_list(self):
        # output data stream can be dict OR lsit, neither shoudl crash
        cfg_in = {'foo': 'bar',
                  'products': {"p1": {'foo': 'bar', "output_data_streams": [{}]},
                               "p2": {'spam': 'eggs', "output_data_streams": [{"ip": "127.0.0.1"}]}}}
        cfg_out = EddMasterController.auto_assign_ip_addresses_to_outputs_streams(cfg_in, ip_manager.IPManager('239.32.0.0/16'))
        self.assertEqual(cfg_out, cfg_in)



    def test_setting_of_auto_ips(self):
        cfg_in = {'products': {'p1':{"output_data_streams": {"foo": {"ip": "auto+15"}, "bar": {"ip": "auto+3"}, "spam": {"ip": "auto+3"} }}}}
        cfg_out = EddMasterController.auto_assign_ip_addresses_to_outputs_streams(cfg_in, ip_manager.IPManager('239.32.0.0/16'))

        ranges = set()
        # Every output stream should have a valid multicast IP
        for o in cfg_out['products']['p1']['output_data_streams'].values():
            ranges.add(o['ip'])
            self.assertTrue(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring(o['ip'])))

        # The IPs should be unique
        self.assertEqual(len(ranges), len(cfg_out['products']['p1']['output_data_streams']))


    def test_full_json(self):
        cfg_in = json.loads('{"products":[{"id":"dig_pack_controller","bit_depth":8,"sampling_rate":3200000000.0,"predecimation_factor":4,"flip_spectrum":false,"noise_diode_pattern":{"percentage":0.5,"period":0.01},"output_data_streams":{"polarization_0":{"format":"MPIFR_EDD_Packetizer:1"},"polarization_1":{"format":"MPIFR_EDD_Packetizer:1"}}},{"id":"gated_spectrometer","input_data_streams":[{"source":"dig_pack_controller:polarization_0","format":"MPIFR_EDD_Packetizer:1"},{"source":"dig_pack_controller:polarization_1","format":"MPIFR_EDD_Packetizer:1"}],"output_data_streams":{"polarization_0_0":{"format":"GatedSpectrometer:1","ip":"auto","port":"7152"},"polarization_0_1":{"format":"GatedSpectrometer:1","ip":"auto","port":"7152"},"polarization_1_0":{"format":"GatedSpectrometer:1","ip":"auto","port":"7152"},"polarization_1_1":{"format":"GatedSpectrometer:1","ip":"auto","port":"7152"}},"naccumulate":16384,"fft_length":262144},{"id":"fits_interface","keep_socket_alive":true,"input_data_streams":[{"source":"gated_spectrometer:polarization_0_0","format":"GatedSpectrometer:1"},{"source":"gated_spectrometer:polarization_0_1","format":"GatedSpectrometer:1"},{"source":"gated_spectrometer:polarization_1_0","format":"GatedSpectrometer:1"},{"source":"gated_spectrometer:polarization_1_1","format":"GatedSpectrometer:1"}]}]}')
        cfg_out = EddMasterController.auto_assign_ip_addresses_to_outputs_streams(EddMasterController._sanitizeConfig(cfg_in), ip_manager.IPManager('239.32.0.0/16'))

        for o in cfg_out['products']['gated_spectrometer']['output_data_streams'].values():
            self.assertTrue(ip_utils.is_valid_multicast_range(*ip_utils.split_ipstring(o['ip'])))

class TestAnsibleOuputParsing(unittest.TestCase):
    def test_invalid_json(self):
        """Invalid json should result in an unknown error message"""
        msg = get_ansible_fail_message('{gf')
        self.assertEqual(msg, 'unknown error')

    def test_custom_fail(self):
        """If a host in the last play failed, the custom error should be returned"""
        msg = get_ansible_fail_message({'plays':[{'tasks':[{'foo'}, {'hosts': {'A': {"failed": True, "msg": "custom_error"}}}]}]})
        self.assertIn("custom_error",  msg)



class TestGatherAndThrow(unittest.IsolatedAsyncioTestCase):
    async def test_all_tasks_working(self):
        async def t1():
            return 'A'
        async def t2():
            return 'B'

        res = await gather_and_throw([asyncio.create_task(t1()), asyncio.create_task(t2())])
        self.assertIn('A', res)
        self.assertIn('B', res)

    async def test_task_fail(self):
        async def t1():
            return 'A'
        async def t2():
            raise RuntimeError('foo')
            return 'B'

        with self.assertRaises(RuntimeError):
            res = await gather_and_throw([asyncio.create_task(t1()), asyncio.create_task(t2())])


class Test_AnsibleInventory(unittest.IsolatedAsyncioTestCase):

    def setUp(self) -> None:
        self.inventory_dir = "/tmp/ansible_inventory/"
        self.groupvars_dir = self.inventory_dir + "group_vars/"
        host = {"all":{}}
        os.makedirs(self.inventory_dir, exist_ok=True)
        os.makedirs(self.groupvars_dir, exist_ok=True)
        with open(self.inventory_dir + "hosts", "w") as f:
            yaml.dump(host, f)
        return super().setUp()

    def tearDown(self) -> None:
        shutil.rmtree(self.inventory_dir)
        return super().tearDown()

    async def test_get_ansible_inventory(self):
        group_vars = {
            "edd_inventory_folder": "effelsberg_devel",
            "docker_registry": "eddinfra0",
            "docker_registry_port": 5000,
            "docker_registry_webui_port": 8083,
            "dummy_list":[0,1,2,3,4,"cheese"]
        }
        inventory_fname = self.groupvars_dir + "all.yml"
        with open(inventory_fname, "w") as f:
            yaml.dump(group_vars, f)
        inventory = await get_ansible_inventory(self.inventory_dir)
        for key, val in group_vars.items():
            self.assertEqual(inventory[key], val)


class TestConfigGlob(unittest.TestCase):
    def setUp(self):
        self.cfg = {
                'search1': {},
                'search2': {},
                'foo': {}
                }

    def test_update_of_all(self):
        c = config_update(self.cfg, {"*": {'foo': 'bar'}})

        # updated config should contain all keys from input and all should
        # contain the update value 
        for k in self.cfg:
            self.assertEqual(c[k]['foo'], 'bar')

        # input should not be modified
        self.assertNotIn('foo', self.cfg['foo'])

    def test_update_of_pattern_only(self):
        c = config_update(self.cfg, {"search?": {'foo': 'bar'}})

        self.assertEqual(c['search1']['foo'], 'bar')
        self.assertEqual(c['search2']['foo'], 'bar')
        self.assertNotIn('foo', c['foo'])

    def test_update_of_single_only(self):

        c = config_update(self.cfg, {"search1": {'foo': 'bar'}})
        self.assertEqual(c['search1']['foo'], 'bar')
        self.assertNotIn('foo', c['search2'])
        self.assertNotIn('foo', c['foo'])



if __name__ == '__main__':
    unittest.main()
