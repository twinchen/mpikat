import mpikat.core.logger
from mpikat.utils.process_tools import ManagedProcess

_log = mpikat.core.logger.getLogger("mpikat.utils.dada_tools.reader")

class DadaClient():
    """
    is the base class for clients (read or write). The CLIs are executed by an
    instance of the ManagedProcess-class (see mpikat/utils/process_tools.py)

    Methods:
        start()
        stop()

    Attributes:
        key (str): The DADA key
        stdout_handler (callable): Handler for parsing stdandard outputs
        proc (ManagedProcess): The running process
        cmd (str): The command to run

    """
    def __init__(self, key, stdout_handler=None, physcpu=None):
        """Base class for reading and writing clients

        Parameters:
            key (str): The hex key used for the DADA buffer
            stdout_handler (callable): A function that parses the standard output
                of the client
            physcpu (str): A string containing the IDs of the used CPU cores
                (e.g. '1,2,3')

        Returns:
            DadaClient: An instance of the the DadaClient class.

        """
        self.key = key
        self._stdout_handler = stdout_handler
        self._proc = None
        self._physcpu: str = physcpu
        self.cmd = ""
        if physcpu:
            self.cmd += f"taskset -c {self._physcpu} "

    @property
    def proc(self):
        return self._proc

    def is_alive(self) -> bool:
        if self._proc is not None:
            return self._proc.is_alive()
        return False

    def start(self, env: dict={}):
        """Starts the client process"""
        if self._proc is None:
            self._proc = ManagedProcess(self.cmd, stdout_handler=self._stdout_handler, env=env)
        return self._proc

    def stop(self):
        """Stops the client process"""
        if isinstance(self._proc, ManagedProcess) and self._proc.is_alive():
            self._proc.terminate()
        self._proc = None

    def stdout_handler(self, handle: callable):
        if self._proc is None:
            self._stdout_handler = handle; return
        _log.warning("Process already set")


class DbNull(DadaClient):
    """
    is a read client which writes the data from the ringbuffer to /dev/null
    """
    def __init__(self, key, zerocopy=False, daemon=False,
                 stdout_handler=None, physcpu=None):
        """is a read client which writes the data from the ringbuffer to
            /dev/null

        Parameters
            key (str): see DadaClient
            zerocopy (bool): use zero copy direct shm access
            daemon (bool): Run as daemon process
            physcpu (str): see DadaClient
            stdout_handler (callable): see DadaClient

        Returns:
            DbNull: An instance of the the DbNull class.

        """
        DadaClient.__init__(self, key, stdout_handler, physcpu)
        self.cmd += "dada_dbnull -k {}".format(key)
        if zerocopy:
            self.cmd += " -z "
        if daemon:
            self.cmd += " -d"


class DbDisk(DadaClient):
    """
    is a read client which writes the data from the ringbuffer to a directory
    """
    def __init__(self, key, directory="/tmp/", size=False, bypass=False,
                 overwrite=True, zerocopy=False, daemon=False,
                 stdout_handler=None, physcpu=None):
        """is a read client which writes the data from the ringbuffer to a
            directory
        Parameters
            key (str): see DadaClient
            directory (str): The directory to write to
            bytes (int): set optimal bytes for writing
            bypass (bool): use O_DIRECT flag to bypass kernel buffering
            overwrite (bool): Overwrite files with the same name in the same
                location
            zerocopy (bool): use zero copy transfer
            physcpu (str): see DadaClient
            stdout_handler (callable): see DadaClient


        Returns:
            DbNull: An instance of the the DbNull class.

        """
        DadaClient.__init__(self, key, stdout_handler, physcpu)
        self.directory = directory
        self.cmd += "dada_dbdisk -k {}".format(key)
        self.cmd += " -D {}".format(self.directory)
        if bypass:
            self.cmd += " -o "
        if overwrite:
            self.cmd += " -W "
        if size:
            self.cmd += " -t {}".format(size)
        if zerocopy:
            self.cmd += " -z "
        if daemon:
            self.cmd += " -d "


class DbSplitDb(DadaClient):
    """
    is a read and write client, which connects one input buffer to multiple
    output buffers. However, the DbSplitDb has no wait()-function as it is
    reading from another DADA source and will not exit automatically.
    IN     OUT
     | ---> O
     | ---> O
     O
     | ---> O
     | ---> O
     """
    def __init__(self, in_key, out_keys, chunksize=0, stdout_handler=None,
                 physcpu=None):
        """is a read and write client, which connects one input buffer to
            multiple output buffers.

        Parameters:
            in_key (str): The key of the input buffer which is splitted
            out_key (str): A string containing all output dada keys to write.
                (e.g. 'dadc,dade,dae0,dae2')
            chunksize (int): The size of the chunks to copy from input to an
                output buffer
            physcpu (str): see DadaClient
            stdout_handler (callable): see DadaClient

        Returns:
            DbNull: An instance of the the DbNull class.
        """
        DadaClient.__init__(self, in_key, stdout_handler, physcpu)
        self.out_keys = out_keys
        self.chunksize = int(chunksize)
        self.cmd += "dbsplit -i {}".format(self.key)
        self.cmd += " -o " + ",".join(self.out_keys)
        self.cmd += " -c {}".format(self.chunksize)
