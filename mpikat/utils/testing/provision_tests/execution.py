import asyncio
import subprocess
import json
import time
import redis

import aiokatcp

from mpikat.core import logger

from . registry import _registry
from . common import EDDTestContext, StageSummary, EDDTestResult, TestState, Stage
from . default_tests import register_default_tests

_log = logger.getLogger('mpikat.utils.testing.provision_tests.execution')


async def execute_stage(stage, stage_parameters, connections):
    """
    Command a stage
    """
    _log.debug('Executing stage %s', stage)
    stage_params = stage_parameters.get(stage, None)
    response = None
    try:
        if stage_params:
            response, __ = await connections['master_controller'].request(stage.name.replace('_', '-'), stage_params)
        else:
            response, __ = await connections['master_controller'].request(stage.name.replace('_', '-'))
    except aiokatcp.FailReply as E:
        _log.error('Execution of stage %s failed', stage)
        #raise RuntimeError from E
    if response:
        response = response[0].decode('ascii')
        try:
            response = json.loads(response)
        except json.JSONDecodeError:
            _log.warning("Stage response could not be decoded to json object")
    else:
        response = "OK"
    _log.info("Executed %s with params %s, got response %s", stage.name, stage_params, response)
    return response

async def execute_stage_tests(stage, connections, registry, stage_response=None):
    """
    Runs all tests for a stage
    """
    _log.debug('Executing test for stage %s', stage)
    # Get test result
    summary = StageSummary(stage, pipelines=list(connections.keys()))
    starttime = time.time()

    results = await asyncio.gather(*[run_test(test,
        EDDTestContext(master_controller=connections['master_controller'],
                       pipeline=connections[test.pipeline_name],
                       status_server=connections['status_server'],
                       stage_response=stage_response)
        ) for test in registry.get_tests_for_stage(stage)])
    for r in results:
        summary.add(r)


    summary.duration = time.time() - starttime

    return summary

#
# Test Runner
#

async def run_test(test, context):
    """
    Run a single test and return the result.
    """
    _log.debug('Run test %s', test.name)
    starttime = time.time()
    result = EDDTestResult(test.name, test.pipeline_name)

    try:
        await test.func(context)
    except AssertionError as E:
        _log.debug('Test failed by assertion: %s', test.name)
        result.state = TestState.FAIL
        result.output.append(str(E))
    except Exception as E:  # pylint: disable=broad-except
        _log.debug('Test failed by exception: %s', test.name)
        result.state = TestState.ERROR
        _log.debug('Exception during test: %s', test.name)
        result.output.append(str(E))
    else:
        if test.tbc:
            result.state = TestState.TBC
        else:
            result.state = TestState.OK

    result.output.extend(context.output)
    result.duration = time.time() - starttime
    return result



def load_inventory(inventory):
    """
    Load relevant data from an inventory
    """
    _log.debug('Loading inventory %s', inventory)
    data = json.loads(subprocess.check_output(f'ansible-inventory -i {inventory} --list'.split(), stderr=subprocess.PIPE))
    mc_host = data['mastercontroller']['hosts'][0]
    mc_port = data['_meta']['hostvars'][mc_host]['master_controller_port']
    redis_host = data['redis']['hosts'][0]
    redis_port = data['_meta']['hostvars'][redis_host]['redis_port']
    telescope_meta = redis.StrictRedis(db=4, host=redis_host, port=redis_port)
    telescope_meta_data_server_string = telescope_meta.hgetall("TMIServer")[b"value"].decode().split(":")
    telescope_meta_host = telescope_meta_data_server_string[0]
    telescope_meta_port = telescope_meta_data_server_string[1]
    version_tag = data['_meta']['hostvars'][mc_host]['version_tag']
    edd_inventory_folder = data['_meta']['hostvars'][mc_host]['edd_inventory_folder']

    return {'mastercontroller': {'ip': f'{mc_host}', 'port': f'{mc_port}'}, 'status_server': {'ip': f'{telescope_meta_host}', 'port': f'{telescope_meta_port}'},'version_tag': f'{version_tag}', 'edd_inventory_folder': f'{edd_inventory_folder}'}





async def run_tests(output, inventory_data, stage_parameters, registry=_registry, run_default_tests=True):
    """
    run all tests and process the output their output.

    Args:
        output:           Output processor to be used
        inventory_data:   Inventory to be used for the test
        stage_parameters: parameters to be used for stage commands
        registry:         registry to be used (default: singleton)
    """
    print('Start running tests ...')
    starttime = time.time()

    connections = {'master_controller': await aiokatcp.Client.connect(inventory_data['mastercontroller']['ip'],
                                                                      inventory_data['mastercontroller']['port'])}

    if Stage.configure not in stage_parameters:
        stage_parameters[Stage.configure] = '{}'

    await connections['master_controller'].request('deprovision')
    await execute_stage(Stage.provision, stage_parameters, connections)

    _, informs = await connections['master_controller'].request('sensor-value', 'current-config')
    provision_config = json.loads(informs[0].arguments[4].decode())

    registry.add_class_tests(provision_config['products'])
    if run_default_tests:
        register_default_tests(provision_config['products'].keys(), registry)

    # Construct connections to all products
    keys = []
    tasks = []
    for k, i in provision_config['products'].items():
        keys.append(k)
        tasks.append(aiokatcp.Client.connect(i['ip'], i['port']))
    results = await asyncio.gather(*tasks)
    connections.update(dict(zip(keys, results)))

    status_server_connection = await aiokatcp.Client.connect(inventory_data['status_server']['ip'],
                                                             inventory_data['status_server']['port'])
    connections.update({'status_server': status_server_connection})

    # run tests only now as connections not available otherwise
    stage_summary = await execute_stage_tests(Stage.provision, connections, registry)
    stage_summary.duration = time.time() - starttime

    _, informs = await connections['master_controller'].request('sensor-value', 'configuration_graph')
    configuration_graph = informs[0].arguments[4].decode('ascii')

    # Construct connections to all products
    keys = []
    tasks = []
    for k in provision_config['products']:
        keys.append(k)
        tasks.append(connections[k].request('sensor-value', 'pipeline-version'))
    results = [r[0].arguments[4].decode('ascii') for _, r in await asyncio.gather(*tasks)]
    versions = dict(zip(keys, results))


    run_summary = {'inventory_data': inventory_data, 'stage_parameters': stage_parameters, 'stage_results': [], 'configuration_graph': configuration_graph, 'pipeline_versions': versions}
    run_summary['stage_results'].append(stage_summary)
    # Run all stages ...
    for stage in Stage:
        stage_starttime = time.time()
        if stage is Stage.provision:
            continue
        stage_res = await execute_stage(stage, stage_parameters, connections)
        stage_summary = await execute_stage_tests(stage, connections, registry, stage_res)
        stage_summary.duration = time.time() - stage_starttime
        run_summary['stage_results'].append(stage_summary)

    run_summary['duration'] = time.time() - starttime

    print(f'Finished running tests. Total execution time {run_summary["duration"]:.1}s')
    output.run(run_summary)
    return run_summary


