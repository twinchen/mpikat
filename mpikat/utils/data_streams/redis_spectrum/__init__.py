from mpikat.core.data_stream import DataStream, DataStreamRegistry

class RedisSpectrumStreamFormat(DataStream):
    """
    Format description oof Redis Spectrum stream
    """

    stream_meta = {
        "type": "RedisSpectrumStream:1",
        "ip": "",
        "port": "",
        "description": "Redis json stream of integrated spectra.",
        "central_freq": "",
        "receiver_id": "",
        "band_flip": ""
    }

    data_items = {
        "timestamp":0,
        "interval":0,
        "f_min":0,
        "f_max":0,
        "data":"",
    }

DataStreamRegistry.register(RedisSpectrumStreamFormat)