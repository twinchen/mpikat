from mpikat.utils.testing.provision_tests.common import TestState

from . OutputProcessor import OutputProcessor

class TerminalOutput(OutputProcessor):
    """
    Output test results to terminal
    """
    class bcolors:  # pylint: disable=too-few-public-methods
        """
        Definitions of colors for terminal output.
        From https://stackoverflow.com/questions/287871/how-do-i-print-colored-text-to-the-terminal
        """
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKCYAN = '\033[96m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'

    colors = {
        TestState.NOT_EXECUTED: bcolors.WARNING,
        TestState.OK:  bcolors.OKGREEN,
        TestState.FAIL: bcolors.FAIL,
        TestState.ERROR: bcolors.FAIL,
        TestState.TBC: bcolors.OKBLUE,
             }

    def print_head(self, summary):
        pass

    def print_test_result(self, test):

        ress = f'{test.state.name:<14}'
        result = f"{self.colors[test.state]}{ress}{self.bcolors.ENDC}"
        print(f"  {result}  {test.name:60} {test.duration:.1f}s")

    def print_stage(self, summary):
        print(f"Test results after {summary.stage.name:<20} - {summary.stats[TestState.FAIL] + summary.stats[TestState.ERROR]} / {summary.n_tests} failed. {summary.stats[TestState.TBC]} require manual check.")

        for pipeline in summary.results:
            print(f'Tests for {pipeline} [{len(summary.results[pipeline])}]')
            for test in summary.results[pipeline]:
                self.print_test_result(test)

        print(f"\n........................................................ Duration of stage: {summary.duration:5.1f} s\n")
