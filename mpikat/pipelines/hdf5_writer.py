"""
The HDF5 Writer serializes data streams to disk in HDF5 format.

Data is stored in HDF5 group `scan/XXX` of the corresponding file with
consecutive numbers XXX starting from 000. Every measurement prepare creates a
new scan if no new file is opened. The file is closed and flushed on every
measurement stop. Version of mpikat and the current date is written into a dataset history of the file.


Configuration Settings
----------------------
use_date_based_subfolders (boolean)
  If True (default), output files will be stored in a sub-folder structure
  YYYY/MM/DD corresponding to the current date to limit the number of files in
  one directory. For large number of measurements this reduces the pain of
  handling the files e.g. using ls command.

input_data_streams
  List of input data stream definitions. Each input data stream will be
  serialized to disk according to the specified format.


Measurement prepare
-------------------
new_file (boolean)
    If true, a new file is created. If false, data is appended as new scan to the previously opened file.

filename (str, None)
   Name of the file to write to. If None, a unique filename will be created automatically

file_id (str)
   A string that will be included in automatic file name generation


Data Formats
------------
For every data format, a dedicated handler is required. Currently, only gated
spectrometer data format is supported.



"""
#Requires python 3.7+ and spead 3+

import asyncio
import copy
import json
import multiprocessing
import os
import time
import numpy as np

from aiokatcp.sensor import Sensor

from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
from mpikat.core.hdf5_file_writer import WriterProcess
from mpikat.core import logger

import mpikat.utils.async_util # pylint: disable=unused-import
from mpikat.pipelines.gated_spectrometer import Plotter, GatedSpectrometerSpeadHandler
from mpikat.utils.spead_capture import SpeadCapture, SpeadPacket
from mpikat.utils.sensor_watchdog import conditional_update
from mpikat.utils import numa, output

_log = logger.getLogger("mpikat.pipelines.hdf5_writer")

_DEFAULT_CONFIG = {
    "id":"hdf5_writer",
    "type":"hdf5_writer",
    "input_data_streams": [
        {
            "source": "gated_spectrometer_0:polarization_0_0",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.173",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_0:polarization_0_1",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.172",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_0",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.175",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_1",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.174",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_0:polarization_0_0",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.176",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_0:polarization_0_1",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.177",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_0",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.178",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_1",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.179",
            "port": "7152"
        }
    ],
    "enable_plotting":True,
    "chunksize":'auto',
    "default_hdf5_group_prefix":"S",
    "spead_config":copy.deepcopy(SpeadCapture.DEFAULT_CONFIG),
    "output_directory":"/mnt",
    "use_date_based_subfolders":True,
    "measurement_prepare_default": {
        "new_file": False,                      # If true, a new file is created, otherwise data is appended as new group to existing file
        "filename": None,                       # Filename to be used. If None, a filename will be created automatically
        'file_id': None                         # Custom ID to be included in automatically created file. random string is used if None.
    }
}

# ToDo: prepare_heap() is a unnecssary funtion and should be deleted.
#       Instead the Hdf5FileWriter should accept the SpeadPacket-format

def prepare_heap(heap: SpeadPacket) -> tuple:
    """Prepares a heap so that it can be added as Snapshot to a Plotter

    Args:
        heap (_type_): _description_

    Returns:
        tuple: _description_
    """
    data = {}
    data['timestamp'] = np.array([heap.reference_time])

    data['integration_time'] = np.array([heap.number_of_input_samples / heap.sampling_rate])
    if heap.type == 1:
        # For stokes divide the inegration time by a factor of 2 as the number
        # of input samples is doubled as two polarizations are combined
        data['integration_time'] /= 2
    data['saturated_samples'] = np.array([heap.number_of_saturated_samples])
    data['spectrum'] = heap.data
    streamtype_prefixes = {0: 'P', 1: 'S'}
    section_id = f"{streamtype_prefixes[heap.type]}{heap.polarization}_ND{heap.noise_diode_status}"
    _log.trace("Set section_id: {}".format(section_id))
    return data, section_id


class HDF5Writer(EDDPipeline):
    """
    Write EDD Data Streams to HDF5 Files.
    """
    def __init__(self, ip, port, loop=None):
        """
        Args:
            ip:   IP accepting katcp control connections from.
            port: Port number to serve on
        """
        super().__init__(ip, port, default_config=_DEFAULT_CONFIG, loop=loop)

        self._capture_thread = None
        self._writer_process = None
        self._writer_stop_event = None
        self._writer_queue = None
        self.data_plotter = Plotter()

    def setup_sensors(self):
        """
        Setup monitoring sensors
        """
        super().setup_sensors()

        self._spectra_written = Sensor(int,
            "written-packages",
            description="Number of spectra written to file.",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._spectra_written)

        self._incomplete_heaps = Sensor(int,
            "incomplete-heaps",
            description="Incomplete heaps received.",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._incomplete_heaps)

        self._complete_heaps = Sensor(int,
            "complete-heaps",
            description="Complete heaps received.",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._complete_heaps)

        self._current_file = Sensor(str,
            "current-file",
            default="",
            description="Current filename.",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._current_file)

        self._current_file_size = Sensor(float,
            "current-file-size",
            description="Current filesize.",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._current_file_size)

        self._current_filesystem_size = Sensor(int,
            "current-filesystem-size",
            description="Size of the file system used for data output [bytes].",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._current_filesystem_size)
        self._current_filesystem_avail = Sensor(int,
            "current-filesystem-avail",
            description="Size available on the file system used for data output [bytes].",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._current_filesystem_avail)

        self._bandpass = Sensor(str,
            "bandpass",
            description="band-pass data (base64 encoded)",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._bandpass)

        self._writer_queue_size = Sensor(int,
            "write_queue_size",
            description="number of items in the write queue",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._writer_queue_size)

    # --------------------- #
    # State change routines #
    # --------------------- #
    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """Configures the HDF5Writer pipeline

        - Retrieves the address of the network interface card
        - Instantiates a Plotter object
        """
        _log.info("Configuring HDF5 Writer")

        nic_name, nic_description = numa.getFastestNic()
        _log.info("Capturing on interface {}, ip: {}, speed: {} Mbit/s".format(nic_name, nic_description['ip'], nic_description['speed']))
        self._config['spead_config']['interface_address'] = nic_description['ip']
        self._config['spead_config']['numa_affinity'] = numa.getInfo()[nic_description['node']]['cores']

        _log.info("Final configuration:\n%s", json.dumps(self._config, indent=4))
        if self._config["enable_plotting"]:
            self._plot_task = asyncio.create_task(self.data_plotter.consume_plots(self._bandpass))


    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        """Starts the capturing of data
        """
        _log.info("Starting capture")

        spead_handler = GatedSpectrometerSpeadHandler()

        self._capture_thread = SpeadCapture(self._config["input_data_streams"],
            spead_handler,
            self._package_writer,
            self._config['spead_config'])

        self._capture_thread.start()
        _log.debug("Done capture starting!")



    @state_change(target="set", allowed=["ready"], intermediate="measurement_preparing")
    async def measurement_prepare(self, config: dict):
        """prepares the measurement

        - Creates the storage directories
        - Set up the writing process

        Args:
            config (dict): configuration for the measurement_prepare
        """
        if ("new_file" in config and config["new_file"]) or (not self._current_file.value):
            _log.debug("Creating new file")

            if self._config["use_date_based_subfolders"]:
                _log.debug("Using date based subfolders:")
                path = output.create_datebased_subdirs(self._config["output_directory"])
            else:
                _log.debug("Using flat file hirarchy:")
                path = self._config["output_directory"]

            filename = config.get('filename')
            if not filename:
                filename = output.create_file_name(path=path, suffix='hdf5', file_id_no=config.get('file_id'))

            self._current_file.set_value(filename)
            self.add_output_file(filename)
        if "attributes" in config:
            _log.warning("Attributes specified, but currently not supported!")

        self._writer_stop_event = multiprocessing.Event()
        self._writer_queue = multiprocessing.JoinableQueue()

        self._writer_process = multiprocessing.Process(target=WriterProcess,
            args=(self._current_file.value,
                  self._writer_stop_event,
                  self._writer_queue,
                #   config.get('attributes'),
                  not config.get('override_new_subscan'),
                  self._config['chunksize']))


    @state_change(target="measuring", allowed=["set"], intermediate="measurement_starting")
    async def measurement_start(self):
        """Start the measurement by starting the writing thread
        """
        _log.info("Starting file output")
        self._writer_process.start()


    @state_change(target="ready", allowed=["measuring", "set"], ignored=['ready'], intermediate="measurement_stopping")
    async def measurement_stop(self):
        """Stops the measurement by stopping the writer thread
        """
        if self._writer_process and self._writer_process.is_alive():

            delay = .5   # check every .5 sec
            counter = 10 # wait 10 s
            # Queue join has no timeout!
            while not self._writer_queue.empty():
                await asyncio.sleep(delay)
                counter -= delay
                if counter < 0:
                    _log.warning("Queue not empty after %i s! Aborting.", counter)
                    break

            self._writer_stop_event.set()
            _log.debug("Joining thread")
            await asyncio.to_thread(self._writer_process.join, timeout=10.)

        self._writer_stop_event = None
        self._writer_queue = None
        self._writer_process = None


    @state_change(target="idle", allowed=["ready", "streaming", "deconfiguring"], intermediate="capture_stopping")
    async def capture_stop(self):
        """Stop the data/network capturing
        """
        self._stop_capturing()


    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        if self.data_plotter.plotting:
            self._plot_task.cancel()
        self._stop_capturing()


    def _stop_capturing(self):
        _log.debug("Stopping capturing")
        if self._capture_thread:
            self._capture_thread.stop()
            self._capture_thread.join(10.0)
            self._capture_thread = None
        _log.debug("Capture thread cleaned")


    def _package_writer(self, packet: SpeadPacket):
        """
        Writes a received heap to the writer queue and the data plotter
        Args:
            packet (dict): packet containing a heap
        """
        _log.trace("Writing package")
        if self.data_plotter.plotting:
            self.data_plotter.addSnapshot(packet)

        data, section_id = prepare_heap(packet)
        if self._state == "measuring":
            # ToDo: Handle different prefixes in one stream.
            attributes = None

            self._writer_queue.put((section_id, data, attributes))
            _log.debug('Putting data to write queue, qsize: %i', self._writer_queue.qsize())
        else:
            _log.trace("Not measuring, Dropping package")


    async def periodic_sensor_update(self):
        """
        Updates the katcp sensors.

        This is a periodic update as the connection are managed using threads and not co-routines.
        """
        while True:
            timestamp = time.time()
            if self._capture_thread:
                conditional_update(self._incomplete_heaps, self._capture_thread.incomplete_heaps, timestamp=timestamp)
                conditional_update(self._complete_heaps, self._capture_thread.complete_heaps, timestamp=timestamp)
                statvfs = os.statvfs(self._config["output_directory"])
                conditional_update(self._current_filesystem_avail, statvfs.f_frsize * statvfs.f_bavail, timestamp=timestamp)
                conditional_update(self._current_filesystem_size, statvfs.f_frsize * statvfs.f_blocks, timestamp=timestamp)

            if self._writer_process and self._writer_process.is_alive():
                try:
                    filesize = os.path.getsize(self._current_file.value)
                except FileNotFoundError: # File may not exist at time of check
                    filesize = 0
                conditional_update(self._current_file_size, filesize, timestamp=timestamp)
                conditional_update(self._writer_queue_size, self._writer_queue.qsize(), timestamp=timestamp)
            await asyncio.sleep(1)


    async def start(self):
        await super().start()
        task = asyncio.create_task(self.periodic_sensor_update())
        self.add_service_task(task)

    async def on_stop(self):
        """
        Handle server stop. Stop capture thread
        """
        try:
            self._stop_capturing()
        except Exception as E:
            _log.error("Exception during stop! {}".format(E))
        if self._writer_process and self._writer_process.is_alive():
            self._writer_stop_event.set()
            try:
                await asyncio.to_thread(self._writer_process.join, timeout=5)
            except TimeoutError:
                self._writer_process.kill()


if __name__ == "__main__":
    asyncio.run(launchPipelineServer(HDF5Writer))
