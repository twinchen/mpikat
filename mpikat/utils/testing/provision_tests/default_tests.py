from . common import Stage, EDDTest


state_targets = {
        Stage.provision: ["idle"],
        Stage.configure: ['configured'],
        Stage.capture_start: ['ready', 'streaming'],
        Stage.measurement_prepare: ['set', 'streaming'],
        Stage.measurement_start: ['measuring', 'streaming'],
        Stage.measurement_stop: ['ready', 'streaming'],
        Stage.deconfigure: ['idle']
        }


class StateTest:
    """
    The pipeline state should be in the correct state after a state change.
    """
    def __init__(self, stage):
        self.stage = stage

    async def __call__(self, context):
        _, result = await context.pipeline.request("sensor-value", 'pipeline-status')
        state = result[0].arguments[4].decode('ascii')

        assert state in state_targets[self.stage], f"Pipeline in incorrect state {state}. Expected: {' or '.join(state_targets[self.stage])}"

def register_default_tests(pipelines, registry):
    """
    Add a set of default tests for all pipelines
    """
    for pipeline_name in pipelines:
        for stage in state_targets:
            registry.add(EDDTest(stage, pipeline_name=pipeline_name, func=StateTest(stage), name='test_pipeline_has_correct_state'))


